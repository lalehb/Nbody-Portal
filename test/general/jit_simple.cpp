#include <iostream>
#include "Nbody.h"

using namespace std;
using namespace Nbody;

int main (int argc, char** argv)
{
  Var x,y;
  Func kernel;

  cout << "Simple add function...\n";
  kernel(x,y) = x+y;

  cout << "Vectorizing outer loop...\n";
  kernel.vectorize(x,4);

  cout << "Computing function to lowered form...\n";
  const char* result_file = "simple_lowered.stmt";
  kernel.compile_to_lowered_form(result_file);

  cout << "Evaluating function...\n";
  Points<int32_t> output = kernel.evaluate(8,5);

  /* Check output */
  for (int j = 0; j < 5; ++j)
    for (int i = 0; i < 8; ++i) {
      cout << i << ", " << j << ": " << output(i, j) << '\n';
    }

  cout << "Passed." << endl;
  return 0;
}

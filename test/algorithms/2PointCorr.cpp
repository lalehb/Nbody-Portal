#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;


static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <correlationValue>  \n", use);
}




int main (int argc, char** argv){

       if (argc != 4) {
            usage__ (argv[0]);
            return -1;
        }

        /* input the target dataset */
        std::string filePathT = argv[1];
        Storage fileObjT(filePathT);

        /* input the source dataset */
        std::string filePathS = argv[2];
        Storage fileObjS(filePathS);

        int h1 = atoi(argv[3]);

        Clock timer;

        /* defining the kernel funcion */
        Var a;
        Var b;
        Expr EuclidDist = sqrt(pow((a-b),2));
        Expr q1 = Flt::make(h1);

        Expr CorrEuclidDist = (sqrt(pow((a-b),2) < q1);

        /* defining the N-body expression using portal IRs*/
        NBodyExpression expr;
        expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , a , fileObjT);
        expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , b , fileObjS , CorrEuclidDist);

        const char* result_file = "test/2PointCorr.stmt";
        expr.compile_to_lowered_form(result_file);


        /* N-body caculation with portal IRs using the tree traversal */
        timer.start();
        expr.executeTraverse();
        cout << "Total time: " << timer.seconds() << endl;

        Storage output = expr.getOutput();
        bool valid = expr.Validate(output);

        if (!valid) {
          cout << "Passed!" << endl;
        }
        else {
          cout << "Failed!" << endl;
        }

	return 0;
}

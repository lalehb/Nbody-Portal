#include "IRNode.h"

namespace Nbody {

template<> IRNodeType ExprNode<Integer>::_ir_type = {};
template<> IRNodeType ExprNode<Flt>::_ir_type = {};
template<> IRNodeType ExprNode<Dbl>::_ir_type = {};
template<> IRNodeType ExprNode<String>::_ir_type = {};
template<> IRNodeType ExprNode<Cast>::_ir_type = {};
template<> IRNodeType ExprNode<rsqrt>::_ir_type = {};
template<> IRNodeType ExprNode<Variable>::_ir_type = {};
template<> IRNodeType ExprNode<Add>::_ir_type = {};
template<> IRNodeType ExprNode<Sub>::_ir_type = {};
template<> IRNodeType ExprNode<Mul>::_ir_type = {};
template<> IRNodeType ExprNode<Div>::_ir_type = {};
template<> IRNodeType ExprNode<Mod>::_ir_type = {};
template<> IRNodeType ExprNode<Min>::_ir_type = {};
template<> IRNodeType ExprNode<Max>::_ir_type = {};
template<> IRNodeType ExprNode<ComparisonOp>::_ir_type = {};
template<> IRNodeType ExprNode<Call>::_ir_type = {};
template<> IRNodeType ExprNode<Vector>::_ir_type = {};
template<> IRNodeType ExprNode<VectorSet>::_ir_type = {};
template<> IRNodeType ExprNode<Let>::_ir_type = {};
template<> IRNodeType StmtNode<LetStmt>::_ir_type = {};
template<> IRNodeType StmtNode<Assign>::_ir_type = {};
template<> IRNodeType ExprNode<Select>::_ir_type = {};
template<> IRNodeType ExprNode<SSO>::_ir_type = {};
template<> IRNodeType ExprNode<Result>::_ir_type = {};
template<> IRNodeType ExprNode<Load>::_ir_type = {};
template<> IRNodeType StmtNode<Store>::_ir_type = {};
template<> IRNodeType StmtNode<MultiStore>::_ir_type = {};
template<> IRNodeType StmtNode<For>::_ir_type = {};
template<> IRNodeType StmtNode<Realize>::_ir_type = {};
template<> IRNodeType StmtNode<Evaluate>::_ir_type = {};
template<> IRNodeType StmtNode<Block>::_ir_type = {};
template<> IRNodeType StmtNode<BlockStmt>::_ir_type = {};
template<> IRNodeType StmtNode<While>::_ir_type = {};
template<> IRNodeType StmtNode<OrderedArray_Insert>::_ir_type = {};


const std::string Call::abs = "abs";
}

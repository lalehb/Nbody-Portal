#ifndef INC_FLATTEN_DIMENSIONS_H
#define INC_FLATTEN_DIMENSIONS_H

#include "IRNode.h"

/* Convert a statement with multi-dimensional store (MultiStore)
and Call into statemtents with single-dimensional store (Store),
and Load nodes respectively. */

namespace Nbody {

using std::ostringstream;

class Flatten : public IRPass {
public:
  Flatten() {}

private:
  Expr flatten_args(const std::string& name, const std::vector<Expr>& args) {
    Expr idx = 0;
    Expr base = 0;
    for (size_t i = 0; i < args.size(); ++i) {
      ostringstream stride_name, begin_name;
      stride_name << name << ".stride." << i;
      begin_name << name << ".begin." << i;
      Expr stride = Variable::make(Int(32), stride_name.str());
      Expr begin = Variable::make(Int(32), begin_name.str());
      idx += args[i] * stride;
      base += begin * stride;
    }
    idx = idx - base;
    return idx;
  }
  
  void visit(const SSO * op){
	  expr = op;
  }

  void visit(const MultiStore* op) {
    Expr index = mutate(flatten_args(op->name, op->args));
    Expr value = mutate(op->value);
    stmt = Store::make(op->name, value, index);
  }

  void visit(const Call* op) {
    if (op->ctype == Call::Extern || op->ctype == Call::Intrinsic) {
      std::vector<Expr> args(op->args.size());
      bool mod = false;
      for (size_t i = 0; i < args.size(); ++i) {
        args[i] = mutate(op->args[i]);
        if(!args[i].same_as(op->args[i]))
          mod = true;
      }
      if (mod)
        expr = Call::make(op->type, op->name, args, op->ctype);
      else
        expr = op;
    }
    else {
      Expr index = mutate(flatten_args(op->name, op->args));
      expr = Load::make(op->type, op->name, index, op->points);
    }
  }
};

Stmt flatten_dimensions(Stmt s) {
  return Flatten().mutate(s);
}

}

#endif

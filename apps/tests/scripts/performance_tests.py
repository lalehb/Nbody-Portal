#!/usr/bin/env python

import os
from os import path
import time
import argparse
import csv

# ----- main directories for executions ----- #
TOP_DIR = "/home/leip1/Nbody-ML/"
RUN_DIR = TOP_DIR + "apps/tests/library"
OUT_DIR = TOP_DIR +  "apps/tests/results/"
DATASET_DIR = "/home/leip1/"
RESULTS_DIR = TOP_DIR +  "apps/tests/"


# ----- setups for scaling experiments ----- #
#TREES = ["kdtree"]
TREES = ["kdtree", "covertree"]

TRAVERSALS = ["yes"]
#TRAVERSALS = ["yes", "no"]  # I need to add another level for Multi tree too and change it as S,D,M (Single, Dual, Multi)

EXPR_NUMBERS = [1, 2, 3, 4, 5]


# ---- very small data set jut for checking ----- #
DATASETS = ["G2-D2-5000"]
DIM = [2]
SIZE = [5000]

# ---- real data sets with smaller size ----- #
DATASETS = ["IHEPC3", "Census2", "KDD3", "yahoo_1M", "HIGGS4", "covtype"]
DIM = [9, 68, 37, 11, 29, 55]
SIZE = [500000, 100000, 300000, 1000000, 100000, 500000]

# ---- real data sets with larger size ----- #
#DATASETS = ["IHEPC", "Census3", "KDD2", "yahoo2","HIGGS2"]
#DIM = [9, 68, 37, 11, 29]
#SIZE = [2075259, 2458285, 4898431, 41904293, 11000000]


# ----- scaling function  for kNN algorithm ----- #
def scaling_kNN():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for expNum in EXPR_NUMBERS:
          
            outputName = OUT_DIR + "perf-kNN-%s-%s-expr%d.csv"%(dataset, tree, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            #command = "(env OMP_NUM_THREADS=%d S=no %s/test--kNN_%s %d %d %s 1 2>&1 | tee -a  %s)" \
            command = "(%s/test--kNN_%s %d %d %s 1 2>&1 | tee -a  %s)" \
                         % (RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command) 

        level = level + 1


# ----- scaling function  for Range Search algorithm ----- #
def scaling_RS():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for expNum in EXPR_NUMBERS:

            outputName = OUT_DIR + "perf-range_search-%s-%s-expr%d.csv"%(dataset, tree, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            command = "(%s/test--range_search_%s %d %d %s 2 3 2>&1 | tee -a  %s)" \
                         % (RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command)
            
        level = level + 1


# ----- scaling function  for kde algorithm ----- #
def scaling_kde():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for expNum in EXPR_NUMBERS:
        
            outputName = OUT_DIR + "perf-kde-%s-%s-expr%d.csv"%(dataset, tree, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            command = "(%s/test--kde_%s %d %d %s 1 2>&1 | tee -a  %s)" \
                         % (RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command)
            
        level = level + 1


# ----- scaling function  for emst algorithm ----- #
def scaling_emst():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for expNum in EXPR_NUMBERS:

            outputName = OUT_DIR + "perf-emst-%s-%s-expr%d.csv"%(dataset, tree, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            command = "(%s/test--emst_%s %d %d %s 2>&1 | tee -a  %s)" \
                         % (RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command)
           
        level = level + 1


# ----- scaling function  for HD algorithm ----- #
def scaling_HD():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for expNum in EXPR_NUMBERS:

            outputName = OUT_DIR + "perf-HD-%s-%s-expr%d.csv"%(dataset, tree, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            command = "(%s/test--HD_%s %d %d %s 2>&1 | tee -a  %s)" \
                         % (RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command)
            
        level = level + 1


# ----- scaling function  for EM algorithm ----- #
def scaling_EM():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for expNum in EXPR_NUMBERS:

            outputName = OUT_DIR + "perf-EM-%s-%s-expr%d.csv"%(dataset, tree, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            command = "(%s/test--EM_%s %d %d 2 0.1  %s 100 0.1  2>&1 | tee -a  %s)" \
                         % (RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command)

        level = level + 1


def main():

  # ----- making a directory for results ----- #
  output_dir = RESULTS_DIR + "results"
  os.system("mkdir " + output_dir )
  # ----- call to seperate functions for each algorithm ----- #
  #scaling_kNN()
  #scaling_RS()
  #scaling_kde()
  scaling_emst()
  #scaling_HD()
  #scaling_EM()

if __name__ == "__main__":
  main()



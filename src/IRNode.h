#ifndef INC_IRNODE_H
#define INC_IRNODE_H


#include <iostream>
#include <assert.h>
#include "IRVisitor.h"
#include "Type.h"
#include "Util.h"
#include "Data.h"

namespace Nbody {

/* Class representing the type of IR Node. Every IR Node
 * returns an unique pointer for identification */
struct IRNodeType {};

/* Abstract base class for a IR node. We use visitor
 * pattern to traverse IR nodes */
struct IRNode {
  IRNode() {}
  virtual ~IRNode() {}
  virtual void accept(IRVisitor *v) const = 0;
  virtual const IRNodeType* ir_type() const = 0;
  mutable RefCount count;
  //static IRNode* make(){}
  static void  make(){}
};

template<>
inline RefCount& count<IRNode>(const IRNode* n) {
  return n->count;
}

template<>
inline void destroy<IRNode>(const IRNode* n) {
  delete n;
}

/* IR nodes are split into expressions and statements.
 * Expressions represent a value and have a type. (eg: add)
 * Statements do not represent any value. (eg: store) */

/* We use CRTP (curiously recurring template pattern)
 * to implement static polymorphism. StmtNode and
 * ExprNode lie between the abstract base class and
 * actual IR nodes in the inheritance hierarchy. It
 * implements the accept method required by the visitor
 * pattern. */
struct BaseStmtNode : public IRNode {
};

template<typename T>
struct StmtNode : public BaseStmtNode {
  void accept(IRVisitor *v) const {
    v->visit((const T*)this);
  }
  static IRNodeType _ir_type;
  const IRNodeType* ir_type() const {
    return &_ir_type;
  }
 //static IRNode* make(){}
 static void  make(){}
};

struct ExprType : public IRNode {
  Type type;
};

template<typename T>
struct ExprNode : public ExprType {
  void accept(IRVisitor *v) const {
    v->visit((const T*)this);
  }
  static IRNodeType _ir_type;
  const IRNodeType* ir_type() const {
    return &_ir_type;
  }
};


/* Base class for handles. IR nodes are passed handles
 * to them. It also manages dispatching visitors. */
struct IRHandle : public Ptr<const IRNode> {
  IRHandle() : Ptr<const IRNode>() {}
  IRHandle(const IRNode *p) : Ptr<const IRNode>(p) {}

  void accept (IRVisitor *v) const {
    ptr->accept(v);
  }

  /* Downcast to actual IR node */
  template<typename T>
  const T* ir() const {
    if (ptr->ir_type() == &T::_ir_type)
      return (const T*) ptr;
    return NULL;
  }
};

/* Integer node */
struct Integer : public ExprNode<Integer> {
  int value;

  static Integer *make (int value) {
    Integer *node = new Integer;
    node->type = Int(32);
    node->value = value;
    return node;
  }
};

/* Float node */
struct Flt : public ExprNode<Flt> {
  double value;

  static Flt *make (double value) {
    Flt *node = new Flt;
    node->type = Float(32);
    node->value = value;
    return node;
  }
};

/* Double node */
struct Dbl : public ExprNode<Dbl> {
  double value;

  static Dbl *make (double value) {
    Dbl *node = new Dbl;
    node->type = Float(64);
    node->value = value;
    return node;
  }
};

/* String node */
struct String : public ExprNode<String> {
  std::string value;

  static String *make (const std::string &val) {
    String *node = new String;
    node->type = Handle();
    node->value = val;
    return node;
  }
};

/* Handle to actual expression nodes */
struct Expr : public IRHandle {
  Expr() : IRHandle() {}

  Expr(const ExprType *n) : IRHandle(n) {}

  Expr(int x) : IRHandle(Integer::make(x)) {}

  Expr(float x) : IRHandle(Flt::make(x)) {}

  Expr(double x) : IRHandle(Dbl::make(x)) {}

  Expr(const std::string &s) : IRHandle(String::make(s)) {}

  Type type() const {
    return ((const ExprType *)ptr)->type;
  }

  inline Expr operator[](int index);
};
}

/* Now that we've defined Expr, we can include Parameter.h */
#include "Parameter.h"

/* Include Reduction.h */
#include "Reduction.h"

namespace Nbody {

/* Handle to statement nodes */
struct Stmt : public IRHandle {
  Stmt() : IRHandle() {}

  Stmt(const BaseStmtNode *n) : IRHandle(n) {}
};

/* Cast from one type to another */
struct Cast : public ExprNode<Cast> {
  Expr value;

  static Expr make (Type t, Expr v) {
    assert (v.defined() && "Cast of undefined expression");

    Cast *node = new Cast;
    node->type = t;
    node->value = v;
    return node;
  }
};

/* Cast from one type to another */
struct rsqrt : public ExprNode<rsqrt> {
  Expr value;

  static Expr make (Expr v) {
    assert (v.defined() && "Cast of undefined expression");

    rsqrt *node = new rsqrt;
    node->type = v.type();
    node->value = v;
    return node;
  }
};

/* Variable node -- can also be a loop variable or function
 * argument  */
struct Variable : public ExprNode<Variable> {
  std::string name;
  Parameter param;
  ReductionDomain rdom;

  /* If type is not specified, default type is double */
  static Expr make (std::string name) {
    return make (Int(32), name, Parameter(), ReductionDomain());
  }

  static Expr make (Type type, std::string name) {
    return make (type, name, Parameter(), ReductionDomain());
  }

  static Expr make (Type type, std::string name, Parameter param) {
    return make (type, name, param, ReductionDomain());
  }

  static Expr make (Type type, std::string name, ReductionDomain rdom) {
    return make (type, name, Parameter(), rdom);
  }

  static Expr make (Type type, std::string name, Parameter param, ReductionDomain rdom) {
    Variable *node = new Variable;
    node->type = type;
    node->name = name;
    node->param = param;
    node->rdom = rdom;
    return node;
  }
};

/* Basic binary operations such as Add, Sub, Mul, Div,
 * and Mod of two expressions */
struct Add : public ExprNode<Add> {
  Expr a, b;

  static Expr make (Expr a, Expr b) {
    assert (a.defined() && "Add of undefined a");
    assert (b.defined() && "Add of undefined b");
    assert (a.type() == b.type() && "Add type mismatch");
    Add *node = new Add;
    node->type = a.type();
    node->a = a;
    node->b = b;
    return node;
  }
};

struct Sub : public ExprNode<Sub> {
  Expr a, b;

  static Expr make (Expr a, Expr b) {
    assert (a.defined() && "Sub of undefined a");
    assert (b.defined() && "Sub of undefined b");
    assert (a.type() == b.type() && "Sub type mismatch");

    Sub *node = new Sub;
    node->type = a.type();
    node->a = a;
    node->b = b;
    return node;
  }
};

struct Mul : public ExprNode<Mul> {
  Expr a, b;

  static Expr make (Expr a, Expr b) {
    assert (a.defined() && "Mul of undefined a");
    assert (b.defined() && "Mul of undefined b");
    assert (a.type() == b.type() && "Mul type mismatch");
    Mul *node = new Mul;
    node->type = a.type();
    node->a = a;
    node->b = b;
    return node;
  }
};

struct Div : public ExprNode<Div> {
  Expr a, b;

  static Expr make (Expr a, Expr b) {
    assert (a.defined() && "Div of undefined a");
    assert (b.defined() && "Div of undefined b");
    assert (a.type() == b.type() && "Div type mismatch");
    Div *node = new Div;
    node->type = a.type();
    node->a = a;
    node->b = b;
    return node;
  }
};

struct Mod : public ExprNode<Mod> {
  Expr a, b;

  static Expr make (Expr a, Expr b) {
    assert (a.defined() && "Mod of undefined a");
    assert (b.defined() && "Mod of undefined b");
    assert (a.type() == b.type() && "Mod type mismatch");
    Mod *node = new Mod;
    node->type = a.type();
    node->a = a;
    node->b = b;
    return node;
  }
};

struct Min : public ExprNode<Min> {
  Expr a, b;
  static Expr make (Expr a, Expr b) {
    assert (a.defined() && "Min of undefined value");
    assert (b.defined() && "Min of undefined value");
    assert (a.type() == b.type() && "Min of mismatched types");

    Min *node = new Min;
    node->type = a.type();
    node->a = a;
    node->b = b;
    return node;
  }
};

struct Max : public ExprNode<Max> {
  Expr a, b;
  static Expr make (Expr a, Expr b) {
    assert (a.defined() && "Max of undefined value");
    assert (b.defined() && "Max of undefined value");
    assert (a.type() == b.type() && "Max of mismatched types");

    Max *node = new Max;
    node->type = a.type();
    node->a = a;
    node->b = b;
    return node;
  }
};

/* Comparison operations such as ==, !=, <, >, <=, and
 * >= of one or two expressions */
struct ComparisonOp : public ExprNode<ComparisonOp> {
  std::string op;
  Expr a, b;

  static Expr make (Expr a, std::string op) {
    assert (a.defined() && "ComparisonOp of undefined a");
    ComparisonOp *node = new ComparisonOp;
    node->type = Bool(); //a.type();
    node->a = a;
    node->op = op;
    return node;
  }

  static Expr make (Expr a, std::string op, Expr b) {
    assert (a.defined() && "ComparisonOp of undefined a");
    assert (b.defined() && "ComparisonOp of undefined b");
    assert (a.type() == b.type() && "ComparisonOp type mismatch");
    ComparisonOp *node = new ComparisonOp;
    node->type = Bool(); //a.type();
    node->a = a;
    node->b = b;
    node->op = op;
    return node;
  }
};

/* For loop. Executes the 'body' statements from 'begin'
 * to 'end'. */
struct For : public StmtNode<For> {
  std::string name;
  Expr begin, end, stride;
  typedef enum {Serial, Parallel, Vectorized} ForType;
  ForType ftype;
  Stmt body;

  static Stmt make (std::string name, Expr begin, Expr end, Expr stride, ForType ftype, Stmt body) {
    assert (begin.defined() && "For of undefined begin index");
    assert (end.defined() && "For of undefined end index");
    assert (stride.defined() && "For of undefined stride");
    assert (begin.type().is_scalar());
    assert (end.type().is_scalar());
    assert (body.defined() && "For of undefined body");

    For *node = new For;
    node->name = name;
    node->begin = begin;
    node->end = end;
    node->stride = stride;
    node->ftype = ftype;
    node->body = body;
    return node;
  }
};

struct While : public StmtNode<While>{
	std::string name;
	Expr condition;
	Stmt body;

	static Stmt make(std::string name , Expr condition , Stmt body){
		assert(condition.defined() && "While undefined condition");
		assert(body.defined() && "While undefined condition");

		While * node = new While;
		node->name = name;
		node->condition = condition;
		node->body = body;
		return node;
	}
};

}
/* Now that we've defined Expr and ForType, we can include Function.h */
#include "Function.h"



namespace Nbody {


/* Function call - a call to an internal (user defined) or external
 * (such as pow, sqrt) function */
struct Call : public ExprNode<Call> {
  std::string name;
  std::vector<Expr> args;
  typedef enum {Intrinsic, Points, Extern, Nbody} CallType;
  CallType ctype;
  static const std::string abs;
  Function func;
  Data points;

  static Expr make (Type type, std::string name, const std::vector<Expr> &args, CallType ctype, Function func, Data points) {
    for (size_t i = 0; i < args.size(); i++)
      assert (args[i].defined() && "Call of undefined argument");

    Call *node = new Call;
    node->type = type;  // Return type
    node->name = name;
    node->args = args;
    node->ctype = ctype;
    node->func = func;
    node->points = points;
    return node;
  }

  /* Constructor for calls from externally defined or instrinsic functions */
  static Expr make(Type type, std::string name, const std::vector<Expr> args, CallType ctype) {
    return make(type, name, args, ctype, Function(), Data());
  }

  /* Constructor for calls to Nbody functions */
  static Expr make(Function func, const std::vector<Expr> args) {
    assert(func.value().defined() && "Call to undefined Nbody function");
    return make(func.value().type(), func.name(), args, Nbody, func, Data());
  }

  /* Constructor for loads from points */
  static Expr make(Data points, const std::vector<Expr> args) {
    return make(points.type(), points.name(), args, Points, Function(), points);
  }

  static Expr make(Type type , Data points, const std::vector<Expr> args) {
    return make(type, points.name(), args, Points, Function(), points);
  }
};

/* Vector node where element i is 'base' + i*'stride' */
struct Vector : public ExprNode<Vector> {
  int width;
  Expr base, stride;

  static Expr make (Expr base, Expr stride, int width) {
    assert (base.defined() && "Vector of undefined base");
    assert (stride.defined() && "Vector of undefined stride");
    assert (width > 1 && "Vector of width<=1");

    Vector *node = new Vector;
    node->type = base.type().make_vector(width);
    node->base = base;
    node->stride = stride;
    node->width = width;
    return node;
  }
};

/* Create a vector of 'width' elements each having the same value */
struct VectorSet : public ExprNode<VectorSet> {
  Expr value;
  int width;

  static Expr make (Expr value, int width) {
    assert (value.defined() && "Vector copy of undefined value");
    assert (value.type().is_scalar() && "Vector copy of vector");
    assert (width > 1 && "Vector of width<=1");

    VectorSet* node = new VectorSet;
    node->type = value.type().make_vector(width);
    node->value = value;
    node->width = width;
    return node;
  }
};

/* Assign expression. If Assign::name was previously assigned to, Assign::name refers to Let::value */
struct Assign : public StmtNode<Assign> {
  std::string name;
  Expr value;
  bool staticAssign;

  static Stmt make (std::string name, Expr value, bool staticAssign = false) {
    assert (value.defined() && "Assign of undefined value");
    Assign *node = new Assign;
    node->name = name;
    node->value = value;
	  node->staticAssign = staticAssign;
    return node;
  }
};

/* Let expression. Within Let::body, Let::name refers to Let::value */
struct Let : public ExprNode<Let> {
  std::string name;
  Expr value;
  Expr body;

  static Expr make (std::string name, Expr value, Expr body) {
    assert (value.defined() && "Let of undefined value");
    assert (body.defined() && "Let of undefined expression");
    Let *node = new Let;
    node->name = name;
    node->value = value;
    node->body = body;
    return node;
  }
};

/* Statement form of Let expression. Within LetStmt::body,
LetStmt::name refers to LetStmt::value */
struct LetStmt : public StmtNode<LetStmt> {
  std::string name;
  Expr value;
  Stmt body;

  static Stmt make (std::string name, Expr value, Stmt body) {
    assert (value.defined() && "LetStmt of undefined value");
    assert (body.defined() && "LetStmt of undefined statement");

    LetStmt *node = new LetStmt;
    node->name = name;
    node->value = value;
    node->body = body;
    return node;
  }
};

/* A ternary operator equivalent to C */
struct Select : public ExprNode<Select> {
  Expr cond, true_value, false_value;

  static Expr make (Expr cond, Expr true_value, Expr false_value) {
    assert (cond.defined() && "Select of undefined condition");
    assert (true_value.defined() && "Select of undefined true value");
    assert (false_value.defined() && "Select of undefined false value");
    assert (cond.type().is_bool() && "First argument of Select is not a bool");
    assert (true_value.type() == false_value.type() && "Select of mismatched type");
    assert (cond.type().is_scalar() || cond.type().nelem == true_value.type().nelem &&
            "Select of wrong type");

    Select *node = new Select;
    node->type = true_value.type();
    node->cond = cond;
    node->true_value = true_value;
    node->false_value = false_value;
    return node;
  }
};

/* Load a value from a buffer (treated as an array of the given type). */
struct Load : public ExprNode<Load> {
  enum LoadType {REFERENCE , DEREFERENCE , VALUE, FF};
  std::string name;
  Expr index;
  Expr stride;
  Expr pointer;
  int intStride;
  /* If it's a load from Points, this will point to that */
  Data points;
  LoadType lt;


  static Expr make (Type type, std::string name, Expr index, Data points , bool getPointer = false) {
	if(getPointer){
		//Load pointer
		assert (index.defined() && "Load of undefined index");
		Load *node = new Load;
		node->name = name;
		node->type = type;
		node->index = index;
		node->points = points;
		node->lt = LoadType::REFERENCE;
		return node;
	}
    return make (type, name, index, 1, points);
  }

  static Expr make (Type type, std::string name, Expr index, bool getPointer , int stride) {
  	if(getPointer){
  		//Load pointer
  		assert (index.defined() && "Load of undefined index");
  		Load *node = new Load;
  		node->name = name;
  		node->type = type;
  		node->index = index;
  		node->lt = LoadType::REFERENCE;
  		node->intStride = stride;
  		return node;
  	} else {
      assert (index.defined() && "Load of undefined index");
  		Load *node = new Load;
  		node->name = name;
  		node->type = type;
  		node->index = index;
  		node->lt = LoadType::VALUE;
  		node->intStride = stride;
  		return node;
    }
  }

  static Expr make(Type type , std::string name , Expr pointer , Expr index){
	//Load from pointer
	assert (index.defined() && "Load of undefined index");
	assert (pointer.defined() && "Load of undefined pointer");
	Load *node = new Load;
	node->type = type;
	node->name = name;
	node->index = index;
	node->pointer = pointer;
	node->lt = LoadType::DEREFERENCE;
	return node;
  }

  static Expr make(Type type, std::string name, Expr index, Expr stride, Data points) {
    assert (index.defined() && "Load of undefined index");
    Load *node = new Load;
    node->type = type;
    node->name = name;
    node->index = index;
    node->stride = stride;
    node->points = points;
	  node->lt = LoadType::VALUE;
    return node;
  }
};


struct SSO : public ExprNode<SSO>{
	std::string name;
	static SSO make (std::string name){
		SSO * node = new SSO;
		node->name = name;
		return *node;
	}
	inline Expr operator[](int index){
		Expr mult = Mul::make(Integer::make(index) , Variable::make(this->name + ".stride.0"));
		return Load::make(Float(32) , this->name , mult , true , 1);
	}
};

struct Result : public ExprNode<Result>{
	static Expr make (std::string name){
		return Variable::make(Float(32) , "in_result_" + name);
	}
};

/* Store an expression or value to an indentifier. */
struct Store : public StmtNode<Store> {
  std::string name;
  Expr value;
  Expr index;
  Expr stride;

  static Stmt make (std::string name, Expr value, Expr index) {
    return make (name, value, index, 1);
  }

  static Stmt make (std::string name, Expr value, Expr index, Expr stride) {
    assert (value.defined() && "Store of undefined value");
    assert (index.defined() && "Store at undefined index");
    assert (stride.defined() && "Store at undefined stride");

    Store *node = new Store;
    node->name = name;
    node->value = value;
    node->index = index;
    node->stride = stride;
    return node;
  }
};

/* Store to a multi-dimensional array. */
struct MultiStore : public StmtNode<MultiStore> {
  std::string name;
  Expr value;
  std::vector<Expr> args;

  static Stmt make (std::string name, Expr value, const std::vector<Expr>& args) {
    assert (value.defined() && "MultiStore of undefined value");
    for (size_t i = 0; i < args.size(); ++i)
      assert (args[i].defined() && "MultiStore of undefined index");

    MultiStore *node = new MultiStore;
    node->name = name;
    node->value = value;
    node->args = args;
    return node;
  }
};

/* Range struct contains min and max values in 1-D */
struct Range {
  Expr min, max;
  Range() {}
  Range(Expr min, Expr max) : min(min), max(max) {}
};

/* The range in 2-D */
typedef std::vector<Range> Region;

/* Allocate a multi-dimensional buffer with range specified by 'bounds' */
struct Realize : public StmtNode<Realize> {
  std::string name;
  Type type;
  Region bounds;
  Stmt body;

  static Stmt make(const std::string& name, const Type& type, const Region& bounds, Stmt body) {
    for (size_t i = 0; i < bounds.size(); ++i) {
      assert(bounds[i].min.defined() && "Realize of undefined");
      assert(bounds[i].max.defined() && "Realize of undefined");
      assert(bounds[i].min.type().is_scalar() && "Realize of vector");
      assert(bounds[i].max.type().is_scalar() && "Realize of vector");
    }
    assert(body.defined() && "Realize of undefined");

    Realize* node = new Realize;
    node->name = name;
    node->type = type;
    node->bounds = bounds;
    node->body = body;
    return node;
  }
};

/* Evalaute an expression and return nothing */
struct Evaluate : public StmtNode<Evaluate> {
  Expr value;

  static Stmt make (Expr v) {
    assert (v.defined() && "Evaluate of undefined value");

    Evaluate* node = new Evaluate;
    node->value = v;
    return node;
  }
};

/* A sequence of expressions to be executed in order */
struct Block : public StmtNode<Block> {
  std::vector<Expr> stmts;

  static Stmt make (std::vector<Expr> &stmts) {
    for (size_t i = 0; i < stmts.size(); i++)
      assert (stmts[i].defined() && "Block of undefined expression");

    Block *node = new Block;
    node->stmts = stmts;
    return node;
  }
};

/* A sequence of statements to be executed in order */
struct BlockStmt : public StmtNode<BlockStmt> {
  std::vector<Stmt> stmts;

  static Stmt make (std::vector<Stmt> &stmts) {
    for (size_t i = 0; i < stmts.size(); i++)
      assert (stmts[i].defined() && "Block of undefined statament");

    BlockStmt *node = new BlockStmt;
    node->stmts = stmts;
    return node;
  }
};

struct OrderedArray_Insert : public StmtNode<OrderedArray_Insert>{
	std::string arrayName;
	int k;
	Expr value;
	Expr index;


	static Stmt make(std::string arrayName , int k , Expr value , Expr index){
		assert(value.defined() && "value not defined");
		assert(index.defined() && "index not defined");

		OrderedArray_Insert * node = new OrderedArray_Insert;
		node->arrayName = arrayName;
		node->k = k;
		node->value = value;
		node->index = index;
		return node;
	}
};




inline Expr Expr::operator[](int index){
	assert(this->defined() && "operator '[]' of undefined Expr");
	return Load::make(Float(32) , "OPOverload" , *this , Integer::make(index));
  }

}
#endif

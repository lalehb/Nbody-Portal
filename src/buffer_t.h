#ifndef INC_BUFFER_T_H
#define INC_BUFFER_T_H

#ifndef BUFFER_T_DEFINED
#define BUFFER_T_DEFINED

/* Defines the internal runtime representation of Points */

typedef struct buffer_t {
  uint8_t* data;

  /* The runtime begin and end of data */
  int32_t begin[2];
  int32_t end[2];
  int32_t stride[2];

  /* The number of bytes of each data element */
  int32_t elem_size;
} buffer_t;

#endif

#endif

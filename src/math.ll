declare float @llvm.sqrt.f32(float) nounwind readnone

define weak_odr float @sqrt_f32(float %x) nounwind uwtable readnone alwaysinline {
  %y = tail call float @llvm.sqrt.f32(float %x) nounwind readnone
  ret float %y
}

declare double @llvm.sqrt.f64(double) nounwind readnone

define weak_odr double @sqrt_f64(double %x) nounwind uwtable readnone alwaysinline {
  %y = tail call double @llvm.sqrt.f64(double %x) nounwind readnone
  ret double %y
}

declare float @llvm.pow.f32(float, float) nounwind readnone

define weak_odr float @pow_f32(float %x, float %y) nounwind uwtable readnone alwaysinline {
  %z = tail call float @llvm.pow.f32(float %x, float %y) nounwind readnone
  ret float %z
}

declare double @llvm.pow.f64(double, double) nounwind readnone

define weak_odr double @pow_f64(double %x, double %y) nounwind uwtable readnone alwaysinline {
  %z = tail call double @llvm.pow.f64(double %x, double %y) nounwind readnone
  ret double %z
}

declare float @llvm.floor.f32(float) nounwind readnone

define weak_odr float @floor_f32(float %x) nounwind uwtable readnone alwaysinline {
  %y = tail call float @llvm.floor.f32(float %x) nounwind readnone
  ret float %y
}

declare double @llvm.floor.f64(double) nounwind readnone

define weak_odr double @floor_f64(double %x) nounwind uwtable readnone alwaysinline {
  %y = tail call double @llvm.floor.f64(double %x) nounwind readnone
  ret double %y
}


declare <4 x float> @llvm.x86.sse.rsqrt.ss(<4 x float>) nounwind readnone

define weak_odr float @fast_inverse_sqrt_f32(float %x) nounwind uwtable readnone alwaysinline {
  %vec = insertelement <4 x float> undef, float %x, i32 0
  %approx = tail call <4 x float> @llvm.x86.sse.rsqrt.ss(<4 x float> %vec)
  %result = extractelement <4 x float> %approx, i32 0
  ret float %result
}

declare <4 x float> @llvm.x86.sse.rsqrt.ps(<4 x float>) nounwind readnone

define weak_odr <4 x float> @fast_inverse_sqrt_f32x4(<4 x float> %x) nounwind uwtable readnone alwaysinline {
  %approx = tail call <4 x float> @llvm.x86.sse.rsqrt.ps(<4 x float> %x);
  ret <4 x float> %approx
}

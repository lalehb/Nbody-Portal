1- compile flags can be used like:

   c++  Naive-llvm.cpp -lpthread `llvm-config --cxxflags  --libs core  --ldflags --system-libs` -w -o naive-llvm

2- Running this will result in prinintg the LLVM IRs (I dump the modules at the end for printing IRs)

   ./naive-llvm <num_pts1> <num_pts2> <Dim>

3- For printing the IR nodes with CLANG this command can be used:
 
  clang++ Naive.cpp -lpthread `llvm-config --cxxflags  --libs core  --ldflags --system-libs` -w -S  -emit-llvm -o llvmIR_withClang
 

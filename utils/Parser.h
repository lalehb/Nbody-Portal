#ifndef _PARSER_H_
#define _PARSER_H_

#define DATA_SIZE 10

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"

#include <iostream>
#include <cstdio>
#include <vector>

#include "utils.h"
#include "reals.h"
#include "vector_of_array.hpp"
#include "exprtk.hpp"
#include "Epanechnikov_kernel.h" 
#include "Spherical_kernel.h"
#include "Gaussian_kernel.h"
#include "General_kernel.h"
#include "IRNode.cpp"
#include "IRPrinter.cpp"
#include "IRNode.h"

typedef Metric<Point, Point> MetricType;
typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;
typedef vector_of_array<real_t> Points;
typedef typename Points::array_type Point;

typedef exprtk::symbol_table<real_t> symbol_table_t;
typedef exprtk::expression<real_t>     expression_t;
typedef exprtk::parser<real_t>             parser_t;


using namespace std;
namespace Parser {

  static int NumofOp;
  static int NumofDatasets;

  Points Dataset[DATA_SIZE];
  int DatasetSize[DATA_SIZE];
  const char* DatasetNames[DATA_SIZE];
  int dim;
  MetricType*  D;
  /* arrays to keep the  variables of the kernels*/
  array<string,10> DVars;
  array<string,10> KVars;
  array<real_t,10> vars_temp;
  


  Kernel* kernel;
  string KernelInput = "";  
  string DistanceInput = "";
  string condition;
  /* TODO
  * Look for other operators and add all the operations possible
  */
  enum Operation {
    OP_Forall,
    OP_Mul,
    OP_Union,
    OP_Max,
    OP_Min,
    OP_Argmax,
    OP_Argmin,
    OP_Sum,
  } Op[10];

  /* Reading the input datasets */
  Points Read(const char* filename, int num_pts, int Dim) {

    /* Allocate memory for original  datasets */
    Points DataInput(num_pts, Dim);
    from_file(DataInput, filename);
    dim = Dim;
    return DataInput;

  }
 

  MetricType* DistanceR(string S) {
    MetricType* k;

    symbol_table_t symbol_table;
    //symbol_table.add_constants();
    for (int i = 0; i < dim; i++ ) {
       symbol_table.add_variable(DVars[i],vars_temp[i]);
    }
    
    expression_t expression;
    expression.register_symbol_table(symbol_table);

    parser_t parser;
    parser.compile(S,expression);

    //k = new MetricType(expression, D);
    return k;
  }
 
  /* Setting up the distance function between points */
  //MetricType* DistanceReader(string DistancePattern, int dataset1, int dataset2) {
  MetricType* DistanceReader(string DistancePattern) {
    MetricType* distance = new EuclideanMetric<Point, Point>();
    DistanceInput = DistancePattern;
    distance = DistanceR(DistancePattern);
    return distance;
  }





  /* Translating an input  of  string to an expression */
  Kernel* Reader(string S) {
    Kernel* k;

    symbol_table_t symbol_table;
    symbol_table.add_constants();
    for (int i = 0; i < dim-1; i++ ) {
       symbol_table.add_variable(KVars[i],vars_temp[i]);
    }
    
    real_t d;
    symbol_table.add_variable("D",d);
       
    expression_t expression;
    expression.register_symbol_table(symbol_table);

    parser_t parser;
    parser.compile(S,expression);

    k = new GeneralKernel(expression, D);
    return k;
  }
 
  /* Reading the kernel and checking for predefined kernels or a general kernel */
  Kernel* KernelReader(string inputKernel) {
 // Kernel* KernelReader(string inputKernel) {
    Kernel* k;
    KernelInput = inputKernel;
    k = Reader(inputKernel);
    return k;
  } 


  string Condition(string s) {
    //TODO
    /* We need to generate the prune condition based on the kernel
     * function and the operators that are provided by user
     * */ 
    return s;
  }



  /* IR node generation for operations */

  Nbody::IRNode* Handle_op_generation(Operation O, int b, int e, int s) {
   Nbody::IRNode* Op1;
   std::string begin = "begin";
   std::string end = "end";
   
   Nbody::Expr stride = 1;

   switch (O) {
     case 0:
     {  
       Nbody::Forall* Op = new Nbody::Forall();
       std::string i = "f";
       //Op = Op->make(i, (begin), (end), (stride));
       Op = Op->make(i, (b), (e), (s));
       return Op;
     }
     case 1:
     {  
       Nbody::Mul* Op = new Nbody::Mul();
       std::string i = "o";
    //   Op = Op->make(i, (begin), (end), (stride));
       return Op;
     }
     case 2:
     {  
       Nbody::Union* Op = new Nbody::Union();
       std::string i = "o";
      // Op = Op->make(i, (begin), (end), (stride));
       Op = Op->make(i, (b), (e), (s));
       return Op;
     }
     case 3:
     {  
       Nbody::Max* Op = new Nbody::Max();
       std::string i = "o";
       //Op = Op->make(i, (begin), (end), (stride));
       Op = Op->make(i, (b), (e), (s));
       return Op;
     }
     case 4:
     {  
       Nbody::Min* Op = new Nbody::Min();
       std::string i = "o";
       //Op = Op->make(i, (begin), (end), (stride));
       Op = Op->make(i, (b), (e), (s));
       return Op;
     }
     case 5:
     {  
       Nbody::ArgMax* Op = new Nbody::ArgMax();
       std::string i = "o";
       //Op = Op->make(i, (begin), (end), (stride));
       return Op;
     }
     case 6:
     {  
       Nbody::ArgMin* Op = new Nbody::ArgMin();
       std::string i = "o";
      // Op = Op->make(i, (begin), (end), (stride));
       return Op;
     }
     case 7:
     {  
       Nbody::Sum* Op = new Nbody::Sum();
       std::string i = "o";
       //Op = Op->make(i, (begin), (end), (stride));
       Op = Op->make(i, (b), (e), (s));
       return Op;
     }
     default:
     {  
       Nbody::Forall* Op = new Nbody::Forall();
       std::string i = "f";
       //Op = Op->make(i, (begin), (end), (stride));
       return Op;
     }
   
   }
   return Op1;
  }

  Nbody::IRNode* Handle_output_generation(Operation Op) {
    Nbody::outputIR* output = new Nbody::outputIR();
    double ScalarVal;
    std::vector<double> ArrayVal;
    Points pointset;
    //output = output->make(Op.name);
    switch(Op) {
     case 0:// Forall
     {  
       output = output->make("", pointset);
       //cout << "hello\n";
       return output;
     }
     case 1: //Mul
     {  
      
       output = output->make("Mul", ScalarVal);
       return output;
     }
     case 2://Union
     {  
       output = output->make("Union", pointset);
       return output;
     }
     case 3://max
     {  
       output = output->make("Max",ScalarVal);
       return output;
     }
     case 4://Min
     {  
       output = output->make("Min", ScalarVal);
       return output;
     }
     case 5://Argmax
     {
       output = output->make("ArgMax", ScalarVal);
       return output;
     }
     case 6://ArgMin
     {  
       output = output->make("ArgMin",ScalarVal);
       return output;
     }
     case 7://Sum
     {  
       output = output->make("Sum", ScalarVal);
       return output;
     }
     default://Forall
     {  
       output = output->make("Forall", pointset);
       return output;
     }
   }
   return output;

  }

  /* IR node generation for datasets */
  Nbody::IRNode* Handle_dataset_generation(int i, int s) {
    Nbody::DataPoint* Data = new Nbody::DataPoint();
    Data =   Data->make(Dataset[i], s, dim,i);
    return Data;
  }

  /* IR node generation for distance function */
  Nbody::IRNode*  Handle_distance_generation() {
    Nbody::distance* dist =  new Nbody::distance();
    dist = dist->make(DVars[0], DVars[1], DistanceInput);
    return dist;

    
  }
  
  /*IR node generation for condition function */
  Nbody::IRNode* Handle_condition_generation(string N) {
   Nbody::condition* cond = new Nbody::condition();
   cond->make(N);
   return cond;
  }

 /* IR node generation for Kernel function */
  Nbody::IRNode*  Handle_kernel_generation() {
    Nbody::IRNode* KernelNode = new Nbody::kernelIR();
    return KernelNode;
  }


  Nbody::IRNode*  Handle_kernel_generation(string inputKernel, int argc) {
    Kernel* k = Reader(inputKernel);
    Nbody::kernelIR* KernelNode = new Nbody::kernelIR();
    KernelNode->name = inputKernel;
    KernelNode->make(inputKernel, k);
    return KernelNode;
  }


//==================== printing the first IR level ============== //
  //TODO -> if prune or approximate for this algorithm
  bool prune_approx() {
    return true;
  }

/* printing the BaseCase for the skeleton of NBody*/
  void Base_case_printer(Nbody::IRPrinter* P){

    cout  << endl;

    P->indent();   
    Nbody::IRNode* D1 = Handle_dataset_generation(0, DatasetSize[0]);
    D1->accept(P);
    P->indent();
    Nbody::IRNode* D2 = Handle_dataset_generation(1, DatasetSize[1]);
    D2->accept(P);
    P->indent(); 
    
    cout  << endl;
   Nbody::IRNode* a; 
    for (int i = 0; i < NumofOp; ++i) {
      P->indent_level++;
      P->indent();
      a = Handle_op_generation(Op[i],0,DatasetSize[i], 1);
      a->accept(P);
      cout  << endl;
    }
  
    P->indent();
    Nbody::IRNode* Dist =  Handle_distance_generation();
    Dist->accept(P);
    P->indent();
    P->indent_level-= 2;
    Nbody::IRNode* kern =  Handle_kernel_generation(KernelInput, 1);
    kern->accept(P);

    Nbody::IRNode* k;
    for(int i = 0; i <NumofOp; ++i) {
      k = Handle_output_generation(Op[NumofOp - 1 - i]);
      k->accept(P);
    }
      P->indent_level-=12;
      P->indent();
      cout << "}" << endl;
  }


  void Print_IR(Nbody::IRPrinter* P) {
      
    Base_case_printer(P);
 
  } 

//================= End of prining functions

  void compile_to_file() {
    std::ostream& s = std::cout;
    Nbody::IRPrinter* printer = new Nbody::IRPrinter(s);
    
    Print_IR(printer);
  }

  /*  Checkes if the kernel provided is a match for our framework*/
  bool Kernel_checker() {
    bool check = false;
    return check;
  }

  void Prune_generator() {
   /* generating the prune condition */
  }

  void Nbody_compute() {
    Points s_perm(Dataset[0].size(),dim);
    Points t_perm(Dataset[1].size(), dim);
    compile_to_file();
  }
}
#endif

function y = matchings(f1, f2, d1, d2)

% use pdist2 for euclidean distance

d1 = double(d1);
d2 = double(d2);
keypoints1 = double(f1(1:2, :));
keypoints2 = double(f2(1:2, :));





matches = 0; 
T1 = 500; 
T2 = 16; 

% find keypoint matches
for i = 1: size(keypoints1,2)
    
    
    for j = 1 : size(keypoints2,2)
        
%                 if (norm(d1(:, i)-d2(:, j) ) < 40)
%                   fprintf('%f\n', norm(d1(:, i)-d2(:, j) )); 
%                 end
                
%                 if( norm(keypoints1(:, i)- keypoints2(:, j)) <= T2)
%                     fprintf('%f\n', norm(keypoints1(:, i)- keypoints2(:, j))); 
%                 end

        if (  norm(d1(:, i)-d2(:, j) ) <= T1 ...
                && norm(keypoints1(:, i)- keypoints2(:, j)) <= T2)
            matches = matches + 1; 
            % ITS A MATCH
        end
    end
    
    
end


y = matches; 



end

function D = eDist(A,B)


n = size(A, 1); 
D = 0; 
if (n ~= size(B,1))
    fprintf('error, vector sizes do not match');
    return; 
else
    for i = 1 : n
        
    D = D + (A(i) - B(i))^2; 
        
    end
    
    D = sqrt(D); 
    
end



end


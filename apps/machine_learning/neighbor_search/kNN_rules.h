#ifndef _KNN_RULES_H_
#define _KNN_RULES_H_
#define prune_type unsigned long long

#include <cfloat>

#include "Clock.hpp"
#include "kNN_vector.h"
#include "Metric.h"
#include <omp.h>

typedef Metric<Point, Point> MetricType;
typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;

/**
 * \brief kNN-distance structure. References a feature vector by its index and its squared distance to another implicit vector.
 *
 * Implements its own comparison function with the parenthesis operator for STL-based algorithm use.
 *
 * \param T  Type used to encode the distance between two feature vectors. Should be the same than the data from the vectors.
 */
class kNN_distance {
  public:
    real_t squared_distance; 	/* Squared distance of the referenced element to an implicit point */
    int index; 	/* Index of the feature vector in the data set */

    /* Default and convenience constructors */
    kNN_distance() : squared_distance(DBL_MAX), index(-1) {}
    kNN_distance(int i, real_t sq_dist) : squared_distance(sq_dist), index(i) {}

    /* Distance comparison operator for kNN_distances. Allows kNN_distance objects to be used as STL comparison functors */
    bool operator< (const kNN_distance &v) const {
      return squared_distance < v.squared_distance;
    }
};

typedef vector<kNN_distance> kNeighbors;
typedef vector<kNeighbors> resultNeighbors;
typedef kNN_vector<kNN_distance> kvector;


/* Helper structure for tree traversal and incremental hyperrectangle-hypersphere intersection data */
template <typename Tree, typename S, typename C = kNN_vector<kNN_distance> >
class kNN_rules {
  public:
    typedef typename Tree::NodeTree Box;
    int dim;

    /* Number of neighbours to retrieve */
    unsigned int k;
    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;

    long num_base = 0;
    long num_visit = 0;
    long num_prunes = 0;
    int counter = 0;
    long basecase[32];

    /* Points to permutated source array */
    Points_t& src_data;
    /* Reference input target array */
    Points_t& trg_data;

    MetricType* metric;

    /* Current k neighbor indices */
    vector<C>& neighbors;

    /* prune generation would be used */
    int pg_rule = 0;

    /* Current distance from the farthest nearest neighbour to the reference point */
    std::vector<real_t> farthest_distance;

    /* two constructor for two cases of: 1- using  prune generator class and 2- embeded prune */
    /* two constructor for two cases of: 1- using  prune generator class and 2- embeded prune */
    kNN_rules(int d, unsigned int k, Tree& st, Tree& tt, Points_t& s, Points_t& t, MetricType* m, vector<C>& n)
        : dim(d), k(k), src_tree(st), trg_tree(tt), src_data(s), trg_data(t), metric(m), neighbors(n),
            farthest_distance(trg_tree.points_column_major(), DBL_MAX) {
            // farthest_distance(trg_tree.points(), DBL_MAX) {
            for (size_t i= 0; i < 32; i++ )
              basecase[i] = 0;
          };

    /* Base case calculation */
    void base_case (Box& s, int t);
    void base_case (Box& s, Box& t);
    void base_case (vector<Box*> t);

    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);
    bool prune_subtree (Box& s, Box& t);
    bool prune_subtree (vector<Box*> t);

    /* Calculate which branch to visit first */

    std::vector<int> visit (Box& s, int t);
    std::vector<int> visit (Box& s, Box& t);

    /* TODO */
    /* void visit (vector<Box*>& t)
     * reorder the vector of Box*, which is the child nodes
     */
    std::vector<int> visit (vector<Box*> t, int div);

    /* Update distance */
    void update_state (Box& s, Box& t);
    void update_state (vector<Box*> t);

    /* Updaee the current value of the distance to the nearest point in the hyperrectangle */
    real_t distance_update (Box& s, int t);
    real_t distance_update (Box& s, Box& t);
    real_t distance_update (vector<Box*> t);

    /* Calculate the min, max k-th nn distance for all the points in a box */
    pair<real_t, real_t> calculate_nearest_farthest_distance (Box& t);
    real_t calculate_farthest_distance (Box& t);

    /* centorid case computation for the subtree is will be prunes, in this case since it's prune it does nothing*/
    void centroid_case (Box& s, int t) {};
    void centroid_case (Box& s, Box& t) {};
    void centroid_case (vector<Box*> t) {};

    void set_temp(int temp) {};
    void adjustPartitionCounter(int , int , int , int){};
    double get_temp(){};


  struct {
        bool operator()(std::pair<real_t, int> a, std::pair<real_t, int> b) const{return a.first < b.first;}
  } customLess;
};

// Template implementation

template<typename Tree, typename S, typename C>
void
kNN_rules<Tree,S,C>::base_case (Box& s, int t) {
  SquaredEuclideanMetric<Point, Point> metric;
  real_t dist;
  for (size_t j = s.begin(); j < s.end(); ++j) {
    dist = metric.compute_distance (trg_data[t], src_data[j]);
    neighbors[t].push_back(kNN_distance(j, dist));
  }
  /* Update current farthest nearest neighbor distance */
  farthest_distance[t] = neighbors[t].back().squared_distance;
}



template<typename Tree, typename S, typename C>
void
kNN_rules<Tree,S,C>::base_case (Box& s, Box& t) {
  SquaredEuclideanMetric<Point, Point> metric;
  real_t dist;

  for (size_t i = t.begin(); i < t.end(); ++i) {
    dist = s.min_distance(trg_data,i);
    // dist = s.min_distance(trg_data[i]);
   if (dist > farthest_distance[i]) {
     // cout << "cutted: " << i << " , " << s.begin() << " - " << s.end() << " -> " << dist << " > " << farthest_distance[i] << "\n";
    continue;
   }

    for (size_t j = s.begin(); j < s.end(); ++j) {
      dist = 0;
      for (size_t d = 0; d < dim; ++d) {
        // dist += (src_data[j][d] - trg_data[i][d]) * (src_data[j][d] - trg_data[i][d]);
        dist += (src_data[d][j] - trg_data[d][i]) * (src_data[d][j] - trg_data[d][i]);
      }
      neighbors[i].push_back(kNN_distance(j, dist));
    }
    farthest_distance[i] = neighbors[i].back().squared_distance;
  }
}

  /* k is set to 1 for KNN with two dataset and has been used
  * for the multi-tree, in the case of an algorithm with more
  * than two dataset the kernel function and k should have
  * change accordingly
  */

template<typename Tree, typename S, typename C>
void
kNN_rules<Tree,S,C>::base_case (vector<Box*> s) {
  SquaredEuclideanMetric<Point, Point> metric;
  real_t dist;

  int k = 1;
  for (size_t i = s[k]->begin(); i < s[k]->end(); ++i) {
    /* Check if this target point can be improved further */
    dist = s[k-1]->min_distance(trg_data[i]);
    if (dist > farthest_distance[i])
      continue;

    for (size_t j = s[k-1]->begin(); j < s[k-1]->end(); ++j) {
      dist = metric.compute_distance (trg_data[i], src_data[j]);
      neighbors[i].push_back(kNN_distance(j, dist));
    }
    /* Update current farthest nearest neighbor distance */
    farthest_distance[i] = neighbors[i].back().squared_distance;
  }
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S, typename C>
inline
bool
kNN_rules<Tree,S,C>::prune_subtree (Box& s, int t) {
  int i = s.index();
  return s.min_distance(trg_data,t) >= farthest_distance[t];
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S, typename C>
inline
bool
kNN_rules<Tree,S,C>::prune_subtree (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  auto nearest_farthest = calculate_nearest_farthest_distance(t);
  real_t nearest_dist = nearest_farthest.first;
  real_t farthest_dist = nearest_farthest.second;
  // real_t tight_bound = min (farthest_dist, nearest_dist + 2 * t.maxdist);
  real_t tight_bound = farthest_dist;
  if (t.min_distance(s) >= tight_bound) {
    return true;
  }

  return false;
}

  /* k is set to 1 for KNN with two dataset and has been used
  * for the multi-tree, in the case of an algorithm with more
  * than two dataset the kernel function and k should have
  * change accordingly
  */
template<typename Tree, typename S, typename C>
inline
bool
kNN_rules<Tree,S,C>::prune_subtree (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  real_t farthest_dist = calculate_nearest_farthest_distance(*s[k]).second;
  return s[k]->min_distance(*s[k-1]) >= farthest_dist;
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S, typename C>
inline
std::vector<int>
kNN_rules<Tree,S,C>::visit (Box& s, int t) {
  real_t left_score  = distance_update(src_tree.node_data[s.child  ], t);
  real_t right_score = distance_update(src_tree.node_data[s.child+1], t);
  if (right_score < left_score)
      return vector<int> {1,0};
    else
      return vector<int> {0,1};
}

/* Return the visiting order of the child nodes */
template<typename Tree, typename S, typename C>
std::vector<int>
kNN_rules<Tree,S,C>::visit (Box& s, Box& t) {
      // return vector<int> {0,1};

      std::vector<int> visit_order;
      std::vector<std::pair<real_t, int> > dist_index;
      for (int i = 0; i < s.num_child(); i++) {
        real_t dist = distance_update(src_tree.node_data[s.child+i], t);
        dist_index.push_back(std::make_pair(dist, i) );
      }
      std::sort(dist_index.begin(), dist_index.end(), customLess);
      for (int i = 0; i < dist_index.size(); i++)
        visit_order.push_back(dist_index[i].second);

      /*  for comparison of visit order outputed by sort kd-tree vs. what is expected*/
      #ifdef _DEBUG
        real_t left_score  = distance_update(src_tree.node_data[s.child  ], t);
        real_t right_score = distance_update(src_tree.node_data[s.child+1], t);
        if (right_score < left_score) {
          if ((visit_order[0] !=1) or (visit_order[1] !=0)) {
            cout << s.index() << " , " << t.index() <<
                       " : <" << visit_order[0]  << " , "<< visit_order[1] << "> , (1,0)" << endl;
          exit(0);
          }
        }
        else {
          if ((visit_order[0] !=0) or (visit_order[1] !=1)) {
            cout << s.index() << " , " << t.index() <<
                       " : <" << visit_order[0]  << " , "<< visit_order[1] << "> , (0,1)" << endl;
          exit(0);
          }
        }

      #endif

      return visit_order;

}


template<typename Tree, typename S, typename C>
inline
vector<int>
kNN_rules<Tree,S,C>::visit (vector<Box*> s, int div) {
  std::vector<int> visit_order;
  std::vector<std::pair<real_t, int> > dist_index;
  /* Decide which box to compare with */
  int next = (div == s.size() - 1) ? div - 1 : div + 1;
  for (int i = 0; i < s[div]->num_child(); i++) {
    real_t dist = distance_update(src_tree.node_data[s[div]->child+i],
                                   *s[next]);
    dist_index.push_back(std::make_pair(dist, i) );
  }

  std::sort(dist_index.begin(), dist_index.end() );
  for (int i = 0; i < dist_index.size(); i++)
    visit_order.push_back(dist_index[i].second);
  return visit_order;

}

/**
 * Update distance information.
 */
template<typename Tree, typename S, typename C>
inline
void
kNN_rules<Tree,S,C>::update_state (Box& s, Box& t) {
  for (int i = 0; i < t.num_child(); i++) {
    distance_update(s, trg_tree.node_data[t.child + i]);
  }
}

template<typename Tree, typename S, typename C>
inline
void
kNN_rules<Tree,S,C>::update_state (vector<Box*> s) {
  int k = 1;
  distance_update(s[k-1], trg_tree.node_data[s[k]->child  ]);
  distance_update(s[k-1], trg_tree.node_data[s[k]->child+1]);
}

/**
 * Intersection data is updated.
 */
template<typename Tree, typename S, typename C>
real_t
kNN_rules<Tree,S,C>::distance_update (Box& s, int t) {
  int i = s.index();
  return s.min_distance(trg_data,t);
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S, typename C>
real_t
kNN_rules<Tree,S,C>::distance_update (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  real_t dist = t.min_distance(s);
  return dist;
}

template<typename Tree, typename S, typename C>
real_t
kNN_rules<Tree,S,C>::distance_update (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  return s[k]->min_distance(s[k-1]);
}
/* Calculate the min, max k-th nn distance for all the points in a box */
template<typename Tree, typename S, typename C>
pair<real_t, real_t>
kNN_rules<Tree,S,C>::calculate_nearest_farthest_distance (Box& t) {

  real_t near_dist = DBL_MAX;
  real_t far_dist = 0;

  for (size_t i = t.begin(); i < t.end(); ++i) {
    if (farthest_distance[i] > far_dist)
      far_dist = farthest_distance[i];
    if (farthest_distance[i] < near_dist)
      near_dist = farthest_distance[i];
  }
  return make_pair(near_dist, far_dist);
}


#endif

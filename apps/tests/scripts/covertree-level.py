#!/usr/bin/env python

import os
from os import path
import time
import argparse
import csv

# ----- main directories for executions ----- #
TOP_DIR = "/home/Laleh/Workspace/Laleh/Nbody-ML/"
RUN_DIR = TOP_DIR + "apps/tests/library"
OUT_DIR = TOP_DIR +  "apps/tests/results/"
DATASET_DIR = "/home/real_datasets/synthetic/point/"
RESULTS_DIR = TOP_DIR +  "apps/tests/"


# ----- setups for scaling experiments ----- #
TREES = ["covertree"]

TRAVERSALS = ["yes"]

EXPR_NUMBERS = [1, 2, 3]
LEVELS = [1, 2, 4, 8, 10, 16, 32, 64, 100, 128]

# ----- threads for scaling -----#
THREADS = [32]

# ---- very small data set jut for checking ----- #

# ---- real data sets with smaller size ----- #
DATASETS = ["G2-D2-1000000"]
DIM = [2]
SIZE = [1000000]



# ----- scaling function  for kNN algorithm ----- #
def kNN_covertree():
  for traversal in TRAVERSALS:
    for levelP in LEVELS:
      level = 0
      for dataset in DATASETS:
        for threads in THREADS:
          for expNum in EXPR_NUMBERS:
          
            outputName = OUT_DIR + "perf-kNN-covertree-%s-th%d-expr%d-level%d.csv"%(dataset, threads, expNum, levelP)
            datasetName = DATASET_DIR + dataset + ".csv"
            #command = "(env CILK_NWORKERS=%d  S=no %s/test--kNN_kdtree %d %d %s 3 %d 2>&1 | tee -a  %s)" \
            command = "(env OMP_NUM_THREADS=%d S=no %s/test--kNN_covertree %d %d %s 3 %d 2>&1 | tee -a  %s)" \
                         % (threads, RUN_DIR, SIZE[level], DIM[level], datasetName, levelP, outputName)
            print command
            os.system(command) 

        level = level + 1




def main():
  # ----- making a directory for results ----- #
  output_dir = RESULTS_DIR + "results"
  os.system("mkdir " + output_dir )
  # ----- call to seperate functions for each algorithm ----- #
  kNN_covertree()

if __name__ == "__main__":
  main()



#ifndef _BORUVKA_EMST_H_
#define _BORUVKA_EMST_H_

#define counter_type unsigned long long

#include "boruvka_emst_rules.h"
#include "Single_tree_traversal_Pascal.h"
#include "Dual_tree_traversal_Pascal.h"
#include "Multi_tree_traversal_Pascal.h"
#include "Metric.h"
#include "edge_pair.h"
#include "Clock.hpp"

template <typename Tree>
class MST {
  public:
    int dim;

    /* Total distance of the tree */
    real_t total_dist;

    /* Pointer to the tree */
    Tree& tree;

    /* level for traversal */
    int level;

    /* The metric for distance calculations */
    MetricType* metric;

    int multi;

    /* Edges */
    std::vector<EdgePair> edges;

    MST(int n, int d, Tree& t) : dim(d), total_dist(0.0), tree(t), metric(new EuclideanMetricType()) {
      edges.reserve(n - 1);
    }

    MST(int n, int l, int d, Tree& t) : dim(d), level(l),  total_dist(0.0), tree(t), metric(new EuclideanMetricType()) {
      edges.reserve(n - 1);
    }
    MST(int n, int l, int d, Tree& t, int multi_tree) : dim(d), level(l), total_dist(0.0), tree(t), multi(multi_tree), metric(new EuclideanMetricType()) {
      edges.reserve(n - 1);
    }
    MST(int n, int d, Tree& t, MetricType* m) : dim(d), total_dist(0.0), tree(t), metric(m) {
      edges.reserve(n - 1);
    }

    /* Compute the minimum spanning tree */
    void compute_mst (Mat& results);

    /* Add a single edge to the edge list */
    void add_edge(const size_t e1, const size_t e2, const real_t dist);

    /* Add all edges found to the list of neighbors */
    void add_all_edges(UnionFind& connections, Neighbors& neighbors);

    /* Reset the values in the tree after each iteration */
    void reset(UnionFind& connections, Neighbors& neighbors, fixed_vector<int>& component_index);

    /* Helper to reset the nearest neighbor distance and check for fully connected nodes */
    void reset_helper(Box& box, UnionFind& connections, fixed_vector<int>& component_index);

    /* Unpermute the edge list (if necessary) and output the results */
    void compute_result(Mat& results);
};

// Template instantiation

/**
 * Iteratively compute the minimum spanning tree. The results are stored in a
 * #edges x 3 matrix whose three attributes are in edge, out edge, and distance
 * between the two edges.
 *
 * \param results   Matrix where the results are stored
 */

template <typename Tree>
void
MST<Tree>::compute_mst (Mat& results) {
  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  counter_type num_prunes = 0;
  int num_pts = tree.points();

  /* Create a vector to store the current nearest neighbors */
  Neighbors current(num_pts);
  for (size_t i = 0; i < tree.points(); ++i)
    current[i].squared_distance = DBL_MAX;
  UnionFind connections(num_pts);

  /* Set the component index for all nodes */
  fixed_vector<int> component_index(tree.nodes());
  vector<Box>& node = tree.node_data;
  for(size_t i = 0; i < tree.nodes(); ++i) {
    size_t npoints = node[i].end() - node[i].begin();
    component_index[i] = ((npoints == 1) && node[i].is_leaf()) ? node[i].begin() : -1;
  }

  Clock timer;
  if (single_mode) {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef MSTRules<Tree, Vec> Rule;
    Rule rules(dim, tree, tree.data_perm, connections, current, component_index, metric);

    while (edges.size() < (num_pts - 1)) {
      /* Create the traverser */
      SingleTreeTraversal<Tree, Rule> traverser(tree, rules, num_prunes);

      for (size_t i = 0; i < num_pts; ++i) {
        /* Start traversal from the root of the tree */
        traverser.traverse (tree.root(), i);
      }

      add_all_edges(connections, current);
      reset(connections, current, component_index);
    }
  }
  else {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef MSTRules<Tree, Mat> Rule;
    Rule rules(dim, tree, tree.data_perm, connections, current, component_index, metric);


    while (edges.size() < (num_pts - 1)) {
      if (multi == 0) {
        /* Create the traverser */
        DualTreeTraversal<Tree, Rule, Rule> traverser(tree, tree, rules, rules, num_prunes, level);

        /* Start traversal from the root of the tree */
        traverser.traverse (tree.root(), tree.root());

       }
       else {
         vector<Tree*> trees;
         trees.push_back(&tree);
         trees.push_back(&tree);

         /* Create the traverser */
         MultiTreeTraversal<Tree, Rule, Rule> traverserM(trees, rules, rules, num_prunes, level);

         /* Start traversal from the root of the trees */
         traverserM.traverse ();
       }
      add_all_edges(connections, current);
      reset(connections, current, component_index);
    }
  }
  compute_result(results);

  double time_rs = timer.seconds();
  if (single_mode)
    cerr << "Single-tree Boruvka minimum spanning tree time: " << time_rs << " seconds\n";
  else if (!multi)
    cerr << "Dual-tree emst time: " << time_rs << " seconds\n";
  else
     cerr << "Multi-tree emst time: " << time_rs << " seconds\n";

  cout << "Total Distance: " << total_dist << endl;
}

/* Add all edges found to the list of neighbors */
template <typename Tree>
void
MST<Tree>::add_all_edges(UnionFind& connections, Neighbors& neighbors) {
  for (size_t i = 0; i < tree.points(); ++i) {
    size_t component = connections.find(i);
    size_t inedge = neighbors[component].in;
    size_t outedge = neighbors[component].out;
    if (connections.find(inedge) != connections.find(outedge)) {
      total_dist += neighbors[component].squared_distance;
      add_edge(inedge, outedge, neighbors[component].squared_distance);
      connections.merge(inedge, outedge);
    }
  }
}

/* Add a single edge to the edge list */
template <typename Tree>
void
MST<Tree>::add_edge(const size_t e1, const size_t e2, const real_t dist) {
  assert (dist >= 0 && "Edge distance cannot be negative");
  if (e1 < e2)
    edges.push_back(EdgePair(e1, e2, dist));
  else
    edges.push_back(EdgePair(e2, e1, dist));
}


/* Reset the values stored in the tree after each iteration */
template <typename Tree>
void
MST<Tree>::reset(UnionFind& connections, Neighbors& neighbors, fixed_vector<int>& component_index) {
  for (size_t i = 0; i < tree.points(); ++i) {
    neighbors[i].squared_distance = DBL_MAX;
    reset_helper(tree.root(), connections, component_index);
  }
}

/* Reset helper */
template <typename Tree>
void
MST<Tree>::reset_helper(Box& box, UnionFind& connections, fixed_vector<int>& component_index) {
  vector<Box>& nodes = tree.node_data;
  /* Recurse to the leaf nodes */
  if (!box.is_leaf()) {
    for (int i = 0; i < box.num_child(); i++)
      reset_helper(nodes[box.child+i], connections, component_index);
  }

  /* Get the component of the the first child or point */
  const int component = !box.is_leaf() ? component_index[box.child] : connections.find(box.begin());

  /* Check if all the components of children are the same */
  if (!box.is_leaf()) {
    for (int i = 0; i < box.num_child(); i++) {
      if (component_index[box.child+i] != component)
        return;
    }
  }

  /* Check if all the points are in the same component */
  for (size_t i = box.begin(); i < box.end(); ++i)
    if (connections.find(i) != component)
      return;

  /* If the above checks passed, all components are the same */
  component_index[box.index()] = component;
}

/* Unpermute the edge list and output the results */
template <typename Tree>
void
MST<Tree>::compute_result(Mat& result) {
  /* Sort the edges */
  std::sort (edges.begin(), edges.end(), CompEdges);

  for (size_t i = 0; i < tree.points()-1; ++i) {
    size_t index1 = tree.index[edges[i].first()];
    size_t index2 = tree.index[edges[i].second()];

    if (index1 < index2) {
      edges[i].first() = index1;
      edges[i].second() = index2;
    }
    else {
      edges[i].first() = index2;
      edges[i].second() = index1;
    }

    result[i][0] = edges[i].first();
    result[i][1] = edges[i].second();
    result[i][2] = edges[i].distance();
  }
}
#endif

#include "ValueStorage.h"

template <typename T>
ValueStorage<T>::ValueStorage(){
	
}

template <typename T>
ValueStorage<T>::ValueStorage(T item){
	this->storedItem = item;
}

template <typename T>
T ValueStorage<T>::value(){
	return this->storedItem;
}
#include "PortalFunction.h"

PortalFunction::PortalFunction(){

}

PortalFunction::PortalFunction(TYPE t):type(t){

}

PortalFunction::PortalFunction(Nbody::Expr external):externalFunction(external){
	type = TYPE::EXPR;
}

/**
Creates the Expr of the PortalFunction

Args
	setInfo - input from NBodyLower, used to determine what the inputs are and the dimensionality of the problem
**/
Nbody::Expr PortalFunction::getExpr(std::vector<std::pair<Nbody::Expr , int>> setInfo) {
	if(this->type == TYPE::EXPR){
		return this->externalFunction;
	}
	Nbody::Expr result;
	Nbody::Expr tempCalc;
	Nbody::Stmt temp;
	int m = this->getSmallestDimensionality(setInfo);
	int i;
	switch(this->type){
		case SQRDIST:
			assert(setInfo.size() == 2 && "SQRDIST PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
				          - Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = Nbody::pow(tempCalc , Nbody::Integer::make(2));
				else result += Nbody::pow(tempCalc , Nbody::Integer::make(2));
			}
			return result;
		case EUDIST:
			assert(setInfo.size() == 2 && "EUDIST PortalFunction requires layer size of two.");
		case EUCLIDEAN:
			assert(setInfo.size() == 2 && "EUCLIDEAN PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
				          - Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = Nbody::pow(tempCalc , Nbody::Integer::make(2));
				else result += Nbody::pow(tempCalc , Nbody::Integer::make(2));
			}
			return Nbody::sqrt(result);
		case MANAHATTAN:
			assert(setInfo.size() == 2 && "MANAHATTAN PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
				          - Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = Nbody::abs(tempCalc);
				else result += Nbody::abs(tempCalc);
			}
			return result;
		case CHEBYSHEV:
			assert(setInfo.size() == 2 && "CHEBYSHEV PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
				          - Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = tempCalc;
				else result = Nbody::max(Nbody::abs(tempCalc) , result);
			}
			return result;
		case DETERMINANT:
				tempCalc = Determinant(setInfo[0].first, 2);
				return result;
		case INVERSE:
				temp = Inverse(setInfo[0].first, 2);
				return result;
		case TRANSPOSE:
				temp = Transpose(setInfo[0].first, 2);
				return result;


	}
	return result;
}



std::vector<int> PortalFunction::getCoFactor(std::vector<int>  FlatMatrix, int p, int q, int n) {
	std::vector<int> result;
	for (int i = 0; i < FlatMatrix.size(); i++){
		if ((FlatMatrix[i] / n != p) && (FlatMatrix[i] % n != q))
		result.push_back(FlatMatrix[i]);
	}
 return result;
}

Nbody::Expr PortalFunction::Determinant(Nbody::Expr Mat , int size) {
	std::vector<int> flatMat;
	for (int i = 0; i < (size * size); i++)
			flatMat.push_back(i);
	return Determinant(Mat, flatMat, size);
}

Nbody::Expr PortalFunction::Determinant(Nbody::Expr Mat, std::vector<int> flatMat , int size) {
	Nbody::Expr result;
	Nbody::Expr sign = Nbody::Integer::make(1);
	Nbody::Expr tempCalc;
	if (size == 1)
	  return Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(flatMat[0]) , Mat , Nbody::Integer::make(flatMat[0]));

	for (int i = 0; i < size; i++){
		tempCalc = Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(flatMat[i]) , Mat , Nbody::Integer::make(flatMat[i]))
		         * Determinant(Mat, getCoFactor(flatMat, flatMat[0], flatMat[i], size-1), size-1);
		result += Nbody::Mul::make(sign, tempCalc);
		sign    = Nbody::Mul::make(sign, Nbody::Integer::make(-1));
	}
}




Nbody::Stmt PortalFunction::Transpose(Nbody::Expr Mat, int size) {

  Nbody::Expr temp1;
	Nbody::Expr temp2;
	std::vector<Nbody::Stmt> out;
	Nbody::Stmt out1;
	Nbody::Stmt out2;
	for (int i = 0; i < size ; i++){
		for (int j = 0; j < i ; j++){
			temp1 = Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , Mat , Nbody::Integer::make((i*size + j)));
			temp2 = Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , Mat , Nbody::Integer::make((j*size + i)));
			out1  = Nbody::Store::make("s0"+std::to_string(i) , Mat, temp2 , Nbody::Integer::make((i*size + j)));
			out2  = Nbody::Store::make("s0"+std::to_string(i) , Mat, temp1 , Nbody::Integer::make((j*size + i)));
			out.push_back(out1);
			out.push_back(out2);
		}
	}
  Nbody::Stmt result = Nbody::BlockStmt::make(out);
	return result;

}
/*
Inverse has four main steps:
  - matrix of minors:
	        for each cell  ignore the values in the current row and columns, and calculate
					the determinant using the remaining values and replace it for the current cell

	- matrix of cofactors :
	        apply a "checkerboard" of minuses to the "Matrix of Minors". In other words, we need to change the sign of alternate cells

	- adjugate :
	       "Transpose" all elements of the previous matrix

	- multiply by 1/determinant
*/
Nbody::Stmt PortalFunction::Inverse(Nbody::Expr Mat, int size) {
  std::vector<Nbody::Stmt> result;
	std::vector<int> flatMat;
	for (int i = 0; i < (size * size); i++)
			flatMat.push_back(i);
	/* computing the matrix of minors */
	Nbody::Stmt out = MatrixOfMinor(Mat,flatMat,size);
	result.push_back(out);

  /* generating the matrix of cofactors */
	Nbody::Expr temp1;
	Nbody::Stmt out1;
	Nbody::Expr sign = Nbody::Integer::make(1);
	for (int i = 0; i < size ; i++){
		for (int j = 0; j < i ; j++){
			temp1 = Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , Mat , Nbody::Integer::make((i*size + j)));
			temp1 = Nbody::Mul::make(sign, temp1);
			out1  = Nbody::Store::make("s0"+std::to_string(i) , Mat, temp1 , Nbody::Integer::make((i*size + j)));
			result.push_back(out1);
			sign    = Nbody::Mul::make(sign, Nbody::Integer::make(-1));
		}
	}
  /* making the adjogate matrix */

	out = Transpose(Mat, size);
	result.push_back(out);

	/* multiply by determinant */

	Nbody::Expr determinant = Determinant(Mat, size);
	for (int i = 0; i < size ; i++){
		for (int j = 0; j < i ; j++){
			temp1 = Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , Mat , Nbody::Integer::make((i*size + j)));
			temp1 = Nbody::Mul::make(determinant, temp1);
			out1  = Nbody::Store::make("s0"+std::to_string(i) , Mat, temp1 , Nbody::Integer::make((i*size + j)));
			result.push_back(out1);
		}
	}

  Nbody::Stmt result_final = Nbody::BlockStmt::make(result);
	return result_final;
}

Nbody::Stmt PortalFunction::MatrixOfMinor(Nbody::Expr Mat, std::vector<int> flatMat, int size) {
  std::vector<Nbody::Stmt> out;
	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++) {
			int k = (i*(size-1))+j;
			Nbody::Expr temp = Determinant(Mat, getCoFactor(flatMat, flatMat[i], flatMat[k], size-1), size-1);
			Nbody::Stmt outResult = Nbody::Store::make("s0"+std::to_string(i) , Mat, temp , Nbody::Integer::make((i*size + j)));
			out.push_back(outResult);
		}
	}
	Nbody::Stmt result = Nbody::BlockStmt::make(out);
	return result;
}
Nbody::Expr PortalFunction::Sum(Nbody::Expr e, int size) {
	Nbody::Expr result;
	for (int i = 0; i < size; i++){
			Nbody::Expr temp = Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , e , Nbody::Integer::make((i)));
			result += temp;
	}
	return result;
}


Nbody::Expr PortalFunction::getExpr(Nbody::Expr a) {
	if(this->type == TYPE::EXPR){
		return this->externalFunction;
	}
	std::vector<std::pair<Nbody::Expr , int>> setInfo;
	Nbody::Expr result;
	Nbody::Expr tempCalc;
	Nbody::Stmt temp;
	int m = this->getSmallestDimensionality(setInfo);
	int i;
	switch(this->type){
		case SQRDIST:
			assert(setInfo.size() == 2 && "SQRDIST PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
									- Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = Nbody::pow(tempCalc , Nbody::Integer::make(2));
				else result += Nbody::pow(tempCalc , Nbody::Integer::make(2));
			}
			return result;
		case EUDIST:
			assert(setInfo.size() == 2 && "EUDIST PortalFunction requires layer size of two.");
		case EUCLIDEAN:
			assert(setInfo.size() == 2 && "EUCLIDEAN PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
									- Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = Nbody::pow(tempCalc , Nbody::Integer::make(2));
				else result += Nbody::pow(tempCalc , Nbody::Integer::make(2));
			}
			return Nbody::sqrt(result);
		case MANAHATTAN:
			assert(setInfo.size() == 2 && "MANAHATTAN PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
									- Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = Nbody::abs(tempCalc);
				else result += Nbody::abs(tempCalc);
			}
			return result;
		case CHEBYSHEV:
			assert(setInfo.size() == 2 && "CHEBYSHEV PortalFunction requires layer size of two.");
			for(i = 0; i < m; ++i){
				tempCalc =  Nbody::Load::make(Nbody::Float(32) , "s0"+std::to_string(i) , setInfo[0].first , Nbody::Integer::make(i))
									- Nbody::Load::make(Nbody::Float(32) , "s1"+std::to_string(i) , setInfo[1].first , Nbody::Integer::make(i));
				if(i == 0) result = tempCalc;
				else result = Nbody::max(Nbody::abs(tempCalc) , result);
			}
			return result;
		case DETERMINANT:
				tempCalc = Determinant(setInfo[0].first, 2);
				return result;
		case INVERSE:
				temp = Inverse(setInfo[0].first, 2);
				return result;
		case TRANSPOSE:
				temp = Transpose(setInfo[0].first, 2);
				return result;
		case SUM:
				result = Sum(setInfo[0].first, setInfo[0].second);
				return result;



	}
	return result;
}

/**
Helper function to determine what the smallest number of elements a point in the problem has

Args
	setInfo - input from NBodyLower, used to determine what the inputs are and the dimensionality of the problem
**/
int PortalFunction::getSmallestDimensionality(std::vector<std::pair<Nbody::Expr , int>> setInfo) const{
	int min;
	for(int i = 0; i < setInfo.size(); ++i){
		if(i == 0){
			min = setInfo[i].second;
		}
		else if(min > setInfo[i].second){
			min = setInfo[i].second;
		}
	}
	return min;
}

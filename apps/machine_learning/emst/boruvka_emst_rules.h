#ifndef _BORUVKA_EMST_RULES_H_
#define _BORUVKA_EMST_RULES_H_

#include <cfloat>
#include <algorithm>
// #include "Binary_tree.h"
#include "Metric.h"
#include "union_find.h"

typedef Metric<Point, Point> MetricType;
typedef EuclideanMetric<Point, Point> EuclideanMetricType;

/**
 * \brief ComponentData structure. References a component data by edge indices and its squared edge distance.
 */
class ComponentData {
  public:
    size_t in; 	/* In edge index */
    size_t out; 	/* Out edge index */
    real_t squared_distance; 	/* Squared edge distance */

    /* Default and convenience constructors */
    ComponentData() {}
    ComponentData(size_t in, size_t out, real_t sq_dist) : in(in), out(out), squared_distance(sq_dist) {}

};

typedef vector<ComponentData> Neighbors;

/* Helper structure for tree traversal and intersection data */
template <typename Tree, typename S>
class MSTRules {
  public:
    typedef typename Tree::NodeTree Box;
    int dim;

    /* Pointer to the tree */
    Tree& tree;

    /* Points to permutated dataset */
    Points_t& data;

    UnionFind& connections;

    /* Current neighbor index */
    Neighbors& neighbors;

    /* The index of the component that all points in this node belong to. */
    fixed_vector<int>& component_index;

    /* Distance to the current nearest point in the hyperrectangle */
    // S hyperrect_distance;

    MetricType* metric;

    MSTRules(int d, Tree& t, Points_t& p, UnionFind& c, Neighbors& n, fixed_vector<int>& cid, MetricType* m) : dim(d), tree(t), data(p), connections(c), neighbors(n), component_index(cid), metric(m) {}

    /* Base case calculation */
    void base_case (Box& s, int t);
    void base_case (Box& s, Box& t);
    void base_case (vector<Box*> t);

    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);
    bool prune_subtree (Box& s, Box& t);
    bool prune_subtree (vector<Box*> t);

    /* Calculate which branch to visit first */
    vector<int> visit (Box& s, int t);
    vector<int> visit (Box& s, Box& t);
    vector<int> visit (vector<Box*> t, int div);

    /* Update distance */
    void update_state (Box& s, Box& t);
    void update_state (vector<Box*> t);

    /* Update the current value of the distance to the nearest point in the hyperrectangle */
    real_t distance_update (Box& s, int t);
    real_t local_distance_update (Box& s, int t);
    real_t distance_update (Box& s, Box& t);
    real_t distance_update (vector<Box*> t);

    /* Calculate the farthest distance of a box */
    real_t calculate_farthest_distance (Box& t);

    /* centorid case computation for the subtree is will be prunes, in this case since it's prune it does nothing*/
    void centroid_case (Box& s, int t) {};
    void centroid_case (Box& s, Box& t) {};
    void centroid_case (vector<Box*> t) {};

    void set_temp(int temp) {};
    void adjustPartitionCounter(int , int , int , int){};
    double get_temp(){};
};

// Template implementation
template<typename Tree, typename S>
void
MSTRules<Tree,S>::base_case (Box& s, int t) {
  real_t dist;
  /* Find the query component index */
  size_t query_component_index = connections.find(t);

  for (size_t j = s.begin(); j < s.end(); ++j) {
    /* Find the reference component index */
    size_t reference_component_index = connections.find(j);
    /* Check if the points are in different components and if so, compute distance between them */
    if (query_component_index != reference_component_index) {
      dist = metric->compute_distance (data[t], data[j]);
      /* Update current nearest neighbor */
      if (dist < neighbors[query_component_index].squared_distance) {
        assert (t != j);
        neighbors[query_component_index] = ComponentData(t, j, dist);
      }
    }
  }
}

template<typename Tree, typename S>
void
MSTRules<Tree,S>::base_case (Box& s, Box& t) {
  real_t dist;
  for (size_t i = t.begin(); i < t.end(); ++i) {
    /* Check if this target point can be improved further */
    if (local_distance_update(s, i) == DBL_MAX)
      continue;

    /* Find the query component index */
    size_t query_component_index = connections.find(i);

    for (size_t j = s.begin(); j < s.end(); ++j) {
      /* Find the reference component index */
      size_t reference_component_index = connections.find(j);
      /* Check if the points are in different components and if so, compute distance between them */
      if (query_component_index != reference_component_index) {
        dist = metric->compute_distance (data[i], data[j]);
        /* Update current nearest neighbor */
        if (dist < neighbors[query_component_index].squared_distance) {
          assert (i != j);
          neighbors[query_component_index] = ComponentData(i, j, dist);
        }
      }
    }
  }
}


 /* k is set to 1 for KNN with two dataset and has been used
  * for the multi-tree, in the case of an algorithm with more
  * than two dataset the kernel function and k should have 
  * change accordingly
  */


template<typename Tree, typename S>
void
MSTRules<Tree,S>::base_case (vector<Box*> s) {
  int k = 1;
  real_t dist;
  for (size_t i = s[k]->begin(); i < s[k]->end(); ++i) {
    /* Check if this target point can be improved further */
    if (local_distance_update(*s[k-1], i) == DBL_MAX)
      continue;

    /* Find the query component index */
    size_t query_component_index = connections.find(i);

    for (size_t j = s[k-1]->begin(); j < s[k-1]->end(); ++j) {
      /* Find the reference component index */
      size_t reference_component_index = connections.find(j);
      /* Check if the points are in different components and if so, compute distance between them */
      if (query_component_index != reference_component_index) {
        dist = metric->compute_distance (data[i], data[j]);
        /* Update current nearest neighbor */
        if (dist < neighbors[query_component_index].squared_distance) {
          assert (i != j);
          neighbors[query_component_index] = ComponentData(i, j, dist);
        }
      }
    }
  }
}
/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S>
inline
bool
MSTRules<Tree,S>::prune_subtree (Box& s, int t) {
  int i = s.index();
  /* If all points in reference node are farther than current nearest neighbor, prune */
  return s.min_distance(tree.node_data[t]) >= neighbors[connections.find(t)].squared_distance;
    return true;
  return false;
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S>
inline
bool
MSTRules<Tree,S>::prune_subtree (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  /* If all points in reference node are farther than current nearest neighbor, prune */
  real_t farthest_dist = calculate_farthest_distance(t);
  return sqrt(t.min_distance(s)) >= farthest_dist;
}

template<typename Tree, typename S>
inline
bool
MSTRules<Tree,S>::prune_subtree (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  /* If all points in reference node are farther than current nearest neighbor, prune */
  real_t farthest_dist = calculate_farthest_distance(*s[k-1]);
  return sqrt(s[k]->min_distance(*s[k-1]) ) >= farthest_dist;
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S>
inline
vector<int>
MSTRules<Tree,S>::visit (Box& s, int t) {
  vector<Box>& stree = tree.node_data;
  real_t left_score = distance_update(stree[s.child], t);
  real_t right_score = distance_update(stree[s.child+1], t);
  /* Check if left branch should be traversed first */
  if (left_score <= right_score)
    return vector<int> {0,1};
  else
    return vector<int> {1, 0};
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S>
vector<int>
MSTRules<Tree,S>::visit (Box& s, Box& t) {
  vector<Box>& stree = tree.node_data;
  vector<pair<real_t, int>> dist_index;
  for (int i = 0; i < s.num_child(); i++)
    dist_index.push_back(make_pair(distance_update(stree[s.child+i], t), i) );
  struct {
        bool operator()(std::pair<real_t, int> a, std::pair<real_t, int> b) const{return a.first <= b.first;}   
  } customLess;
  sort(dist_index.begin(), dist_index.end(), customLess);
  vector<int> visit_order;
  for (int i = 0; i < dist_index.size(); i++)
    visit_order.push_back(dist_index[i].second);
  return visit_order;
}

template<typename Tree, typename S>
inline
vector<int>
MSTRules<Tree,S>::visit (vector<Box*> s, int div) {
  vector<Box>& stree = tree.node_data;
  int next = (div == s.size() - 1) ? div - 1: div + 1;
  vector<pair<real_t, int>> dist_index;
  for (int i = 0; i < s[div]->num_child(); i++)
    dist_index.push_back(make_pair(distance_update(stree[s[div]->child+i], 
                                   *s[next]), i) );
  struct {
        bool operator()(std::pair<real_t, int> a, std::pair<real_t, int> b) const{return a.first <= b.first;}   
  } customLess;
  sort(dist_index.begin(), dist_index.end(), customLess);
  vector<int> visit_order;
  for (int i = 0; i < dist_index.size(); i++)
    visit_order.push_back(dist_index[i].second);
  return visit_order;
}

/**
 * Update distance information.
 */
template<typename Tree, typename S>
inline
void
MSTRules<Tree,S>::update_state (Box& s, Box& t) {
  vector<Box>& ttree = tree.node_data;
  for (int i = 0; i < t.num_child(); i++) {
    distance_update(s, ttree[t.child+i]);
  }
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S>
real_t
MSTRules<Tree,S>::distance_update (Box& s, int t) {
  int i = s.index();

  size_t query_component_index = connections.find(t);
  /* If query is in the same component as all the references, prune */
  if (query_component_index == component_index[s.index()])
    return DBL_MAX;

  real_t dist = s.min_distance(data[t]);
  return sqrt(dist);
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S>
real_t
MSTRules<Tree,S>::local_distance_update (Box& s, int t) {
  size_t query_component_index = connections.find(t);
  /* If query is in the same component as all the references, prune */
  if (query_component_index == component_index[s.index()])
    return DBL_MAX;
  real_t dist = s.min_distance(data[t]);
  dist = sqrt(dist);
  /* If all points in reference node are farther than current nearest neighbor, prune */
  dist = (dist < neighbors[query_component_index].squared_distance) ? dist : DBL_MAX;
  return dist;
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S>
real_t
MSTRules<Tree,S>::distance_update (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  /* If all queries are in the same component as all the references, prune */
  if (component_index[t.index()] >= 0 &&
      component_index[t.index()] == component_index[s.index()])
    return DBL_MAX;

  real_t dist = t.min_distance(s);
  return sqrt(dist);
}

template<typename Tree, typename S>
real_t
MSTRules<Tree,S>::distance_update (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  /* If all queries are in the same component as all the references, prune */
  if (component_index[s[k]->index()] >= 0 &&
      component_index[s[k]->index()] == component_index[s[k-1]->index()])
    return DBL_MAX;
  real_t dist = s[k]->min_distance(*s[k-1]);
  return sqrt(dist);
}
/* Calculate the farthest distance of a box */
template<typename Tree, typename S>
real_t
MSTRules<Tree,S>::calculate_farthest_distance (Box& t) {
  real_t dist = 0;
  for (size_t i = t.begin(); i < t.end(); ++i) {
    /* Find the query component index */
    const size_t component_index = connections.find(i);
    const real_t neighbor_dist = neighbors[component_index].squared_distance;
    if (neighbor_dist > dist)
      dist = neighbor_dist;
  }
  return dist;
}


#endif

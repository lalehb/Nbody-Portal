#ifndef INC_INLINE_H
#define INC_INLINE_H

#include "IRNode.h"
#include "Qualify.h"

/* Replace calls to functions with their definition */

namespace Nbody {

class Inline : public IRPass {
public:
  Function func;
  bool found;
  Inline(Function f) : func(f), found(false) {}

private:
  void visit(const Call* op) {
    if (op->name == func.name()) {
      // Mutate the arguments
      std::vector<Expr> args(op->args.size());
      for (size_t i = 0; i < args.size(); ++i)
        args[i] = mutate(op->args[i]);

      // Get the body
      Expr body = qualify(func.name() + ".", func.value());

      // Bind the arguments
      assert(args.size() == func.args().size());
      for (size_t i = 0; i < args.size(); ++i)
        body = Let::make(func.name() + "." + func.args()[i], args[i], body);

      expr = body;
      found = true;
    }
    else
      IRPass::visit(op);
  }
};

Stmt inline_function(Stmt s, Function f) {
  Inline i(f);
  s = i.mutate(s);
  return s;
}

Expr inline_function(Expr e, Function f) {
  Inline i(f);
  e = i.mutate(e);
  return e;
}

}

#endif

#ifndef _KNN_VECTOR_H_
#define _KNN_VECTOR_H_

#include <memory>
#include <iterator>
#include <algorithm>
#include <cfloat>
#include "fixed_vector.h"
/**
 * \brief Define an insertion-based vector to hold the K best elements pushed.
 *
 * \note Element insertions are asymptotically linear.
 *
 */

/** A sorted vector of constant capacity */
template <typename T, typename Compare = std::less<T> >
class kNN_vector {
 private:
  /* Number of elements currently in the k-vector */
  size_t length;
  /* Vector of stored elements (sorted with worst one on the first position) */
  fixed_vector<T> data_;
  /* Comparison object */
  const Compare compare;

public:
  /* kNN-vector constructor */
  explicit kNN_vector(size_t N)
      : length(0), data_(N), compare(Compare()) {
  }

  const T& operator[](size_t i) const { return data()[i]; }

  /* Get the number of elements currently in the k-vector. Cost: O(1) */
  size_t size() const { return length; }

  size_t capacity() const { return data_.size(); }

  /* Check if the k-vector is empty. Cost: O(1) */
  bool empty() const { return size() == 0; }

  void clear(){
	  length = 0;
	  //data_ = fixed_vector<T>(data_.size());
	  data_.clear();
	}

  /* Check if the k-vector is full (has K elements). Cost: O(1) */
  bool full() const { return size() == capacity(); }

  /* Get the worst element stored in the k-vector. Cost: O(1) */
  const T& front() const { return data_.front(); }

  /* Get the best element stored in the k-vector. Cost: O(1) */
  const T&  back() const { return data_.back(); }

  /* Get the data */
  const T* data() const { return data_.data(); }

  const T* begin() const { return data(); }
  const T*   end() const { return data()+size(); }

  /* Pop the current worst element from the k-vector. Cost: O(K) */
  void pop_back() { if(!empty()) --length; }

  /* Push a new element into the k-vector. Cost: O(K) */
  void push_back(const T& v) {
    if (size() < capacity())
      insert(length++, v);
    else if (compare(v,back()))
      insert(length-1, v);
  }

 private:

  /** Insert the element @a v into this kNN_vector
   * The maximum position @a v could take is @a i.
   * @pre 0 <= @a i < size()
   * @post @a v is stored into this[j] for some j, 0 <= j <= @a i.
   * @post compare(this[j], this[k]) for all j,k with 0 <= j < k <= @a i.
   */
  void insert(int i, const T& v) {
    for ( ; i > 0 && compare(v, data_[i-1]); --i)
      data_[i] = data_[i-1];
    // cout << " inja: " << i    << " --> " << v.index <<" , "<< v.squared_distance << "\n";
    data_[i] = v;
    // cout << "after \n";
  }
};

#endif

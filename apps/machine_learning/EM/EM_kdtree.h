#ifndef __EM_GMM__EM__
#define __EM_GMM__EM__
#define counter_type unsigned long long


#define thr 32

#include <iostream>
#include <math.h>
#include <ctime>
#include <string>
#include <fstream>
#include <cstdlib>
#include <sys/time.h>
#include <vector>
#include <omp.h>
#include <numeric>
#include <chrono>

#include "Clock.hpp"
#include "E_rules.h"
#include "LL_rules.h"
#include "Single_tree_traversal_Pascal.h"
#include "EM.h"



using namespace Eigen;
using namespace std;

template <typename Tree>
class EM {
  private:

    Points_t& data;    // NumData x Dim -> data matrix with rows as elements of data
    Tree& src_tree;
    real_t** mean;    // NumGuassian x Dim -> matrix of guassian means
    real_t*** sigma;  // NumGussian x Dim x Dim -> matrix of gussian covariance (dxd)
    real_t* pi;       // NumGuassian -> vector of probabilities for each guassian class
    real_t** resp;    // NumData x NumGaussian -> matrix of responsibilities
    int NumData;      // Number of data points [n]
    int NumGaussian;  // Number of Guassian Mixtures [k]
    int Dim;          // dimention of data [d]
    real_t Threshold; // Convergence threshold
    real_t* nk;       // holder array
    stats finalResult;  // stat result of the traverse

    real_t LogLiklihood();
    real_t LogLiklihood_old();
    real_t Gaussian(Point datai , real_t*  mui, int i); // result of multivariate gaussian
    void Estep();      // running the E step of algorithm
    void Mstep();      // running the M step by calling SetPiMean and SetSigma
    void  SetPiMean();
    void SetSigma();

  public:
      int Iteraton= 5;
    real_t*** Cholesky_all;
    real_t* determinant_all;
    real_t** centers ;
    real_t tolerance;
    real_t power;
    size_t num_of_prunes = 0;
    size_t size_of_prunes = 0;
    EM(int n, int k , int d, real_t t, real_t toler, Points_t& data_temp, Tree& tree_input, int iter);

   /* The main EM iteration */
    void IterationEM();

    /* Printing the result of EM iteration */
    void ResultPrinter();
    void Result1(real_t** meani, real_t*** sigmai);

    /* sets the Mean and Sigma of Gaussina*/
    MeanSigma Result();
    real_t Cholesky(real_t** sigmai, real_t** L);
    void ForwardSub(real_t** L, real_t* Y, real_t* result);
};

/* Template instantiation */

/**
 * This function will get the will measures L as --> sigma = L L^T
 * which L is the lower triangular matrix for sigma
 * also will return the determinant of that sigma
*/
template <typename Tree>
real_t EM<Tree>::Cholesky(real_t** sigmai, real_t** L) {
  /* Initialize L to zero */
  real_t determinant = 1;
  for(size_t i = 0; i < Dim; ++i) {
    for(size_t j = 0; j < (i+1); ++j) {
      double sum = 0;
      for (size_t k = 0; k<j ; ++k) {
        sum += L[i][k] * L[j][k];
      }
      L[i][j] = (i == j)? sqrt(sigmai[i][i] - sum) : (1.0 /L[j][j]*(sigmai[i][j]- sum));
    }
    determinant *= L[i][i];
  }
  determinant *= determinant;
  return determinant;
}

/**
 * This function will measure the (L^-1)Y and return the result using forward substitution
*/
template <typename Tree>
void EM<Tree>::ForwardSub(real_t** L, real_t* Y, real_t* result) {
  result[0] = Y[0]/L[0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = Y[i];
    for (size_t j = 0; j < i; ++j)
      temp -= L[i][j] * result[j];
    result[i] = temp / L[i][i];
  }
}

/**
 * Calculating the Gaussian model (multivariate pdf)
 * using Eigen library  for transpose and inverse of matrix
 */

template <typename Tree>
real_t EM<Tree>::Gaussian(Point datai, real_t* mui, int f) {
  real_t* Y[Dim];
  for (int i = 0; i < Dim; ++i)
    Y[i] = datai[i] - mui[i];

  real_t** L = new real_t*[Dim];
  for(size_t i = 0 ; i < Dim; ++i) {
    L[i] = new real_t[Dim];
    for (int j = 0; j < Dim; ++j)
      L[i][j] = Cholesky_all[f][i][j];
  }

  real_t* res = new real_t[Dim];

  ForwardSub(L,Y, res);
  double inner_product = 0;
  for (int i = 0; i < Dim; ++i)
    inner_product += res[i]*res[i];

  real_t result = determinant_all[f] * exp(-0.5 * inner_product );
  return result;
}


/**
 * Calculates the Log liklihood of the data given the parameters of the model
 */

template <typename Tree>
real_t EM<Tree>::LogLiklihood() {
  real_t liklihood ;
  counter_type  num_prunes = 0 ;
  typedef LL_rules<Tree> Rule;
  Rule rules(NumData,Dim, NumGaussian, tolerance, src_tree, data, mean, sigma, pi, resp, Cholesky_all, determinant_all);

  SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);
  // #pragma omp parallel
  {
    // #pragma omp single nowait
    {
      traverser.traverse(src_tree.root(), 0);
      liklihood = rules.LL;
    }
  }
  return liklihood;
}

/**
 * Calculates the  E-step, and filles out the matrix of responsibilities (resp)
 */
template <typename Tree>
void EM<Tree>::Estep() {
  real_t temp;
  for (size_t i = 0; i < NumGaussian; ++i) {
     temp = Cholesky(sigma[i],Cholesky_all[i]);
     determinant_all[i] = (1/(sqrt(temp * power)));
  }

  counter_type  num_prunes = 0 ;
  typedef E_rules<Tree> Rule;
  Rule rules(NumData,Dim, NumGaussian, tolerance, src_tree, data, mean, sigma, pi, resp, Cholesky_all, determinant_all);


  SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);
  #pragma omp parallel
  {
    #pragma omp single nowait
    {
      traverser.traverse (src_tree.root(), 0);
      finalResult = rules.statistics;
    }
  }

  num_of_prunes += rules.num_of_prune;
  size_of_prunes += rules.size_of_prune;
}

/**
 * Calculates the  M-step of the EM algorithm for GMM and filles new values
 * for sigma, mu and pi
 */

 template <typename Tree>
void EM<Tree>::Mstep() {
  /* setting the new pi */
  for (int i = 0; i < NumGaussian; ++i) {
    /* setting pi and Mean */
    pi[i] = (finalResult.SW[i]) / NumData;
    for (int w = 0; w < Dim; ++w) {
      mean[i][w] = finalResult.SWX[i][w] / finalResult.SW[i];
    }
    /* setting the sigma for the Gaussian */
    VectorXd tempMean(Dim);
    VectorXd tempSWX(Dim);
    MatrixXd Temp(Dim,Dim);
    for (int j = 0; j < Dim; ++j) {
       tempMean(j) = mean[i][j];
       tempSWX(j) = finalResult.SWX[i][j];
      for (int k = 0; k < Dim; ++k) {
        Temp(j,k) = finalResult.SWXX[i][j][k] ;
      }
    }
    real_t SWtemp = 1 / finalResult.SW[i];
    MatrixXd tt(Dim,Dim);
    tt = (tempSWX * tempMean.transpose()) * SWtemp;
    Temp = (Temp * SWtemp ) - tt - tt.transpose()  + (tempMean * tempMean.transpose());

    /* Adding Epsilon to the Zero places in the eigen values
     to make the Sigma positive-semi-definite */
    real_t eps = pow(10, -6);

    SelfAdjointEigenSolver<MatrixXd> es(Temp);
    MatrixXd Di = es.eigenvalues().asDiagonal();
    MatrixXd V = es.eigenvectors();
    for (int t = 0; t < Dim; ++t) {
      if (Di(t,t) <= 0)
        Di(t,t) = eps ;
    }
    Temp = V * Di * V.inverse();

    for (int j = 0; j < Dim; ++j) {
      for (int k = 0; k < Dim; ++k) {
        sigma[i][j][k]   = Temp(j,k);
      }
    }
  }

}


/* EM Iterations */

template <typename Tree>
void EM<Tree>::IterationEM() {

  SetPiMean();
  SetSigma();
  /* Timing variables */
  Clock timer;
  double m_time = 0;
  double e_time = 0;
  double l_time = 0;

  real_t oldLL = -INFINITY;
  real_t newLL = LogLiklihood();
  size_t n = 0;
  /* loop iterates until algorithm converges as determined by ConvTres */
   while ( !((n >= Iteraton) or (abs(newLL - oldLL)/NumData < Threshold))) {
    n++;
    auto start1 = chrono::high_resolution_clock::now();
    Estep();
    e_time += chrono::duration_cast<chrono::duration<double>>(chrono::high_resolution_clock::now()-start1).count();
    auto start2 = chrono::high_resolution_clock::now();
    Mstep();
    m_time +=  chrono::duration_cast<chrono::duration<double>>(chrono::high_resolution_clock::now()-start2).count();
   auto start3 = chrono::high_resolution_clock::now();
   newLL = LogLiklihood();
   l_time +=  chrono::duration_cast<chrono::duration<double>>(chrono::high_resolution_clock::now()-start3).count();
    cout.precision(6);
  }

  /* print timing results */
//   cout  << endl <<  "Total Timing for EM kdrtree: " << endl
//   << "E-step       : " << e_time << endl
// //  << "M-step       : " << m_time << endl
// //  << "LL           : "  <<  l_time  << endl
//   << "#Iter        : " << n << endl
//   << "#Prune       : " << num_of_prunes << endl
//   << "Prune size   : " << size_of_prunes << endl;

}



/* Constructor */
template <typename Tree>
EM<Tree>::EM(int n, int k , int d, real_t t, real_t toler, Points_t& data_temp, Tree&  tree_input, int iter) :  data(data_temp), src_tree(tree_input) {
  NumData = n ;
  NumGaussian = k ;
  Dim = d ;
  Threshold = t;
  tolerance = toler;
  Iteraton = iter;
  /* initializing finalResult  */
  stats finalResult(Dim, NumGaussian);
  int i;
  srand(time(NULL));

  /* initilizing mean matrix */
  mean = new real_t*[NumGaussian];
  for (i = 0; i < NumGaussian; ++i) {
    mean[i] = new real_t[Dim];
    for (int f = 0; f < Dim; ++f)
    {
      mean[i][f] = 0;
    }
  }

  /* initilizing sigma matrix */
  sigma = new real_t**[NumGaussian];
  for (i = 0; i < NumGaussian; ++i) {
    sigma[i] = new real_t*[Dim];
    for (int j = 0; j < Dim; ++j) {
      sigma[i][j] = new real_t[Dim];
      for (int f = 0; f < Dim; ++f)
      {
        sigma[i][j][f] = 0;
      }
    }
  }


  /* initilizing pi vector */
  pi = new real_t[NumGaussian];
  real_t sumPi = 0;
  for (i = 0; i < NumGaussian; ++i) {
    pi[i] = rand() / real_t(RAND_MAX);
    sumPi += pi[i];
  }
  cout << endl;
  /* normalizing the Pi*/
  for (i = 0; i < NumGaussian; ++i)
    pi[i] /= sumPi;

  /* initilizing the nk */
  nk = new real_t[NumGaussian];

  /* initilizing responsilibities matrix */
  resp = new real_t*[NumData];
  for (i = 0; i < NumData; ++i) {
    resp[i] = new real_t[NumGaussian];
    for (int j = 0; j < NumGaussian; ++j) {
      resp[i][j] = rand() / real_t(RAND_MAX);
    }
  }


  /* making centers of the boxes in the tree*/
  centers = new real_t*[NumData];
  for(int i = 0 ; i < NumData; ++i){
    centers[i] = new real_t[Dim];
  }

  Cholesky_all = new real_t**[NumGaussian];
  determinant_all = new real_t[NumGaussian];
  for (size_t i = 0; i < NumGaussian; ++i) {
    Cholesky_all[i] = new real_t*[Dim];
    for (size_t j = 0; j < Dim ; ++j) {
      Cholesky_all[i][j] = new real_t[Dim];
    }
  }


  /* measure the pow((2* M_PI),dim) for optimizing the Guassian */
  power = pow((2* M_PI),Dim);
  int num_prunes = 0 ;
  src_tree.centroids(src_tree.root());
}



/* Setting the new pi  and mean for M-step */
template <typename Tree>
void EM<Tree>::SetPiMean() {

  real_t tot = 0;
  for (int i = 0; i < NumGaussian; ++i) {
    nk[i] = 0;
    for (int j = 0; j < NumData; ++j)
      nk[i] += resp[j][i];
    tot += nk[i];
  }
  for (int i = 0; i < NumGaussian; ++i) {
    pi[i] = nk[i]/tot;
  }

  /* setting the new Mean */
  real_t temp[Dim];
  for (int i = 0; i < NumGaussian; ++i) {
    for (int l = 0; l < Dim; ++l)
      temp[l] = 0;
    for (int j = 0; j < NumData; ++j) {
      for (int l = 0; l < Dim; ++l) {
        temp[l] += resp[j][i] * data[j][l];
      }
    }
    for (int l = 0; l < Dim; ++l) {
      mean[i][l] = temp[l]/nk[i];
    }
  }
   #if 0
   /* Totally random start */
   for (int i = 0; i < NumGaussian; ++i) {
     for (int l = 0; l < Dim; ++l)
       mean[i][l] = 10 * rand() / real_t(RAND_MAX);
   }
  /* setting for scikit learn */
   for (int i = 0; i < NumGaussian; ++i) {
     for (int l = 0; l < Dim; ++l)
       mean[i][l] = 0;
   }

   /* test */
   for (int i = 0; i < NumGaussian; ++i) {
     for (int l = 0; l < Dim; ++l)
       mean[i][l] = i;
   }
   #endif

}

/* Setting the new sigma for M-step */
template <typename Tree>
void EM<Tree>::SetSigma() {
  /* setting the new Sigma */
  real_t eps = pow(10, -16);
  for (int i = 0; i < NumGaussian; ++i) {
    MatrixXd sigTemp(Dim,Dim);
    for (int j = 0; j < NumData; ++j) {
      VectorXd x(Dim);
      VectorXd m(Dim);
      VectorXd temp(Dim);
      for (int f = 0; f < Dim ; ++f) {
        x(f) = data[j][f];
        m(f) = mean[i][f];
        temp(f) = x(f)-m(f);
      }
      sigTemp = sigTemp + (resp[j][i]*(temp * temp.transpose()));
    }
    if (nk[i] == 0)
      nk[i] = eps;
    sigTemp = sigTemp /nk[i];

    /* Adding Epsilon to the Zero places in the eigen values to make the Sigma positive-semi-definite */
    real_t eps = pow(10, -6);

    SelfAdjointEigenSolver<MatrixXd> es(sigTemp);
    MatrixXd Di = es.eigenvalues().asDiagonal();
    MatrixXd V = es.eigenvectors();
    for (int z = 0; z < Dim; ++z) {
      if (Di(z,z) <= 0)
        Di(z,z) = eps ;
    }

    sigTemp = V * Di * V.inverse();
    /* sigma assigning */
    for (int j = 0; j < Dim; ++j) {
      for(int f = 0; f < Dim; ++f) {
        sigma[i][j][f] = sigTemp(j,f);
      }
    }
    #if 0
    /* for scikit learn */
    for (int j = 0; j < Dim; ++j) {
      for(int f = 0; f < Dim; ++f){
        if (j ==f)
          sigma[i][j][f] = 1;
        else
          sigma[i][j][f] = 0;
      }
    }
    #endif
  }


    for (size_t i = 0; i < NumGaussian; ++i) {
      determinant_all[i] = Cholesky(sigma[i],Cholesky_all[i]);
    }
}


/**
 * Printing the final results
 */

template <typename Tree>
void EM<Tree>::ResultPrinter() {
  /* print mean */
  cout << "mean : " << endl;
  for ( int i = 0; i < NumGaussian; ++i) {
    for(int j = 0; j < Dim; ++j)
      cout << mean[i][j] << "  ";
    cout << endl;
  }
  cout << "~~~~~~~~~~~~~~~~" <<  endl ;
  /* print sigma */
  cout << "sigma : " << endl << "------------" << endl;

  for (int i = 0; i < NumGaussian ; ++i) {
    for (int j = 0; j < Dim; ++j) {
      for (int f = 0; f < Dim; ++f)
        cout << sigma[i][j][f] << "  ";
      cout << endl;
    }
    cout  << endl << "------------"<< endl;
  }
  cout << "~~~~~~~~~~~~~~~~" <<  endl ;
  /* print pi */
  cout << "pi: " << endl;
  for (int i = 0; i < NumGaussian; ++i)
    cout << pi[i] << "  ";
  cout << endl;
}


template <typename Tree>
void EM<Tree>::Result1(real_t** meani, real_t*** sigmai) {
  /*  mean assignment */
  for ( int i = 0; i < NumGaussian; ++i) {
    for(int j = 0; j < Dim; ++j)
       meani[i][j] = mean[i][j] ;
  }
  /*  sigma assignment */
  for (int i = 0; i < NumGaussian ; ++i) {
    for (int j = 0; j < Dim; ++j) {
      for (int f = 0; f < Dim; ++f)
        sigmai[i][j][f] =  sigma[i][j][f];
    }
  }
}

template <typename Tree>
MeanSigma EM<Tree>::Result() {
  MeanSigma a;
  int i, j, f;
  a.mean = new real_t*[NumGaussian];
  for (i = 0; i < NumGaussian; ++i)
    a.mean[i] = new real_t[Dim];

     a.sigma = new real_t**[NumGaussian];
       for (i = 0; i < Dim; ++i) {
           a.sigma[i] = new real_t*[Dim];
               for (j = 0; j < Dim; ++j)
                    a.sigma[i][j] = new real_t[Dim];
       }
  for ( i = 0; i < NumGaussian; ++i) {
    for(j = 0; j < Dim; ++j)
       a.mean[i][j] = mean[i][j] ;
  }
  for ( i = 0; i < NumGaussian ; ++i) {
    for ( j = 0; j < Dim; ++j) {
      for ( f = 0; f < Dim; ++f)
        a.sigma[i][j][f] =  sigma[i][j][f];
    }
  }
 return a;
}
#endif /* defined(__EM_GMM__EM__) */

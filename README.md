# Portal Overview

Portal is a high-performance domain-specific language and compiler for parallel N-body computations. 
Portal is designed in order to fulfill these three goals:      

* to implement scalable, fast algorithms that have O(n log n) and O (n) complexity   
* to design an intuitive language to enable rapid implementations of a variety of problems         
* to enable parallel large-scale problems to run on multicore systems

We target N-body problems in various domains from machine learning to scientific
computing that can be expressed in Portal to obtain an out-of-the-box optimized parallel implementation.

Bellow is Portal block diagram. The core of the compiler lowers the N-body problem defined in the Portal language to imperative code. Portal constructs a loop nest and injects storage for each layer of the loop according to their corresponding operators. Portals back-end code generator emits machine code via LLVM.      

![Portal](/uploads/c83969ad52d3698ee54e0ab9f5582877/Portal.png)      


Portal is built on the top of the algorithmic framework PASCAL which utilizes tree data-structures and user-controlled pruning or approximations to reduce the asymptotic runtime complexity from being linear in the number of data points to be logarithmic. 
Bellow is the PASCAL block diagram.       

![Pascal](/uploads/23e81549a1983cd1b98f6bff75b65416/Pascal.png)




# Portal Code Structure 
This section provides an overview of general structure of the code. For more details, the documentation in the code could be more helpful.       
For Portal test, you can use `make` command in the main folder to get all the tests, and for PASCAL tests you can use `make` command in the folder `Nbody-ML/apps/tests/library` 

* **Tree**        
This folder includes different tree classes and implementation as well as different bounding boxes that is used by different trees. This includes: Hyper-rectangle, Hyper-sphere as well as Cover_boundry. The tree traversal scheme which is mentioned in the middle red box of Portal diagram is located in this folder.      

* **Src**       
This folder includes the main implementation of Portal. It includes all the blocks in the Portal diagram shown with blue. Separate classes for each part of Portal are located in this folder such as NbodyExpression which is the man interface for generating a Nbody computation, Storage which handles the storage of Portal, Nbodylower which is the lowered version of Nbody expression, IRNodes which are Intermediate Representation of Portal IR, or IRVisitor which  walks over IRs and many more.

* **Apps**         
This folder includes the PASCAL implementation of the 6 Nbody problems as we compare Portal with. This algorithms are EM, KNN, RS, HD, EMST, KDE as well as a set of additional kernels and metrics included in folders `kernels` and `metrics` respectively. It also, includes a folder named `test` which provides the tests for testing each algorithm separately.

* **test**         
This folder include a folder named `algorithms` which represents the tests for different Nbody problems written in Portal language. For each test after you `make` the test, you can simply run the name of executable and it will print a hint on the input needed by the program.     
For example for the KNN problem it shows the usage as: `test--KNN <K> <filenameT> <filenameS>`

* **Utils**
This folder includes the prune generator which is shown as the rightmost red block in the Portal diagram. It also includes some additional utilities such as clock which uses Chrono clock for timing.    

* **Arch**           
This folder includes the needed spec for the compilation.

* **Data**          
This folder includes different datastructres for handling data such as `vector_of_array` and `fixed_vector`      

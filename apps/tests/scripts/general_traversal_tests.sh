#!/bin/bash



SIZE=100000
DIM=68
INPUT1=/home/real_datasets/Census2.csv
INPUT2=/home/real_datasets/Census3.csv
OUT=results.out
INNER=../library/
for threads in 32 1
do
  for l in 4 6 8
  do
    echo "Dual kNN KDTREE  THREADS=$threads LEVEL = $l for Census2" 2>&1 | tee -a ${OUT}
    cd ${INNER}
    env OMP_NUM_THREADS=$threads ./test--kNN_kdtree ${SIZE} ${DIM} ${INPUT1} 3 ${l} 2>&1 | tee -a ${OUT}
  done
done

for threads in 32 1
do
  for l in 4 6 8
  do
    echo "Dual HD KDTREE THREADS=$threads LEVEL = $l for Census2 Census3" 2>&1 | tee -a ${OUT}
    cd ${INNER}
    env OMP_NUM_THREADS=$threads ./test--HD_kdtree ${SIZE} ${DIM} ${INPUT1} ${INPUT2} ${l} 2>&1 | tee -a ${OUT}
  done
done

for threads in 32 1
do
  for l in 4 6 8
  do
    echo "Dual kde KDTREE THREADS=$threads  LEVEL = $l for Census2 " 2>&1 | tee -a ${OUT}
    cd ${INNER}
    env OMP_NUM_THREADS=$threads ./test--kde_kdtree ${SIZE} ${DIM} ${INPUT1} 250 ${l} 2>&1 | tee -a ${OUT}
  done
done

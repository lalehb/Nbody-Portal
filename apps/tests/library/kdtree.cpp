#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Binary_tree.h"
#include "Hrect_bounds.h"
#include "Clock.hpp"

//#define __DEBUG 1

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <distribution>\n", use);
}

int main (int argc, char** argv)
{
  char* distribution;
  unsigned int num_pts;
  unsigned int dim;
  Clock timer;

  if (argc != 4) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  distribution = argv[3];

  /* Initialize seed */
  util::default_generator.seed(1337);

  /* Allocate memory for original dataset */
  Points s(num_pts, dim);

  initialize_dataset(s, distribution);

  /* Allocate memory for permuted dataset */
  Points s_perm(num_pts, dim);

  /* Create and build tree */
  Tree<Hrect> tt(s, s_perm);
  timer.start();
  tt.build_kdtree ();
  double time_tree = timer.seconds();

#ifdef __DEBUG
  cout << "In the main program...\n";
  for (size_t i = 0; i < num_pts; i++) {
    for (size_t j = 0; j < dim; j++)
      cout << tt.data[i][j] << " ";
    cout << endl;
  }

  for (size_t i = 0; i < num_pts; i++) {
      cout << tt.index[i] << " ";
  }
  cout << endl;

  for (size_t i = 0; i < num_pts; i++) {
    for (size_t j = 0; j < dim; j++)
      cout << tt.data_perm[i][j] << " ";
    cout << endl;
  }

  vector<Tree<Hrect>::NodeTree>& ntree = tt.node_data;
  for (size_t i = 0; i < ntree.size(); i++) {
    fprintf (stderr, "\nNode: %lu\n", i);
    for (size_t d = 0; d < dim; d++) {
      fprintf (stderr, "%lf  %lf\n", ntree[i].bounds.lo[d], ntree[i].bounds.hi[d]);
    }
  }
  cout << endl;
#endif

  /* Print time */
  cerr << "Kd-tree construction time: " << time_tree << " seconds\n";
  cout << "Passed." << endl;
  return 0;
}

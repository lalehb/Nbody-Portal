#include "IRPrinter.h"

namespace Nbody {

using std::ostream;


ostream& operator<<(ostream& stream, const Expr& ir) {
  if (!ir.defined())
    stream << "(undefined)\n";
  else {
    IRPrinter p(stream);
    p.print(ir);
  }
  return stream;
}

ostream& operator<<(ostream& stream, const Type& type) {
  switch (type.t) {
    case Type::Int:
      stream << "int";
      break;
    case Type::Float:
      stream << "float";
      break;
    case Type::Handle:
      stream << "handle";
      break;
    default:
      assert(false && "Unknown type");
    }
    stream << type.bits;
    if (type.nelem > 1)
      stream << 'x' << type.nelem;
    return stream;
}

ostream& operator<<(ostream& stream, const Stmt& ir) {
  if (!ir.defined())
    stream << "(undefined)\n";
  else {
    IRPrinter p(stream);
    p.print(ir);
  }
  return stream;
}

ostream& operator<<(ostream& stream, const For::ForType& ftype) {
  switch (ftype) {
    case For::Serial:
      stream << "for";
      break;
    case For::Parallel:
      stream << "parallel";
      break;
    case For::Vectorized:
      stream << "vectorized";
      break;
  }
  return stream;
}
/*
ostream& operator<<(ostream & stream, const Integer) {

}
*/
void IRPrinter::indent() {
  for (size_t i = 0; i < indent_level; ++i)
    stream << ' ';
}

void IRPrinter::visit(const Integer* op) {
  stream << op->value;
}

void IRPrinter::visit(const Flt* op) {
  stream << op->value << 'f';
}

void IRPrinter::visit(const Dbl* op) {
  stream << op->value;
}

void IRPrinter::visit(const String* op) {
  stream << '"';
  for (size_t i = 0; i < op->value.size(); ++i) {
    unsigned char c = op->value[i];
    if (c >= ' ' && c <= '~' && c != '\\' && c != '"')
      stream << c;
    else {
      stream << '\\';
      switch (c) {
        case '"':
          stream << '"';
          break;
        case '\\':
          stream << '\\';
          break;
        case '\t':
          stream << 't';
          break;
        case '\r':
          stream << 'r';
          break;
        case '\n':
          stream << 'n';
          break;
        default:
          std::string hex_digits = "0123456789ABCDEF";
          stream << 'x' << hex_digits[c >> 4] << hex_digits[c & 0xf];
        }
      }
    }
    stream << '"';
}

void IRPrinter::visit(const Cast* op) {
  stream << op->type << '(';
  print(op->value);
  stream << ')';
}

void IRPrinter::visit(const rsqrt* op) {
  stream << "rsqrt" << '(';
  print(op->value);
  stream << ')';
}

void IRPrinter::visit(const Variable* op) {
  stream << op->name;
}

void IRPrinter::visit(const Add* op) {
  stream << '(';
  print(op->a);
  stream << " + ";
  print(op->b);
  stream << ')';
}

void IRPrinter::visit(const Sub* op) {
  stream << '(';
  print(op->a);
  stream << " - ";
  print(op->b);
  stream << ')';
}

void IRPrinter::visit(const Mul* op) {
  stream << '(';
  print(op->a);
  stream << " * ";
  print(op->b);
  stream << ')';
}

void IRPrinter::visit(const Div* op) {
  stream << '(';
  print(op->a);
  stream << " / ";
  print(op->b);
  stream << ')';
}

void IRPrinter::visit(const Mod* op) {
  stream << '(';
  print(op->a);
  stream << " % ";
  print(op->b);
  stream << ')';
}

void IRPrinter::visit(const Min* op){
  stream << "min(";
  print(op->a);
  stream << ", ";
  print(op->b);
  stream << ")";
}

void IRPrinter::visit(const Max* op){
  stream << "max(";
  print(op->a);
  stream << ", ";
  print(op->b);
  stream << ")";
}

void IRPrinter::visit(const ComparisonOp* op) {
  stream << '(';
  print(op->a);
  stream << op->op;
  print(op->b);
  stream << ')';
}

void IRPrinter::visit(const Call* op) {
  stream << op->name << '(';
  for (size_t i = 0; i < op->args.size(); ++i) {
    print(op->args[i]);
    if (i < op->args.size() - 1)
      stream << ", ";
  }
  stream << ')';
}

void IRPrinter::visit(const Load* op) {
  stream << op->name << "[";
  print(op->index);
  stream << "]";
}

void IRPrinter::visit(const Vector* op) {
  stream << "vector(";
  print(op->base);
  stream << ", ";
  print(op->stride);
  stream << ", " << op->width << ")";
}

void IRPrinter::visit(const VectorSet* op) {
  stream << "x" << op->width << "(";
  print(op->value);
  stream << ")";
}

void IRPrinter::visit(const Assign* op) {
  indent();
  std::string isStatic = "";
  if(op->staticAssign) isStatic = "static ";
  stream << "assign " << isStatic << op->name << " = ";
  print(op->value);
  stream << '\n';
}

void IRPrinter::visit(const Let* op) {
    stream << "(let " << op->name << " = ";
    print(op->value);
    stream << " in ";
    print(op->body);
    stream << ")";
}

void IRPrinter::visit(const LetStmt* op) {
  indent();
  stream << "let " << op->name << " = ";
  print(op->value);
  stream << '\n';

  print(op->body);
}

void IRPrinter::visit(const Select* op) {
  stream << "select(";
  print(op->cond);
  stream << ", ";
  print(op->true_value);
  stream << ", ";
  print(op->false_value);
  stream << ")";
}

void IRPrinter::visit(const For* op) {
  indent();
  stream << op->ftype << " (" << op->name << ", ";
  print(op->begin);
  stream << ", ";
  print(op->end);
  stream << ", ";
  print(op->stride);
  stream << ") {\n";

  indent_level += 2;
  print(op->body);
  indent_level -= 2;

  indent();
  stream << "}\n";
}

void IRPrinter::visit(const While* op) {
  indent();
  stream << "while (" << op->name << ", ";
  print(op->condition);
  stream << ") {\n";

  indent_level += 2;
  print(op->body);
  indent_level -= 2;

  indent();
  stream << "}\n";
}

void IRPrinter::visit(const Store* op) {
  indent();
  stream << op->name << "[";
  print(op->index);
  stream << "] = ";
  print(op->value);
  stream << '\n';
}

void IRPrinter::visit(const MultiStore* op) {
  indent();
  stream << op->name << "(";
  for (size_t i = 0; i < op->args.size(); ++i) {
    print(op->args[i]);
    if (i < op->args.size() - 1)
      stream << ", ";
  }
  stream << ") = ";
  print(op->value);
  stream << '\n';
}

void IRPrinter::visit(const Realize* op) {
  indent();
  stream << "realize " << op->name << "(";
  for (size_t i = 0; i < op->bounds.size(); ++i) {
    stream << "[";
    print(op->bounds[i].min);
    stream << ", ";
    print(op->bounds[i].max);
  }
  stream << ") {\n";

  indent_level += 2;
  print(op->body);
  indent_level -= 2;

  indent();
  stream << "}\n";
}

void IRPrinter::visit(const Evaluate* op) {
  indent();
  print(op->value);
  stream << "\n";
}

void IRPrinter::visit(const OrderedArray_Insert* op){
	indent();
	stream << "OrderedArray_Insert( " << op->arrayName << " , " << op->k << " , ";
	print(op->value);
	stream << " , ";
	print(op->index);
	stream << " )\n";
}




}

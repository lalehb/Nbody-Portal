#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Binary_tree.h"
#include "Hrect_bounds.h"
#include "range_search.h"
#include "Clock.hpp"

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <filename> <min> <max> <level>\n", use);
}

bool validate (int num_pts, int dim, Range& r, Points_t& sources, Points_t& targets, resultNeighbors& result) {
  bool error = false;

  /* Set tolerance value for validation */
	const real_t tolerance = 1e-2;

  /* Allocate memory for exhaustive calculation of near neighbors */
  kNeighbors nearest;

  /* Evaluate test/target point one by one */
  for (int i = 0; i < num_pts; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    for (int j = 0; j < num_pts; j++) {
      real_t dist = 0.0;
      for (int d = 0; d < dim; d++)
        dist += (sources[j][d] - targets[i][d]) * (sources[j][d] - targets[i][d]);
      if (r.contains(dist))
        nearest.push_back(kNN_distance(j, dist));
    }

    /* Sort the source points by its distance to the target points */
    sort (nearest.begin(), nearest.end());

    /* Sort the neighbor vector in the increasing order of distance */
    kNeighbors kneighbors = result[i];
    sort (kneighbors.begin(), kneighbors.end());

    for (size_t j = 0; j < kneighbors.size(); j++) {
      if (fabs(kneighbors[j].squared_distance - nearest[j].squared_distance) >= tolerance) {
        cerr << "Target point: " << i << endl;
        cerr << "Nearest neighbor " << j << " failed!\n";
        cerr << "Correct index: " << nearest[j].index << endl;
        cerr << "Correct distance: " << nearest[j].squared_distance << endl;
        cerr << "Computed index: " << kneighbors[j].index << endl;
        cerr << "Computed distance: " << kneighbors[j].squared_distance << endl;
        error = true;
        exit(0);
      }
    }
    nearest.clear();
  }
  return error;
}

int main (int argc, char** argv)
{
  char* filename;
  int num_pts;
  int dim;
  real_t min, max;
  real_t epsilon = 0.0;  /* Set to non-zero value for approximate k-nn */
  bool error = false;
  Clock timer;
  int level = 10;

  if ((argc != 6) && (argc != 7)) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  filename = argv[3];
  min = atof (argv[4]);
  max = atof (argv[5]);
  if (argc == 7)
    level = atoi(argv[6]);

  /* Allocate memory for original source and target dataset */
  Points_t sources(num_pts, dim);
  Points_t targets(num_pts, dim);

  from_file(sources, targets, filename, filename);

  /* Allocate memory for the permuted source and target points */
  Points_t s_perm (num_pts, dim);
  Points_t t_perm (num_pts, dim);

  /* Create and build source and target tree */
  Tree<Hrect> stree(sources, s_perm);
  Tree<Hrect> ttree(targets, t_perm);

  fprintf (stderr, "Building source tree...\n");
  timer.start();
  stree.build_kdtree();
  double time_tree = timer.seconds();
  fprintf (stderr, "Source tree built.\n");

  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  if (!single_mode) {
    fprintf (stderr, "Building target tree...\n");
    timer.start();
    ttree.build_kdtree();
    time_tree += timer.seconds();
    fprintf (stderr, "Target tree built.\n");
  }

#ifdef _DEBUG
  std::cout << "Sources: " << std::endl;
  std::cout << sources << std::endl;

  cout << "In the main program...\n";
  std::cout << s_perm << std::endl;

  cout << "\n Tree structure...\n";
  std::cout << stree << std::endl;
#endif

  /* Get the neighbours with the range (min,max) */
  RangeSearch<Tree<Hrect>> rs(dim, level, stree, ttree, 1);

  /* Calculate k-nearest neighbors using the pruning algorithm */
  resultNeighbors result(num_pts);
  real_t sq_min = min*min;
  real_t sq_max = max*max;
  Range r(sq_min, sq_max);
	rs.search(r, num_pts, result, epsilon);

#ifdef _DEBUG
  /* Print the neighbors indices and distances */
  for (int i = 0; i < num_pts; i++) {
    kNeighbors kneighbors = result[i];
    for (size_t j = 0; j < kneighbors.size(); j++)
      cout << sqrt(kneighbors[j].squared_distance) << "  ";
    cout << endl;
  }
  for (int i = 0; i < num_pts; i++) {
    kNeighbors kneighbors = result[i];
    for (size_t j = 0; j < kneighbors.size(); j++)
      cout << kneighbors[j].index << "  ";
    cout << endl;
  }
#endif

  /* Validate the results */
  error = validate(num_pts, dim, r, sources, targets, result);

  /* Print timing results */
  cerr << "Tree construction time: " << time_tree << " seconds\n";
  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}

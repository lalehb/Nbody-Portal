#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "Utils.h"
//#include "reals_aligned.h"
#include "VectorMatrixOp.h"

template<typename Kernel>
  int
Trans<Kernel>::setup ()
{
  int np = getenv__accuracy();

  compute_sampos (np, 1.0, sampos[UE]);
  compute_sampos (np+2, 3.0, sampos[UC]);
  compute_sampos (np, 3.0, sampos[DE]);
  compute_sampos (np, 1.0, sampos[DC]);

  compute_regpos (np, 1.0, regpos);

  /* Pre-compute UC2UE matrix */
  compute_UC2UE_matrix ();

  /* Pre-compute UE2UC matrices */
  UE2UC = (real_t **) malloc (sizeof(real_t*) * 2*2*2);

  for(int a=0; a<2; a++) {
    for(int b=0; b<2; b++) {
      for(int c=0; c<2; c++) {
        Index3 idx(a, b, c);
        compute_UE2UC_matrix (idx);
      }
    }
  }

  /* Pre-compute UE2DC matrices */
  compute_UE2DC_matrix ();

  /* Pre-compute DC2DE matrix */
  compute_DC2DE_matrix ();

  /* Pre-compute DE2DC matrices */
  DE2DC = (real_t **) malloc (sizeof(real_t*) * 2*2*2);

  for(int a=0; a<2; a++) {
    for(int b=0; b<2; b++) {
      for(int c=0; c<2; c++) {
        Index3 idx(a, b, c);
        compute_DE2DC_matrix (idx);
      }
    }
  }
  return 0;
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::compute_sampos(int np, real_t R, Points& pos)
{
  int n = np*np*np - (np-2)*(np-2)*(np-2);

  /* Allocate memory for pos */
  pos = Points(n);

  real_t step = 2.0/(np-1);
  real_t init = -1.0;
  int cnt = 0;
  for(int i=0; i<np; i++)
    for(int j=0; j<np; j++)
      for(int k=0; k<np; k++) {
        if(i==0 || i==np-1 || j==0 || j==np-1 || k==0 || k==np-1) {
          real_t x = init + i*step;
          real_t y = init + j*step;
          real_t z = init + k*step;
          pos[cnt][0] = R*x;
          pos[cnt][1] = R*y;
          pos[cnt][2] = R*z;
          cnt++;
        }
      }

  return 0;
}

/* ------------------------------------------------------------------------
 */
template<typename Kernel>
  int
Trans<Kernel>::compute_regpos(int np, real_t R, Points& pos)
{
  int n = 2*np*2*np*2*np;

  /* Allocate memory for pos */
  pos = Points(n);

  real_t step = 2.0/(np-1);
  int cnt = 0;
  for (int k=0; k<2*np; k++)
    for (int j=0; j<2*np; j++)
      for (int i=0; i<2*np; i++) {
        int gi = (i<np) ? i : i-2*np;
        int gj = (j<np) ? j : j-2*np;
        int gk = (k<np) ? k : k-2*np;
        pos[cnt][0] = R * gi*step;
        pos[cnt][1] = R * gj*step;
        pos[cnt][2] = R * gk*step;
        cnt ++;
      }
  return 0;
}

/* ------------------------------------------------------------------------
 */
template<typename Kernel>
  int
Trans<Kernel>::compute_localpos (int tp, Point3 center, real_t radius, Points& pos)
{
  int dim = 3;
  Points& spos = sampos[tp];
  for (int i = 0; i < pos.size(); ++i)
    for (int j = 0; j < dim; ++j)
      pos[i][j] = center[j] + radius * spos[i][j];

  return 0;
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::eff_data_size(int tp)
{
  int np = getenv__accuracy();
  int eff_num = (2*np+2)*(2*np)*(2*np);
  if(tp==UE || tp==DE)
    return eff_num * src_dof();
  else
    return eff_num * trg_dof();
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::pln_size(int tp)
{
  if (tp==UE || tp==DE)
    return sampos[tp].size() * src_dof();
  else
    return sampos[tp].size() * trg_dof();
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  real_t
Trans<Kernel>::alt()
{
  int np = getenv__accuracy();
  return pow(0.1, np+1);
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::compute_UC2UE_matrix ()
{
  real_t R = 1;
  real_t *ud2c;

  int r = pln_size (UC);
  int c = pln_size (UE);
  ud2c = (real_t *) reals_alloc__aligned (r * c);

  Points chkpos(sampos[UC].size());
  for(auto& av : chkpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(chkpos.size() * chkpos[0].size(), R, &sampos[UC][0][0], &chkpos[0][0]);
  //daxy(R, sampos[UC], chkpos);

  Points denpos(sampos[UE].size());
  for(auto& av : denpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(denpos.size() * denpos[0].size(), R, &sampos[UE][0][0], &denpos[0][0]);
  //  daxy(sampos[UE].size(), R, sampos[UE], denpos);

  knl->kernel_matrix (chkpos, denpos, ud2c);

  UC2UE = (real_t *) reals_alloc__aligned (c * r);
  pinv(ud2c, alt(), UC2UE, r, c);

  return 0;
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::compute_UE2UC_matrix (Index3 idx)
{
  int dim = 3;
  real_t R = 1;
  real_t *ue2uc;

  int r = pln_size (UC);
  int c = pln_size (UE);
  ue2uc = (real_t *) reals_alloc__aligned (r * c);

  Points chkpos(sampos[UC].size());
  for(auto& av : chkpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(chkpos.size() * chkpos[0].size(), 2.0*R, &sampos[UC][0][0], &chkpos[0][0]);
  //  daxy(sampos[UC].size(), 2.0*R, sampos[UC], chkpos);

  Points denpos(sampos[UE].size());
  for(auto& av : denpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(denpos.size() * denpos[0].size(), R, &sampos[UE][0][0], &denpos[0][0]);
  //  daxy(sampos[UE].size(), R, sampos[UE], denpos);

  // shift
  for (int j = 0; j < sampos[UE].size(); ++j)
    for (int d = 0; d < dim; ++d)
      denpos[j][d] += (2 * idx(d) - 1) * R;

  knl->kernel_matrix (chkpos, denpos, ue2uc);
  UE2UC[idx(2)+idx(1)*2+idx(0)*2*2] = ue2uc;

  return 0;
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::compute_DC2DE_matrix ()
{
  real_t R = 1;
  real_t *dd2c;

  int r = pln_size (DC);
  int c = pln_size (DE);
  dd2c = (real_t *) reals_alloc__aligned (r * c);

  Points chkpos(sampos[DC].size());
  for(auto& av : chkpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(chkpos.size() * chkpos[0].size(), R, &sampos[DC][0][0], &chkpos[0][0]);
  //  daxy(sampos[DC].size(), R, sampos[DC], chkpos);

  Points denpos(sampos[DE].size());
  for(auto& av : denpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(denpos.size() * denpos[0].size(), R, &sampos[DE][0][0], &denpos[0][0]);
  //  daxy(sampos[DE].size(), R, sampos[DE], denpos);

  knl->kernel_matrix (chkpos, denpos, dd2c);
  DC2DE = (real_t *) reals_alloc__aligned (c * r);
  pinv(dd2c, alt(), DC2DE, r, c);

  return 0;
}
/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::compute_DE2DC_matrix (Index3 idx)
{
  int dim = 3;
  real_t R = 1;
  real_t *de2dc;

  int r = pln_size (DC);
  int c = pln_size (DE);
  de2dc = (real_t *) reals_alloc__aligned (r * c);

  Points chkpos(sampos[DC].size());
  for(auto& av : chkpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(chkpos.size() * chkpos[0].size(), 0.5*R, &sampos[DC][0][0], &chkpos[0][0]);
  //  daxy(sampos[DC].size(), 0.5*R, sampos[DC], chkpos);

  Points denpos(sampos[DE].size());
  for(auto& av : denpos)
    std::fill(begin(av), end(av), 0.0);
  daxpy(denpos.size() * denpos[0].size(), R, &sampos[DE][0][0], &denpos[0][0]);
  //  daxy(sampos[DE].size(), R, sampos[DE], denpos);

  // shift
  for (int j = 0; j < sampos[DC].size(); ++j)
    for (int d = 0; d < dim; ++d)
      chkpos[j][d] += (idx(d) - 0.5) * R;

  knl->kernel_matrix (chkpos, denpos, de2dc);
  DE2DC[idx(2)+idx(1)*2+idx(0)*2*2] = de2dc;

  return 0;
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::compute_UE2DC_matrix ()
{
  int np = getenv__accuracy();
  int effnum = (2*np+2)*(2*np)*(2*np);
  real_t R = 1; // TODO: fix needed for vector kernel
  FFT_PLAN forplan;

  /* Pre-compute UE2UC matrices */
  UE2DC = (real_t **) malloc (sizeof(real_t*) * 7*7*7);

  /* Setup dummy forplan to reuse */
  int nnn[3];  nnn[0] = 2*np;  nnn[1] = 2*np;  nnn[2] = 2*np;
  real_t* tmp = reals_alloc__aligned (regpos.size()*trg_dof()*src_dof());
  real_t *UpwEqu2DwnChkii = reals_alloc__aligned (trg_dof()*src_dof() * effnum);
  forplan = FFT_CREATE(3, nnn, src_dof()*trg_dof(), tmp, NULL, src_dof()*trg_dof(), 1, (FFT_COMPLEX*)(UpwEqu2DwnChkii), NULL, src_dof()*trg_dof(), 1, FFTW_ESTIMATE);
  FFT_EXECUTE(forplan);

  for (int i1=-3; i1<=3; i1++)
    for (int i2=-3; i2<=3; i2++)
      for (int i3=-3; i3<=3; i3++)
        if (abs(i1)>1 || abs(i2)>1 || abs(i3)>1 ) {
          // compute and copy translation operator
          int idx;
          idx = (i1+3) + (i2+3)*7 + (i3+3)*7*7;

          Points denpos(1);
          denpos[0][0] = (real_t)(i1)*2.0*R; //shift
          denpos[0][1] = (real_t)(i2)*2.0*R; //shift
          denpos[0][2] = (real_t)(i3)*2.0*R; //shift

          Points chkpos(regpos.size());
          for(auto& av : chkpos)
            std::fill(begin(av), end(av), 0.0);
          daxpy(chkpos.size() * chkpos[0].size(), R, &regpos[0][0], &chkpos[0][0]);
          //      daxpy(regpos.size(), R, regpos, chkpos);

          real_t* tt = reals_alloc__aligned (regpos.size()*trg_dof()*src_dof());
          knl->kernel_matrix(chkpos, denpos, tt);

          // move data to tmp
          real_t* tmp = reals_alloc__aligned (regpos.size()*trg_dof()*src_dof());
          for(int k=0; k<regpos.size();k++) {
            for (int i = 0; i < trg_dof(); i++)
              for (int j = 0; j < src_dof(); j++)
                tmp[i + (j + k*src_dof())*trg_dof()] = tt[i + k*trg_dof() + j*regpos.size()*trg_dof()];

          }
          real_t *UpwEqu2DwnChkii = reals_alloc__aligned (trg_dof()*src_dof() * effnum);
          FFT_RE_EXECUTE(forplan, tmp, (FFT_COMPLEX*)(UpwEqu2DwnChkii));
          UE2DC[idx] = UpwEqu2DwnChkii;
        }
  return 0;
}

/* ------------------------------------------------------------------------
 */

template<typename Kernel>
  int
Trans<Kernel>::plnden_to_effden(int l, real_t* pln_den, real_t* reg_den, real_t* tmp_den)
{
  real_t degvec[src_dof()];
  knl->homogeneous_degree (degvec);
	real_t sclvec[src_dof()];
  for(int s=0; s<src_dof(); s++)
    sclvec[s] = pow(2.0, l*degvec[s]);

	int cnt = 0;
	for(int i=0; i<sampos[UE].size(); i++)
	  for(int s=0; s<src_dof(); s++) {
		  tmp_den[cnt] = pln_den[cnt] * sclvec[s];
		  cnt++;
		}
	samden_to_regden (tmp_den, reg_den);

  return 0;
}

/* ------------------------------------------------------------------------
*/
template<typename Kernel>
int
Trans<Kernel>::samden_to_regden(const real_t* sam_den, real_t* reg_den)
{
  int np = getenv__accuracy();
  int rgnum = 2*np;
  int cnt=0;
  //the order of iterating is the same as SampleGrid
  for(int i=0; i<np; i++)
	  for(int j=0; j<np; j++)
		  for(int k=0; k<np; k++) {
		    if(i==0 || i==np-1 || j==0 || j==np-1 || k==0 || k==np-1) {
			    //the position is fortran style
			    int rgoff = (k+np/2)*rgnum*rgnum + (j+np/2)*rgnum + (i+np/2);
			    for(int f=0; f<src_dof(); f++) {
				    reg_den[src_dof()*rgoff + f] = sam_den[src_dof()*cnt + f];
			    }
			    cnt++;
		    }
		  }

  return 0;
}

/* ------------------------------------------------------------------------
*/

template<typename Kernel>
int
Trans<Kernel>::regval_to_samval(const real_t* reg_val, real_t* sam_val)
{
  int np = getenv__accuracy();
  int rgnum = 2*np;
  int cnt=0;
  //the order of iterating is the same as SampleGrid
  for(int i=0; i<np; i++)
	  for(int j=0; j<np; j++)
		  for(int k=0; k<np; k++) {
		    if(i==0 || i==np-1 || j==0 || j==np-1 || k==0 || k==np-1) {
			    //the position is fortran style
			    int rgoff = (k+np/2)*rgnum*rgnum + (j+np/2)*rgnum + (i+np/2);
			    for(int f=0; f<trg_dof(); f++) {
				    sam_val[trg_dof()*cnt + f] += reg_val[trg_dof()*rgoff + f];
			    }
			    cnt++;
		    }
		  }

  return 0;
}

/* ------------------------------------------------------------------------
*/

template<typename Kernel>
  int
Trans<Kernel>::pointwise_mult (int n, double alpha, FFT_COMPLEX* x, int ix, FFT_COMPLEX* y, int iy, FFT_COMPLEX* z, int iz)
{
  int i;
/*
  SIMD_REG tmp = SIMD_SET_1;
  for (i = 0; i + SIMD_LEN <= (n * 2); i+=SIMD_LEN) {
    SIMD_REG Z = SIMD_LOAD (&(*z)[i]);
    SIMD_REG X = SIMD_LOAD (&(*x)[i]);
    SIMD_REG Y = SIMD_LOAD (&(*y)[i]);

    SIMD_REG x0 = SIMD_SHUFFLE (X, X, SHUFFLE_0);
    SIMD_REG p0 = SIMD_MUL (x0, Y);
    Z = SIMD_ADD (Z, p0);

    x0 = SIMD_SHUFFLE (X, X, SHUFFLE_1);
    SIMD_REG y0 = SIMD_SHUFFLE (Y, Y, SHUFFLE_2);
    p0 = SIMD_MUL (x0, y0);
    p0 = SIMD_MUL (p0, tmp);
    Z = SIMD_ADD (Z, p0);

    SIMD_STORE (&(*z)[i], Z);
  }
*/
  for (i = 0; i < (n * 2); i+=2) {
    (*z)[0] += alpha * ( (*x)[0] * (*y)[0] - (*x)[1] * (*y)[1]);
    (*z)[1] += alpha * ( (*x)[0] * (*y)[1] + (*x)[1] * (*y)[0]);
    x = x + ix;
    y = y + iy;
    z = z + iz;
  }
  return 0;
}


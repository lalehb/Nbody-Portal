#ifndef  _VEC3T_H_
#define  _VEC3T_H_
#include <iostream>
#include <assert.h>

using namespace std;

// 3D vector template
template <class F>
class Vec3 {
private:
  /* 3D vector data of type F */
  F _v[3];
  F mass = 0;

public:
  enum{ X=0, Y=1, Z=2 };

  /* Constructor */
  Vec3()              { _v[0]=F(0);    _v[1]=F(0);    _v[2]=F(0); }
  Vec3(F f)           { _v[0]=f;       _v[1]=f;       _v[2]=f;}
  Vec3(const F* f)    { _v[0]=f[0];    _v[1]=f[1];    _v[2]=f[2]; }
  Vec3(F a,F b,F c)   { _v[0]=a;       _v[1]=b;       _v[2]=c; }
  Vec3(const Vec3& c){ _v[0]=c._v[0]; _v[1]=c._v[1]; _v[2]=c._v[2]; }

  /* Destructor*/
  ~Vec3() {}

  /* Return pointer to _v[0] */
  operator F*()             { return &_v[0]; }
  operator const F*() const { return &_v[0]; }

  /* Return ith element of _v */
  F& operator()(int i)             { assert(i<3); return _v[i]; }
  const F& operator()(int i) const { assert(i<3); return _v[i]; }
  F& operator[](int i)             { assert(i<3); return _v[i]; }
  const F& operator[](int i) const { assert(i<3); return _v[i]; }

  /* Return x-coordinate */
  F& x()             { return _v[0];}
  const F& x() const { return _v[0];}

  /* Return y-coordinate */
  F& y()             { return _v[1];}
  const F& y() const { return _v[1];}

  /* Return z-coordinate */
  F& z()             { return _v[2];}
  const F& z() const { return _v[2];}

  /* Overloaded = operator */
  Vec3& operator= (const Vec3& c) {
    _v[0] =c._v[0]; _v[1] =c._v[1]; _v[2] =c._v[2];
    return *this;
  }

  /* Modify and return the sum of two vectors */
  Vec3& operator+=(const Vec3& c) {
    _v[0]+=c._v[0]; _v[1]+=c._v[1]; _v[2]+=c._v[2];
    return *this;
  }
  /* Modify and return the difference of two vectors */
  Vec3& operator-=(const Vec3& c) {
    _v[0]-=c._v[0]; _v[1]-=c._v[1]; _v[2]-=c._v[2];
    return *this;
  }

  /* Modify and return the product of two vectors */
  Vec3& operator*=(const Vec3& c) {
    _v[0]*=c._v[0]; _v[1]*=c._v[1]; _v[2]*=c._v[2];
    return *this;
  }

  /* Modify and return the ratio of two vectors */
  Vec3& operator/=(const Vec3& c) {
    _v[0]/=c._v[0]; _v[1]/=c._v[1]; _v[2]/=c._v[2];
    return *this;
  }

  /* L-1 norm:  sabsolute value of sum of elements in vector */
  F l1() const {
    F sum=F(0);
    for(int i=0; i<3; i++)
      sum=sum+abs(_v[i]);
    return sum;
  }

  /* L-infinity norm:  max of elements in vector */
  F linfty() const {
    F cur=F(0);
    for(int i=0; i<3; i++)
      cur=max(cur,abs(_v[i]));
    return cur;
  }

  /* L-2 norm:  square root of sum of elements */
  F l2() const {
    F sum=F(0);
    for(int i=0; i<3; i++)
      sum=sum+_v[i]*_v[i];
    return sqrt(sum);
  }

  /* Length = L-2 norm */
  F length() const {
    return l2();
  }

  /* Unit vector in director of this */
  Vec3 dir() const {
    F a=l2();
    return (*this)/a;
  }

   /* return the Euclidean distance from another Vec3 */
   F sq_euclidean_distance( Vec3<F> w) {
     F sum = F(0);
     for(int i = 0 ; i < 3; i++) {
       sum += (_v[i] - w[i]) * (_v[i] - w[i]);
     }
     return sum;
   }
   void set_mass (F m) {
     mass = m;
   }
   F get_mass () {
     return mass ;
   }

};

/* Boolean == overloaded operator returns true if a==b */
template <class F>
inline bool operator==(const Vec3<F>& a, const Vec3<F>& b) {
  bool res = true;
  for(int i=0; i<3; i++)
    res = res && (a(i)==b(i));
  return res;
}

/* Boolean != overloaded operator returns true if a!=b */
template <class F>
inline bool operator!=(const Vec3<F>& a, const Vec3<F>& b) {
  return !(a==b);
}

/* Boolean > overloaded operator returns true if a > b in BOTH x and y-directions */
template <class F>
inline bool operator> (const Vec3<F>& a, const Vec3<F>& b) {
  bool res = true;
  for(int i=0; i<3; i++)
    res = res && (a(i)> b(i));
  return res;
}

/* Boolean < overloaded operator returns true if a < b in BOTH x and y-directions */
template <class F>
inline bool operator< (const Vec3<F>& a, const Vec3<F>& b) {
  bool res = true;
   for(int i=0; i<3; i++)
    res = res && (a(i)< b(i));
  return res;
}

/* Boolean >= overloaded operator returns true if a >= b in BOTH x and y-directions */
template <class F>
inline bool operator>=(const Vec3<F>& a, const Vec3<F>& b) {
  bool res = true;
  for(int i=0; i<3; i++)
    res = res && (a(i)>=b(i));
  return res;
}

/* Boolean <= overloaded operator returns true if a <= b in BOTH x and y-directions */
template <class F>
inline bool operator<=(const Vec3<F>& a, const Vec3<F>& b) {
  bool res = true;
  for(int i=0; i<3; i++)
    res = res && (a(i)<=b(i));
  return res;
}

/* Return negation of vector a */
template <class F>
inline Vec3<F> operator- (const Vec3<F>& a) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = -a[i];
  return r;
}

/* Return sum of components of a and b */
template <class F>
inline Vec3<F> operator+ (const Vec3<F>& a, const Vec3<F>& b) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = a[i]+b[i];
  return r;
}

/* Return difference of components of a and b */
template <class F>
inline Vec3<F> operator- (const Vec3<F>& a, const Vec3<F>& b) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = a[i]-b[i];
  return r;
}

/* Return product of components of a and b */
template <class F>
inline Vec3<F> operator* (const Vec3<F>& a, const Vec3<F>& b) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = a[i]*b[i];
  return r;
}

/* Return ratio of components of a and b */
template <class F>
inline Vec3<F> operator/ (const Vec3<F>& a, const Vec3<F>& b) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = a[i]/b[i];
  return r;
}

/* Return scaling of components of a by scl */
template <class F>
inline Vec3<F> operator* (const Vec3<F>& a, F scl) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = a[i]*scl;
  return r;
}

template <class F>
inline Vec3<F> operator* (F scl, const Vec3<F>& a) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = a[i]*scl;
  return r;
}

/* Return scaling of components of a by 1/scl */
template <class F>
inline Vec3<F> operator/ (const Vec3<F>& a, F scl) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = a[i]/scl;
  return r;
}

/* Return dot-product of two vectors */
template <class F>
inline F dot (const Vec3<F>& a, const Vec3<F>& b) {
  F sum=F(0);
  for(int i=0; i<3; i++)
    sum=sum+a(i)*b(i);
  return sum;
}

/* Overloaded cross product operator */
template <class F>
inline Vec3<F> operator^ (const Vec3<F>& a, const Vec3<F>& b) {
  return Vec3<F>(a(1)*b(2)-a(2)*b(1), a(2)*b(0)-a(0)*b(2), a(0)*b(1)-a(1)*b(0));
}

/* Return cross-product of two vectors */
template <class F>
inline Vec3<F> cross (const Vec3<F>& a, const Vec3<F>& b) {
  return a^b;
}

/* Return the absolute value of each component of a vector */
template <class F>
inline Vec3<F> abs(const Vec3<F>& a) {
  Vec3<F> r;
  for(int i=0; i<3; i++)
    r[i] = abs(a[i]);
  return r;
}

#endif

/*
 * This code is used to execute the EM algorithm for Guassian Mixture Model (GMM)
 * for convergence of the algorithm when the change in the liklihood goes below
 * a threshold.
 */

#include <iostream>
#include <math.h>
#include <ctime>
#include <string>
#include <fstream>
#include <cstdlib>
#include <sys/time.h>
#include "Clock.hpp"

#include <Eigen/Dense>       // for using the determinant and inverse of matrix in Gaussian
#include <Eigen/Eigenvalues> // for eigen values and eigen vectors
#include "EM.h"

using namespace Eigen;
using namespace std;
/*
struct MeanSigma{
  real_t** mean;
  real_t*** sigma;
};
*/
/**
 * Calculating the Gaussian model (multivariate pdf)
 * using Eigen library  for transpose and inverse of matrix
 */

real_t EMBase::Gaussian(Point datai, real_t* mui, real_t** sigmai) {
  MatrixXd sigmaTemp(Dim,Dim);
  VectorXd dataTemp(Dim);
  VectorXd muTemp(Dim);

  for (int i = 0; i < Dim; ++i) {
    dataTemp(i) = datai[i];
    muTemp(i) = mui[i];
    for (int j = 0; j < Dim; ++j)
      sigmaTemp(i,j) = sigmai[i][j];
  }
  VectorXd temp = dataTemp-muTemp;
  real_t result = (1/(sqrt(abs(sigmaTemp.determinant()) * pow((2* M_PI),Dim))))
    * exp(-1 * 0.5 * (real_t)(temp.transpose()* sigmaTemp.inverse() * temp));
  return result;
}

/**
 * Calculates the Log liklihood of the data given teh parameters of the model
 */

real_t EMBase::LogLiklihood() {
  real_t liklihood = 0;
  for (int i = 0; i < this->NumData; ++i) {
    real_t temp = 0;
    for (int j = 0; j < NumGaussian; ++j)
      temp += pi[j] * Gaussian(data[i], mean[j], sigma[j]);
    liklihood += log(temp);
  }
  return liklihood;
}

/**
 * Calculates the  E-step, and filles out the matrix of responsibilities (resp)
 */

void EMBase::Estep() {
  for (int i = 0; i < NumData; ++i) {
    real_t denominator = 0;
    for (int j = 0; j < NumGaussian; ++j) {
      resp[i][j] = pi[j] * Gaussian(data[i], mean[j], sigma[j]);
      denominator += resp[i][j];
    }
    for (int j = 0; j < NumGaussian; ++j)
      resp[i][j] /= denominator;
  }
}

/**
 * Calculates the  M-step of the EM algorithm for GMM and filles new values
 * for sigma, mu and pi
 */

void EMBase::Mstep() {
  SetPiMean();
  SetSigma();
}

/* Setting the new pi  and mean for M-step */
void EMBase::SetPiMean() {
  // setting the new pi
  for (int i = 0; i < NumGaussian; ++i) {
    nk[i] = 0;
    for (int j = 0; j < NumData; ++j)
      nk[i] += resp[j][i];
    pi[i] = nk[i]/NumData;
  }

  // setting the new Mean
  for (int i = 0; i < NumGaussian; ++i) {
    real_t temp[Dim];
    for (int j = 0; j < NumData; ++j) {
      for (int l = 0; l < Dim; ++l)
        temp[l] += resp[j][i] * data[j][l];
    }
    for (int l = 0; l < Dim; ++l) {
      temp[l] = temp[l]/nk[i];
      mean[i][l] = temp[l];
    }
  }
}

/* Setting the new sigma for M-step */
void EMBase::SetSigma() {
  // setting the new Sigma
  real_t eps = pow(10, -16);
  for (int i = 0; i < NumGaussian; ++i) {
    MatrixXd sigTemp(Dim,Dim);
    for (int j = 0; j < NumData; ++j) {
      VectorXd x(Dim);
      VectorXd m(Dim);
      VectorXd temp(Dim);
      for (int f = 0; f < Dim ; ++f) {
        x(f) = data[j][f];
        m(f) = mean[i][f];
        temp(f) = x(f)-m(f);
      }
      sigTemp = sigTemp + (resp[j][i]*(temp * temp.transpose()));
    }
    if (nk[i] == 0)
      nk[i] = eps;
    sigTemp = sigTemp /nk[i];

    // Adding Epsilon to the Zero places in the eigen values to make the Sigma positive-semi-definite
    real_t eps = pow(10, -6);

    SelfAdjointEigenSolver<MatrixXd> es(sigTemp);
    MatrixXd Di = es.eigenvalues().asDiagonal();
    MatrixXd V = es.eigenvectors();
    for (int i = 0; i < Dim; ++i) {
      if (Di(i,i) <= 0)
        Di(i,i) = eps ;
    }

    sigTemp = V * Di * V.inverse();
    // sigma assigning
    for (int j = 0; j < Dim; ++j) {
      for(int f = 0; f < Dim; ++f)
        sigma[i][j][f] = sigTemp(j,f);
    }
  }
}

/* EM Iterations */

void EMBase::IterationEM() {
  real_t oldLL = -INFINITY;
  Mstep();
  //Timing variables
  Clock timer;
  double m_time = 0;
  double e_time = 0;
  double l_time = 0;

  real_t newLL = LogLiklihood();
#ifdef _DEBUG
  cout << endl <<" Printing the Log Liklihood ... " << endl;
#endif
  int iter_counter = 0;
  // loop iterates until algorithm converges as determined by ConvTres
  while (!((abs(newLL - oldLL) < Threshold) or (iter_counter >= Iter))) {
    iter_counter++;
    oldLL = newLL;
    timer.start();
    Estep();
    e_time += timer.seconds();
    timer.start();
    Mstep();
    m_time += timer.seconds();
    timer.start();
    newLL = LogLiklihood();
    l_time += timer.seconds();
#ifdef _DEBUG
    cout << newLL <<endl;
    if (newLL > 0)
      cout << endl << " Positive log liklihood!" << endl;
#endif
  }
#ifdef _DEBUG
  cout  << endl <<  "Total Timing for EM base: " << endl
  << "E-step  : " << e_time << endl
  << "M-step  : " << m_time << endl
  << "LL      : "  <<  l_time << endl << endl;
#endif
}

/* Print the final result of EM algorithm */

void EMBase::ResultPrinter() {
  cout << "~~~~~~~~~~~~~~~~" <<  endl ;
  //print mean
  cout << "mean : " << endl;
  for ( int i = 0; i < NumGaussian; ++i) {
    for(int j = 0; j < Dim; ++j)
      cout << mean[i][j] << "  ";
    cout << endl;
  }
  cout << "~~~~~~~~~~~~~~~~" <<  endl ;
  //print sigma
  cout << "sigma : " << endl << "------------" << endl;

  for (int i = 0; i < NumGaussian; ++i) {
    for (int j = 0; j < Dim; ++j) {
      for (int f = 0; f < Dim; ++f)
        cout << sigma[i][j][f] << "  ";
      cout << endl;
    }
    cout  << endl << "------------"<< endl;
  }
  cout << "~~~~~~~~~~~~~~~~" <<  endl ;
  // print pi
  cout << "pi: " << endl;
  for (int i = 0; i < NumGaussian; ++i)
    cout << pi[i] << "  ";
  cout << endl;
}

void EMBase::Result1(real_t** meani, real_t*** sigmai) {
  //  mean assignment
  for ( int i = 0; i < NumGaussian; ++i) {
    for(int j = 0; j < Dim; ++j)
       meani[i][j] = mean[i][j] ;
  }
  //  sigma assignment
  for (int i = 0; i < NumGaussian ; ++i) {
    for (int j = 0; j < Dim; ++j) {
      for (int f = 0; f < Dim; ++f)
        sigmai[i][j][f] =  sigma[i][j][f];
    }
  }
}

MeanSigma EMBase::Result() {
  MeanSigma a;
  int i, j, f;
  a.mean = new real_t*[NumGaussian];
  for (i = 0; i < NumGaussian; ++i)
    a.mean[i] = new real_t[Dim];

     a.sigma = new real_t**[NumGaussian];
       for (i = 0; i < Dim; ++i) {
           a.sigma[i] = new real_t*[Dim];
               for (j = 0; j < Dim; ++j)
                    a.sigma[i][j] = new real_t[Dim];
       }
  for ( i = 0; i < NumGaussian; ++i) {
    for(j = 0; j < Dim; ++j)
       a.mean[i][j] = mean[i][j] ;
  }
  for ( i = 0; i < NumGaussian ; ++i) {
    for ( j = 0; j < Dim; ++j) {
      for ( f = 0; f < Dim; ++f)
        a.sigma[i][j][f] =  sigma[i][j][f];
    }
  }
 return a;
}

/* Constructor */
EMBase::EMBase(int n, int k , int d, int iter, real_t t, Points_t& dataInput) : data(dataInput) {
  NumData = n ;
  NumGaussian = k ;
  Dim = d ;
  Threshold = t;
  Iter = iter;
  int i;

  // initilizing mean matrix
  mean = new real_t*[NumGaussian];
  for (i = 0; i < NumGaussian; ++i)
    mean[i] = new real_t[Dim];

  // initilizing sigma matrix
  sigma = new real_t**[NumGaussian];
  for (i = 0; i < Dim; ++i) {
    sigma[i] = new real_t*[Dim];
    for (int j = 0; j < Dim; ++j)
      sigma[i][j] = new real_t[Dim];
  }

  // initilizing pi vector
  pi = new real_t[NumGaussian];
  real_t sumPi = 0;
  for (i = 0; i < NumGaussian; ++i) {
    pi[i] = rand() / real_t(RAND_MAX);
    sumPi += pi[i];
  }

  // normalizing the Pi
  for (i = 0; i < NumGaussian; ++i)
    pi[i] /= sumPi;

  // initilizing the nk
  nk = new real_t[NumGaussian];

  //initilizing responsilibities matrix
  resp = new real_t*[NumData];
  for (i = 0; i < NumData; ++i) {
    resp[i] = new real_t[NumGaussian];
    for (int j = 0; j < NumGaussian; ++j)
      resp[i][j] = rand() / real_t(RAND_MAX);
  }
}

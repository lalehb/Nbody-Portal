/**
 *  Defines a hypersphere bounding ball
 */

#ifndef INC_HSPHERE_BOUNDS_H
#define INC_HSPHERE_BOUNDS_H

#include "fixed_vector.h"
#include "Metric.h"

#include "Binary_tree.h"

class Hsphere;

typedef Tree<Hsphere>::NodeTree Box;

/* Class for hypersphere represented by a center and a
 * radius along each dimension.
 */
class Hsphere {
public:
  int dim;
  real_t radius;

  Vec lo;
  Vec hi;
  Vec lo_i;
  Vec hi_i;
  Vec center;

  /* Default constructor */
  Hsphere () {}

  /* Initalizes to the specified dimension */
  Hsphere (unsigned int d) : dim(d), radius(0), center(d), lo(d), hi(d),lo_i(d), hi_i(d) {
    for (int i = 0; i < dim; i++)
      center[i] = 0;
    }

  /* Computes the bounds of the hypersphere enclosing the dataset */
  int bounding_box (Points_t& data);

  /* Update the bounds of the sphere enclosing the dataset */
  int update_bounding_box (unsigned int d, unsigned int beg, unsigned int end,
                           unsigned* index, Points_t& data);

  /* Compute minimum box-to-point squared distance */
  real_t min_distance (const Point& point);

  /*  returns the point with minimum distance from a point */
  Vec min_point (const Point& point);

  /* returns the point with minimum distance from a box*/
  Vec min_point (Box&  box);

  /*  returns the point with maximum  distance from a point */
  Vec max_point (const Point& point);

  /* returns the point with maximum distance from a box*/
  Vec max_point (Box&  box);

  /* Compute minimum box-to-box squared distance */
  real_t min_distance (const Box& box);

  real_t max_distance (Vec& point);
  real_t max_distance (const Box& box);

  /* Compute minimum and maximum box-to-point squared distance */
  Range range_distance (const Point& point);

  /* Compute minimum and maximum box-to-box squared distance */
  Range range_distance (const Box& box);

  /* Returns the width of the hypersphere aka diameter */
  real_t width (unsigned int d) { return 2 * radius; }

  vector<double> boundary_distances(const Box& box);
  vector<Vec> boundary_points(const Box& box);
};

/**
 * Compute the bounds of the sphere enclosing the dataset
 */
int
Hsphere::bounding_box (Points_t& data) {
  size_t n = data.size();
  size_t dim = data[0].size();
  radius = 0;

  /* Calculate the centroid of the points which is also the center
     of the node geometrically */
  for (size_t i = 0; i < n; ++i)
    for (size_t d = 0; d < dim; ++d)
      center[d] += data[i][d];

  for (size_t d = 0; d < dim; ++d)
      center[d] /= n;

  /* Calculate the radius of the node */
  real_t dist;
  SquaredEuclideanMetric<Vec,Point> metric;
  for (size_t i = 0; i < n; i++) {
    dist = metric.compute_distance(center, data[i]);
    if (dist > radius)
      radius = dist;
  }
  radius = std::sqrt(radius);
  for (size_t d = 0; d < dim; ++d) {
    lo[d] = center[d]-radius;
    hi[d] = center[d]+radius;
  }

  return 0;
}

/**
 * Update the bounds of the sphere enclosing the dataset
 */
int Hsphere::update_bounding_box(unsigned d, unsigned beg, unsigned end,
                                 unsigned* index, Points_t& data)
{
  size_t n = end - beg;
  radius = 0;

  /* Update the center of the node being split */
  center[d] = 0;
  for (size_t i = beg; i < end; i++)
    center[d] += data[index[i]][d];
  center[d] /= n;

  /* Calculate the new radius of the node */
  real_t dist;
  SquaredEuclideanMetric<Vec,Point> metric;
  for (size_t i = beg; i < end; i++) {
    dist = metric.compute_distance(center, data[index[i]]);
    if (dist > radius)
      radius = dist;
  }
  radius = std::sqrt(radius);
  return 0;
}

/**
 * Compute minimum box-to-point squared distance
 */
real_t
Hsphere::min_distance (const Point& point) {
  EuclideanMetric<Vec,Point> metric;
  real_t dist = metric.compute_distance(center, point) - radius;
  dist = (dist + fabs(dist))/2;
  return dist * dist;
}

/**
 * Compute minimum box-to-box squared distance
 */
real_t
Hsphere::min_distance (const Box& box) {
  EuclideanMetric<Vec,Vec> metric;
  real_t dist = metric.compute_distance(center, box.bounds.center) - radius - box.bounds.radius;
  dist = (dist + fabs(dist))/2;
  return dist * dist;
}

/**
 * Compute minimum and maximum box-to-point squared distance
 */


vector<double>
Hsphere::boundary_distances(const Box& box) {
  vector<double> bounds_temp;
  EuclideanMetric<Vec,Vec> metric;
  real_t dist = metric.compute_distance(center, box.bounds.center);
  real_t min_dist = pow(((dist - radius - box.bounds.radius) + fabs(dist - radius - box.bounds.radius))/2, 2);
  bounds_temp.push_back(min_dist);
  real_t max_dist = pow(dist + radius + box.bounds.radius, 2);
  bounds_temp.push_back(max_dist);
}


vector<Vec>
Hsphere::boundary_points(const Box& box) {
  vector<Vec> bounds_point;
  Vec p1(dim);
  Vec p2(dim);
  EuclideanMetric<Vec,Vec> metric;
  for (size_t d = 0; d < dim; d++) {
    real_t dd = metric.compute_distance(center, box.bounds.center);
    if ( dd > (radius + box.bounds.radius)) {
      p1[d] = center[d] + radius +  box.bounds.radius;
      p2[d] = center[d] - radius -  box.bounds.radius;
    }
  }
  bounds_point.push_back(p1);
  bounds_point.push_back(p2);

}
Range
Hsphere::range_distance (const Point& point) {
  EuclideanMetric<Vec,Point> metric;
  real_t dist = metric.compute_distance(center, point);
  real_t min_dist = pow(((dist - radius) + fabs(dist - radius))/2, 2);
  real_t max_dist = pow(dist + radius, 2);
  return Range(min_dist, max_dist);
}

/**
 * Compute minimum and maximum box-to-box squared distance
 */
Range
Hsphere::range_distance (const Box& box) {
  EuclideanMetric<Vec,Vec> metric;
  real_t dist = metric.compute_distance(center, box.bounds.center);
  real_t sum_radius = radius + box.bounds.radius;
  real_t max_dist = pow(dist + sum_radius, 2);
  return max_dist;
}

real_t
Hsphere::max_distance (Vec& point) {
  EuclideanMetric<Vec,Vec> metric;
  real_t dist = metric.compute_distance(center, point);
  real_t max_dist = pow(dist + radius, 2);
  return max_dist;
}


real_t
Hsphere::max_distance (const Box& box) {
  EuclideanMetric<Vec,Vec> metric;
  real_t dist = metric.compute_distance(center, box.bounds.center);
  real_t sum_radius = radius + box.bounds.radius;
  real_t min_dist = pow(((dist - sum_radius) + fabs(dist - sum_radius))/2, 2);
  real_t max_dist = pow(dist + sum_radius, 2);
}



Vec
Hsphere::min_point (const Point& point) {
  Vec p(dim);
  EuclideanMetric<Vec,Point> metric;
  for (size_t d = 0; d < dim; d++) {
    if (metric.compute_distance(center, point) > radius)
       p[d] = center[d] - radius ;
  }
  return p;
}

Vec
Hsphere::min_point (Box& box) {
  Vec p(dim);
  EuclideanMetric<Vec,Vec> metric;
  for (size_t d = 0; d < dim; d++) {
    if (metric.compute_distance(center, box.bounds.center ) > (radius + box.bounds.radius))
       p[d] = center[d] - radius -  box.bounds.radius;
  }
  return p;
}

Vec
Hsphere::max_point (const Point& point) {
  Vec p(dim);
  EuclideanMetric<Vec,Point> metric;
  for (size_t d = 0; d < dim; d++) {
    if (metric.compute_distance(center, point) > radius)
       p[d] = center[d] + radius ;
  }
  return p;
}

Vec
Hsphere::max_point (Box& box) {
  Vec p(dim);
  EuclideanMetric<Vec,Vec> metric;
  for (size_t d = 0; d < dim; d++) {
    if (metric.compute_distance(center, box.bounds.center) > (radius + box.bounds.radius))
       p[d] = center[d] + radius +  box.bounds.radius;
  }
  return p;
}

#endif

#include <iostream>
#include "NBodyLower.h"
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "JITModule.h"
#include <math.h>
#include <ctime>

//using namespace std;
using namespace Nbody;

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif

typedef fixed_vector<real_t> Vec;


int abc = 0;
extern "C"
float EucDist(float * s1 , float * s2) {
	//if(abc == 0){
	//	std::cout << "ADDR S1 " << s1 << "\n";
	//	std::cout << "ADDR S1 " << s2 << "\n";
	//	abc++;
	//}
	float x = s1[0] - s2[0];
	float y = s1[1] - s2[1];
	x *= x;
	y *= y;
	//std::cout << "EUCDIST( (" << s1[0] << "," << s1[1] << "),(" << s2[0] << "," << s2[1] << ") )\n";
	//std::cout << s1[0] << " , " << s1[1] << " , " << s1[2] << " , " << s1[3] << " , "<< s1[4] << " , " << s1[5] << " , " << s1[6] << "\n";
	return std::sqrt(x + y);
}

float UnionFunct(float * s1 , float * s2){
	float x = s1[0] - s2[0];
	float y = s1[1] - s2[1];
	x *= x;
	y *= y;
	float c = std::sqrt(x + y);
	return c > 3 && c < 9;
}

extern "C"
float threePoint(float * s1 , float * s2 , float * s3){
	float x = s3[0] - s2[0];
	float y = s3[1] - s2[1];
	x *= x;
	y *= y;
	float c = std::sqrt(x + y);
	return c;
}

extern "C"
float tests(float * s1 , float c) {
	return c > 3 && c < 9;
}

extern "C"
float level1Funct(float * s1 , float result){
	//std::cout << "LEVEL1FUNCT( (" << s1[0] << "," << s1[1] << " , " << result << ") )\n";
	return 10 * result;
}

int main (int argc, char** argv){
	//std::cout << retFive() <<  "\n";
	std::string filePath = "/home/real_datasets/synthetic/point/G2-D2-1000000.csv"; //"/home/real_datasets/Census.csv"; //"/home/real_datasets/synthetic/point/G2-D2-10000.csv"; // ;
	NBodyExpression expr;

	Storage fileObj(filePath);


	Storage setOne("simplePointsFile.csv");
	Storage setTwo("4_points.csv");



	//expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , setOne);
	expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , setOne);
	expr.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setdos" , setTwo , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));
	//expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , setOne , PortalFunction(PortalFunction::TYPE::SQRDIST));

	NBodyExpression expr1;
	expr1.addLayer(PortalOperator(PortalOperator::OP::KMIN , 2) , setOne);
	expr1.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setdos" , setTwo , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));


	NBodyExpression expr2;
	Expr a = Variable::make("setuno");
	Expr b = Variable::make("setdos");
	Expr calc = sqrt(pow(a[0] - b[0] , 2) + pow(a[1] + b[1] , 2));
	expr2.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setuno" , setOne , Result::make("setuno") * Integer::make(2) );
	expr2.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setdos" , setTwo , calc);


	NBodyExpression expr3;
	expr3.addLayer(PortalOperator(PortalOperator::OP::UNIONARG) , "setuno" ,  setOne , reinterpret_cast<void*>(&tests));
	expr3.addLayer(PortalOperator(PortalOperator::OP::MAX) , "setdos" , setTwo , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));

	NBodyExpression expr4;
	expr4.addLayer(PortalOperator(PortalOperator::OP::SUM) , setOne);
	expr4.addLayer(PortalOperator(PortalOperator::OP::SUM) , "setdos" , setTwo , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));

	NBodyExpression expr5;
	expr5.addLayer(PortalOperator(PortalOperator::OP::FORALL) , setOne);
	expr5.addLayer(PortalOperator(PortalOperator::OP::FORALL) , setOne);
	expr5.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setdos" , setTwo , reinterpret_cast<void*>(&threePoint));


	const char* result_file = "loweredLanguage.stmt";
	expr2.compile_to_lowered_form(result_file);

	expr5.execute();
	std::cout << "EXPR5\n" << expr5.getOutput() << "\n\n";

	expr1.compile();
	expr2.compile();
	expr3.compile();
	expr4.compile();
	expr4.execute();
	std::cout << "expr4\n" << expr4.getOutput() << "\n\n";


	Vec insert(2);
	insert[0] = 1;
	insert[1] = 5;

	Vec insertAgain(2);
	insertAgain[0] = 4;
	insertAgain[1] = 3;

	//expr4.insertApproximatePoint(0 , 2 , 4 , insert);
	expr4.insertApproximatePoint(2 , 4 , insert , 0 , 4 , insertAgain);
	//expr4.insertApproximatePoint(1 , 2 , 4 , insert);
	//expr4.adjustSetIterationBounds(0 , 0 , 2);
	//expr4.execute();
	std::cout << "expr4 with insert\n" << expr4.getOutput() << "\n";

	expr3.execute();
	std::cout << "expr3 without reassign\n" << expr3.getOutput() << "\n";
	expr3.clearData();
	expr3.compile();
	expr3.layerStorage[1].set.reassign(setTwo.points());
	expr3.execute();
	std::cout << "expr3 with reassign\n" << expr3.getOutput() << "\n";

	/*std::clock_t start_compile_time = std::clock();
	expr2.compile();
	std::clock_t end_compile_time = std::clock();
	double compile_time = (double) (end_compile_time-start_compile_time) / CLOCKS_PER_SEC * 1000.0;

	std::clock_t start_execute_time = std::clock();
	expr2.execute();
	std::clock_t end_execute_time = std::clock();
	double execute_time = (double) (end_execute_time-start_execute_time) / CLOCKS_PER_SEC * 1000.0;


	std::clock_t start_both_time = std::clock();
	expr1.execute();
	std::clock_t end_both_time = std::clock();
	double both_time = (double) (end_both_time-start_both_time) / CLOCKS_PER_SEC * 1000.0;

	std::cout << "Compile and Execute time (Together): " << both_time << "\n\n";
	std::cout << "Compile time: " << compile_time << "\n";
	std::cout << "Execute time: " << execute_time << "\n";
	std::cout << "Combined time: " << (compile_time + execute_time) << "\n";*/

	int n0 = expr2.layerStorage[0].n;
	int n1 = expr2.layerStorage[1].n;

	std::cout << "N0 " << n0 << " N1 " << n1 << "\n";

	//expr.adjustSetIterationBounds(0 , )
	//expr.adjustSetIterationBounds(1 , 2 , 4);
	expr.execute();
	//expr.adjustPartitionCounter(1 , 0 , 6 , 2);

	expr2.adjustSetIterationBounds(0 , 0 , n0/2);
	expr2.adjustSetIterationBounds(1 , 0 , n1/2);
	expr2.execute();
	//std::cout << expr2.getOutput() << "\n";

	/*
	std::cout << Storage(*expr2.lwr->intermediaryResultStorage[expr2.layerStorage[1].setAccessor].first) << "\n";
	std::cout << Storage(*expr2.lwr->intermediaryResultStorage[expr2.layerStorage[1].setAccessor].second) << "\n\n";
	std::cout << "--" <<  Storage(*expr2.lwr->intermediaryResultStorage[expr2.layerStorage[0].setAccessor].first) << "\n";
	std::cout << "\n" << Storage(*expr2.lwr->intermediaryResultStorage[expr2.layerStorage[0].setAccessor].second) << "\n\n\n";
	*/

	expr2.adjustSetIterationBounds(0 , n0/2 , n0);
	expr2.adjustSetIterationBounds(1 , 0 , n1/2);
	expr2.execute();
	//std::cout << expr2.getOutput() << "\n";

	expr2.adjustSetIterationBounds(0 , 0 , n0/2);
	expr2.adjustSetIterationBounds(1 , n1/2 , n1);
	expr2.execute();
	//std::cout << expr2.getOutput() << "\n";


	expr2.adjustSetIterationBounds(0 , n0/2 , n0);
	expr2.adjustSetIterationBounds(1 , n1/2 , n1);
	expr2.execute();
	//std::cout << expr2.getOutput() << "\n";

	expr1.adjustSetIterationBounds(0 , 0 , 3);
	expr1.adjustSetIterationBounds(1 , 0 , 2);
	expr1.execute();
	expr1.adjustSetIterationBounds(0 , 3 , 6);
	expr1.adjustSetIterationBounds(1 , 0 , 2);
	expr1.execute();
	expr1.adjustSetIterationBounds(0 , 0 , 3);
	expr1.adjustSetIterationBounds(1 , 2 , 4);
	expr1.execute();
	expr1.adjustSetIterationBounds(0 , 3 , 6);
	expr1.adjustSetIterationBounds(1 , 2 , 4);
	expr1.execute();


	std::cout << "EXPR2 \n\n" << expr2.getOutput() << "\n\n";
	std::cout << "EXPR \n\n" << expr.getOutput() << "\n\n";
	std::cout << "EXPR1 \n\n" << expr1.getOutput() << "\n\n";
	/*


	expr1.adjustSetIterationBounds(0 , 0 , 2);
	expr1.adjustSetIterationBounds(1 , 0 , 4);
	expr1.execute();
	// std::cout << expr1.getOutput() << "\n";

	expr1.adjustSetIterationBounds(0 , 2 , 4);
	expr1.adjustSetIterationBounds(1 , 0 , 4);
	expr1.execute();
	// std::cout << expr.getOutput() << "\n";


	expr1.adjustPartitionCounter(1 , 0 , 4 , 2);

	expr1.adjustSetIterationBounds(0 , 0 , 2);
	expr1.adjustSetIterationBounds(1 , 0 , 4);
	expr1.execute();
	// std::cout << expr.getOutput() << "\n";


	expr1.adjustSetIterationBounds(0 , 2 , 4);
	expr1.adjustSetIterationBounds(1 , 0 , 4);
	expr1.execute();
	// std::cout << expr.getOutput() << "\n";

	int i = 0;
	while(true){
		std::cout << "WOW " << (i + 261830) << "\n";
		expr2.adjustSetIterationBounds(0 , 0 , 261830 + i);
		expr2.execute();
		i += 1;
	}
	*/
	//std::cout << "Executing part 2\n";

	//expr2.adjustSetIterationBounds(0 , 265000 , 500000);
	//expr2.execute();

	//std::cout << "Partitioned with only half of the points being run through, with out an updated counter\n";
	//std::cout << expr.getOutput() << "\n";

	//std::cout << "Partitioned with only half of the points being run through, with an updated counter\n";
	//std::cout << expr1.getOutput() << "\n";




	/*std::cout << "Computing function to lowered form...\n";
	const char* result_file = "loweredLanguage.stmt";
	expr.compile_to_lowered_form(result_file);




 	std::cout << "Compiling\n";
	//Storage output = expr.compile();
	Storage output = expr.execute();
	std::cout << "Printing\n";

	//std::cout << "Passed." << std::endl;

	//Storage output1 = expr1.execute();

	std::cout << output << "\n";
	Storage output1 = expr.execute();
	std::cout << output1 << "\n";


	//std::cout << output1 << "\n";

	//std::cout << (output == output1) << "\n";





	//std::cout << (fileObj == Storage(fileObj.points("fileobj"))) << "\n";

	//std::cout << (fileObj == Storage(fileObj.getVectorOfArray())) << "\n";

	Storage output1 = expr.execute();

	expr.adjustSetIterationBounds(0 , 0 , 2);

	Storage output2 = expr.execute();

	expr.adjustSetIterationBounds(0 , 0 , 3);
	expr.adjustSetIterationBounds(1 , 0 , 1	);

	Storage output3 = expr.execute();

	expr.adjustSetIterationBounds(0 , 1 , 3);
	expr.adjustSetIterationBounds(1 , 1 , 2	);

	Storage output4 = expr.execute();

	std::cout << output1 << "\n\n";
	std::cout << output2 << "\n\n";
	std::cout << output3 << "\n\n";
	std::cout << output4 << "\n\n";*/



	return 0;
}

#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <set>
#include <queue>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cfloat>

#include "Binary_tree.h"
#include "utils.h"

using namespace std;

#ifdef NDEBUG
#  include <assert.h>
#else               /* debugging not enabled */
#  define assert(x) /* empty macro to prevent errors */
#endif


/**
 *  Build a kd-tree iteratively.
 * \param n         Number of elements/points in the input data
 * \param dim       Number of the dimensions in the input data
 */

 template <typename Boundary>
   int
 Tree<Boundary>::build_kdtree_column_major ()
 {
   int arr_beg = 0;
   int arr_end = 1;
   int arr_count = 0;
   int child_id;
   int dim = data.size();
   int n = data[0].size();

   // cout << "dim and size: " << dim << " , " << n << "\n";

   /* Get the max number of elements that should be in grouped in any leaf node */
   unsigned int pts_max = getenv__leaf_size();

   vector<NodeTree>& tree = this->node_data;

   /* Compute the bounding box */
   Boundary bounds(dim);
   bounds.bounding_box_column_major (data);

   /* Allocate and push root node */
   tree.push_back (NodeTree(-1, -1, 0, -1, bounds));
   tree[0].beg = 0;
   tree[0].num = data[0].size();
   tree[0].level = 0;

   /* Intialize the array of indices to the D-dimensional input data */
   index.resize(data[0].size());
   std::iota(index.begin(), index.end(), 0);

   /* Build the kd-tree */

   while (arr_beg < arr_end) {

     arr_count = arr_end;
     for (int k = arr_beg; k < arr_end; ++k) {
       tree[k].nodeid = k;
       /* Check if "max" points per leaf condition is satisfied */
       if (tree[k].num > pts_max) {
         tree[k].child = arr_count;

         /* Increment the array count by 2 (since we're splitting the node into 2) */
         arr_count = arr_count + num_child();
         /* Splits node into two */
          int pivot = split_node_column_major(k);

           /* Allocate and push the left branch/leaf */
           tree.push_back (NodeTree(k, -1, tree[k].depth+1, 0, tree[k].bounds));
           child_id = tree[k].child + 0;
           tree[child_id].num = pivot - tree[k].beg;
           tree[child_id].beg = tree[k].beg;
           tree[child_id].bounds.hi[tree[k].split_dim] = tree[k].split_value;
           tree[child_id].level = tree[k].level + 1;

           /* Allocate and push the right branch/leaf */
           tree.push_back (NodeTree(k, -1, tree[k].depth+1, 1, tree[k].bounds));
           child_id = tree[k].child + 1;
           tree[child_id].num = (tree[k].beg + tree[k].num) - pivot;
           tree[child_id].beg = pivot;
           tree[child_id].bounds.lo[tree[k].split_dim] = tree[k].split_value;
           tree[child_id].level = tree[k].level + 1;


       }
     }

     arr_beg = arr_end;
     arr_end = arr_count;
   }
   /* Populate the permuted data array */
   for (int i = 0; i < n; ++i)
      for (int d = 0; d < dim; d++)
        data_perm[d][i] = data[d][index[i]];
   return 0;
 }




 template <typename Boundary>
   int
 Tree<Boundary>::build_kdtree ()
 {
   int arr_beg = 0;
   int arr_end = 1;
   int arr_count = 0;
   int child_id;
   int n = data.size();
   int dim = data[0].size();

   /* Get the max number of elements that should be in grouped in any leaf node */
   unsigned int pts_max = getenv__leaf_size();

   vector<NodeTree>& tree = this->node_data;

   /* Compute the bounding box */
   Boundary bounds(dim);
   bounds.bounding_box (data);

   /* Allocate and push root node */
   tree.push_back (NodeTree(-1, -1, 0, -1, bounds));
   tree[0].beg = 0;
   tree[0].num = data.size();
   tree[0].level = 0;

   /* Intiialize the array of indices to the D-dimensional input data */
   index.resize(data.size());
   std::iota(index.begin(), index.end(), 0);

   /* Build the kd-tree */
   while (arr_beg < arr_end) {
     arr_count = arr_end;
     for (int k = arr_beg; k < arr_end; ++k) {
       tree[k].nodeid = k;

       /* Check if "max" points per leaf condition is satisfied */
       if (tree[k].num > pts_max) {
         tree[k].child = arr_count;

         /* Increment the array count by 2 (since we're splitting the node into 2) */
         arr_count = arr_count + num_child();

         /* Splits node into two */
         int pivot = split_node (k);

         /* Allocate and push the left branch/leaf */
         tree.push_back (NodeTree(k, -1, tree[k].depth+1, 0, tree[k].bounds));
         child_id = tree[k].child + 0;
         tree[child_id].num = pivot - tree[k].beg;
         tree[child_id].beg = tree[k].beg;
         tree[child_id].bounds.hi[tree[k].split_dim] = tree[k].split_value;
         tree[child_id].level = tree[k].level + 1;

         /* Allocate and push the right branch/leaf */
         tree.push_back (NodeTree(k, -1, tree[k].depth+1, 1, tree[k].bounds));
         child_id = tree[k].child + 1;
         tree[child_id].num = (tree[k].beg + tree[k].num) - pivot;
         tree[child_id].beg = pivot;
         tree[child_id].bounds.lo[tree[k].split_dim] = tree[k].split_value;
         tree[child_id].level = tree[k].level + 1;
       }
     }
     arr_beg = arr_end;
     arr_end = arr_count;
   }
   /* Populate the permuted data array */
   for (int i = 0; i < n; ++i)
     data_perm[i] = data[index[i]];

   return 0;
 }



 template <typename Boundary>
   int
 Tree<Boundary>::build_kdtree_tight ()
 {
   int arr_beg = 0;
   int arr_end = 1;
   int arr_count = 0;
   int child_id;
   int n = data.size();
   int dim = data[0].size();

   /* Get the max number of elements that should be in grouped in any leaf node */
   unsigned int pts_max = getenv__leaf_size();

   vector<NodeTree>& tree = this->node_data;

   /* Compute the bounding box */
   Boundary bounds(dim);
   bounds.bounding_box (data);

   /* Allocate and push root node */
   tree.push_back (NodeTree(-1, -1, 0, -1, bounds));
   tree[0].beg = 0;
   tree[0].num = data.size();
   tree[0].level = 0;

   /* Intiialize the array of indices to the D-dimensional input data */
   index.resize(data.size());
   std::iota(index.begin(), index.end(), 0);

   /* Build the kd-tree */
   while (arr_beg < arr_end) {
     arr_count = arr_end;
     for (int k = arr_beg; k < arr_end; ++k) {
       tree[k].nodeid = k;

       /* Check if "max" points per leaf condition is satisfied */
       if (tree[k].num > pts_max) {
         tree[k].child = arr_count;

         /* Increment the array count by 2 (since we're splitting the node into 2) */
         arr_count = arr_count + num_child();

         /* Splits node into two */
         int pivot = split_node (k);

         /* Allocate and push the left branch/leaf */
         tree.push_back (NodeTree(k, -1, tree[k].depth+1, 0, tree[k].bounds));
         child_id = tree[k].child + 0;
         tree[child_id].num = pivot - tree[k].beg;
         tree[child_id].beg = tree[k].beg;
         for (int d = 0 ; d < dim ; d++) {
           tree[child_id].bounds.hi[d] = -DBL_MAX;
           tree[child_id].bounds.lo[d] = DBL_MAX;
         }

         for (int i = tree[child_id].beg; i <  (tree[child_id].beg + tree[child_id].num) ; i++)
             for (int d = 0 ; d < dim ; d++) {
                  if (tree[child_id].bounds.hi[d] < data[index[i]][d])
                     tree[child_id].bounds.hi[d] = data[index[i]][d];
                 if (tree[child_id].bounds.lo[d] > data[index[i]][d])
                   tree[child_id].bounds.lo[d] = data[index[i]][d];
             }
         for (int d = 0 ; d < dim ; d++) {
           tree[child_id].bounds.hi[d] = tree[child_id].bounds.hi[d] < tree[k].bounds.hi[d]?
                                         tree[child_id].bounds.hi[d]: tree[k].bounds.hi[d];
           tree[child_id].bounds.lo[d] = tree[child_id].bounds.lo[d] > tree[k].bounds.lo[d]?
                                         tree[child_id].bounds.lo[d]: tree[k].bounds.lo[d];
         }
         tree[child_id].level = tree[k].level + 1;

         /* Allocate and push the right branch/leaf */
         tree.push_back (NodeTree(k, -1, tree[k].depth+1, 1, tree[k].bounds));
         child_id = tree[k].child + 1;
         tree[child_id].num = (tree[k].beg + tree[k].num) - pivot;
         tree[child_id].beg = pivot;
         for (int d = 0 ; d < dim ; d++) {
           tree[child_id].bounds.hi[d] = -DBL_MAX;
           tree[child_id].bounds.lo[d] = DBL_MAX;
         }

         for (int i = tree[child_id].beg; i <  (tree[child_id].beg + tree[child_id].num) ; i++)
             for (int d = 0 ; d < dim ; d++) {
                  if (tree[child_id].bounds.hi[d] < data[index[i]][d])
                     tree[child_id].bounds.hi[d] = data[index[i]][d];
                  if (tree[child_id].bounds.lo[d] > data[index[i]][d])
                     tree[child_id].bounds.lo[d] = data[index[i]][d];
             }
         for (int d = 0 ; d < dim ; d++) {
             tree[child_id].bounds.hi[d] = tree[child_id].bounds.hi[d] < tree[k].bounds.hi[d]?
                                           tree[child_id].bounds.hi[d]: tree[k].bounds.hi[d];
             tree[child_id].bounds.lo[d] = tree[child_id].bounds.lo[d] > tree[k].bounds.lo[d]?
                                           tree[child_id].bounds.lo[d]: tree[k].bounds.lo[d];
         }
         tree[child_id].level = tree[k].level + 1;
       }
     }
     arr_beg = arr_end;
     arr_end = arr_count;
   }
   /* Populate the permuted data array */
   for (int i = 0; i < n; ++i)
     data_perm[i] = data[index[i]];

   return 0;
 }




template <typename Boundary>
  int
Tree<Boundary>::build_kdtree_midpoint ()
{
  int arr_beg = 0;
  int arr_end = 1;
  int arr_count = 0;
  int child_id;
  int n = data.size();
  int dim = data[0].size();

  /* Get the max number of elements that should be in grouped in any leaf node */
  unsigned int pts_max = getenv__leaf_size();

  vector<NodeTree>& tree = this->node_data;

  /* Compute the bounding box */
  Boundary bounds(dim);
  bounds.bounding_box (data);

  /* Allocate and push root node */
  tree.push_back (NodeTree(-1, -1, 0, -1, bounds));
  tree[0].beg = 0;
  tree[0].num = data.size();
  tree[0].level = 0;

  /* Intialize the array of indices to the D-dimensional input data */
  index.resize(data.size());
  numbers.resize(data.size());
  for(int ll = 0; ll < data.size(); ll++)
     numbers[ll] = -1;
  int counter = 0;
  std::iota(index.begin(), index.end(), 0);

  /* Build the kd-tree */

  while (arr_beg < arr_end) {

    arr_count = arr_end;
    for (int k = arr_beg; k < arr_end; ++k) {
      tree[k].nodeid = k;
      /* Check if "max" points per leaf condition is satisfied */
      int check = 0;
      if (tree[k].num > pts_max) {

        /* Splits node into two */
        int pivot = mid_split_node(k);

        if ((pivot >= 0) && ((pivot <= (tree[k].num+ tree[k].beg)) && (pivot >= tree[k].beg)))
        {
             check = 1;

              tree[k].child = arr_count;

              /* Increment the array count by 2 (since we're splitting the node into 2) */
              arr_count = arr_count + num_child();


              /* Allocate and push the left branch/leaf */
              tree.push_back (NodeTree(k, -1, tree[k].depth+1, 0, tree[k].bounds));
              child_id = tree[k].child + 0;
              tree[child_id].num = pivot - tree[k].beg;
              tree[child_id].beg = tree[k].beg;
              tree[child_id].bounds.hi[tree[k].split_dim] = tree[k].split_value;
              tree[child_id].level = tree[k].level + 1;

              /* Allocate and push the right branch/leaf */
              tree.push_back (NodeTree(k, -1, tree[k].depth+1, 1, tree[k].bounds));
              child_id = tree[k].child + 1;
              tree[child_id].num = (tree[k].beg + tree[k].num) - pivot;
              tree[child_id].beg = pivot;
              tree[child_id].bounds.lo[tree[k].split_dim] = tree[k].split_value;
              tree[child_id].level = tree[k].level + 1;

        }
      }
    }

    arr_beg = arr_end;
    arr_end = arr_count;
  }
  /* Populate the permuted data array */
  for (int i = 0; i < n; ++i)
    data_perm[i] = data[index[i]];
  return 0;
}

template <typename Boundary>
  int
Tree<Boundary>::build_kdtree_midpoint_tight ()
{
  int arr_beg = 0;
  int arr_end = 1;
  int arr_count = 0;
  int child_id;
  int n = data.size();
  int dim = data[0].size();

  /* Get the max number of elements that should be in grouped in any leaf node */
  unsigned int pts_max = getenv__leaf_size();

  vector<NodeTree>& tree = this->node_data;

  /* Compute the bounding box */
  Boundary bounds(dim);
  bounds.bounding_box (data);

  /* Allocate and push root node */
  tree.push_back (NodeTree(-1, -1, 0, -1, bounds));
  tree[0].beg = 0;
  tree[0].num = data.size();
  tree[0].level = 0;

  /* Intialize the array of indices to the D-dimensional input data */
  index.resize(data.size());
  numbers.resize(data.size());
  for(int ll = 0; ll < data.size(); ll++)
     numbers[ll] = -1;
  int counter = 0;
  std::iota(index.begin(), index.end(), 0);

  /* Build the kd-tree */
  while (arr_beg < arr_end) {

    arr_count = arr_end;
    for (int k = arr_beg; k < arr_end; ++k) {
      tree[k].nodeid = k;
      /* Check if "max" points per leaf condition is satisfied */
      int check = 0;
      if (tree[k].num > pts_max) {

        /* Splits node into two */
        int pivot = mid_split_node(k);

        if ((pivot >= 0) && ((pivot <= (tree[k].num+ tree[k].beg)) && (pivot >= tree[k].beg)))
        {
             check = 1;

              tree[k].child = arr_count;

              /* Increment the array count by 2 (since we're splitting the node into 2) */
              arr_count = arr_count + num_child();


              /* Allocate and push the left branch/leaf */
              tree.push_back (NodeTree(k, -1, tree[k].depth+1, 0, tree[k].bounds));
              child_id = tree[k].child + 0;
              tree[child_id].num = pivot - tree[k].beg;
              tree[child_id].beg = tree[k].beg;
              for (int d = 0 ; d < dim ; d++) {
                tree[child_id].bounds.hi[d] = -DBL_MAX;
                tree[child_id].bounds.lo[d] = DBL_MAX;
              }
              for (int i = tree[child_id].beg; i <  (tree[child_id].beg + tree[child_id].num) ; i++)
                  for (int d = 0 ; d < dim ; d++) {
                       if (tree[child_id].bounds.hi[d] < data[index[i]][d])
                          tree[child_id].bounds.hi[d] = data[index[i]][d];
                      if (tree[child_id].bounds.lo[d] > data[index[i]][d])
                        tree[child_id].bounds.lo[d] = data[index[i]][d];
                  }
              for (int d = 0 ; d < dim ; d++) {
                tree[child_id].bounds.hi[d] = tree[child_id].bounds.hi[d] < tree[k].bounds.hi[d]?
                                              tree[child_id].bounds.hi[d]: tree[k].bounds.hi[d];
                tree[child_id].bounds.lo[d] = tree[child_id].bounds.lo[d] > tree[k].bounds.lo[d]?
                                              tree[child_id].bounds.lo[d]: tree[k].bounds.lo[d];
              }
              tree[child_id].level = tree[k].level + 1;

              /* Allocate and push the right branch/leaf */
              tree.push_back (NodeTree(k, -1, tree[k].depth+1, 1, tree[k].bounds));
              child_id = tree[k].child + 1;
              tree[child_id].num = (tree[k].beg + tree[k].num) - pivot;
              tree[child_id].beg = pivot;
              for (int d = 0 ; d < dim ; d++) {
                tree[child_id].bounds.hi[d] = -DBL_MAX;
                tree[child_id].bounds.lo[d] = DBL_MAX;
              }
              for (int i = tree[child_id].beg; i <  (tree[child_id].beg + tree[child_id].num) ; i++)
                  for (int d = 0 ; d < dim ; d++) {
                       if (tree[child_id].bounds.hi[d] < data[index[i]][d])
                          tree[child_id].bounds.hi[d] = data[index[i]][d];
                       if (tree[child_id].bounds.lo[d] > data[index[i]][d])
                          tree[child_id].bounds.lo[d] = data[index[i]][d];
                  }
              for (int d = 0 ; d < dim ; d++) {
                  tree[child_id].bounds.hi[d] = tree[child_id].bounds.hi[d] < tree[k].bounds.hi[d]?
                                                tree[child_id].bounds.hi[d]: tree[k].bounds.hi[d];
                  tree[child_id].bounds.lo[d] = tree[child_id].bounds.lo[d] > tree[k].bounds.lo[d]?
                                                tree[child_id].bounds.lo[d]: tree[k].bounds.lo[d];
              }
              tree[child_id].level = tree[k].level + 1;

        }
      }
    }

    arr_beg = arr_end;
    arr_end = arr_count;
  }
  /* Populate the permuted data array */
  for (int i = 0; i < n; ++i)
    data_perm[i] = data[index[i]];
  return 0;
}


template <typename Boundary>
void
Tree<Boundary>::centroids (NodeTree& s)
{
  // int dim = data[0].size();
  // vector<real_t> data_temp1(dim);
  // cout << s.index()  << " --> "<< s.begin()  << " , " << s.end() << " : " << s.size() << "\n";
  // for (size_t i = s.begin(); i < s.end(); ++i)
  //         cout << i << " , ";
  // cout << "\n";

  // cout << s.index()  << " : " << s.size() << "--> 0: " << s.bounds.lo[0] << " , " << s.bounds.hi[0] << "\n"
  //                   << "         --> 1: " << s.bounds.lo[1] << " , " << s.bounds.hi[1] << "\n\n";

  if (s.is_leaf()) {

    // if (s.level < shallow_leaf)
    //   shallow_leaf = s.level;
    // if (s.level >= deep_leaf)
    //   deep_leaf = s.level;
    // hist[s.level]++;


    // for (size_t i = s.begin(); i < s.end(); ++i) {
    //   for (size_t d = 0; d < dim; ++d)
    //     data_temp1[d] += data_perm[i][d];
    // }
  // for (size_t d = 0; d < dim; ++d)
  //    centers[s.index()][d] = data_temp1[d]/ s.size();
  }
  else {
    /* recursively compute the center on the children*/
    centroids(this->node_data[s.child]);
    centroids(this->node_data[s.child+1]);
    int size1 = this->node_data[s.child].size();
    int size2 = this->node_data[s.child+1].size();
    // for (size_t d = 0; d < dim; ++d)
    //  centers[s.index()][d] =
    //            ((centers[s.child][d] * size1) + (centers[s.child+1][d] * size2))/ (size1 +size2) ;

  }

}

/* Compute maxdist for each node, take root as parameter */
template <typename Boundary>
void
Tree<Boundary>::compute_maxdist (NodeTree& s)
{
  for (size_t i = s.begin(); i < s.end(); ++i) {
    real_t temp = s.bounds.distance_between_points(centers[s.index()],
                                                     data_perm[i]);
    if (temp > s.maxdist)
      s.maxdist = temp;
  }
  if (!s.is_leaf()) {
      compute_maxdist(this->node_data[s.child]);
      compute_maxdist(this->node_data[s.child+1]);
  }
}

/**
 *  Build a ball-tree iteratively.
 * \param n         Number of elements/points in the input data
 * \param dim       Number of the dimensions in the input data
 */

template <typename Boundary>
  int
Tree<Boundary>::build_balltree ()
{
  int arr_beg = 0;
  int arr_end = 1;
  int arr_count = 0;
  int child_id;
  int n = data.size();
  int dim = data[0].size();

  /* Get the max number of elements that should be in grouped in any leaf node */
  unsigned int pts_max = getenv__leaf_size();

  vector<NodeTree>& tree = this->node_data;

  /* Compute the bounding box */
  Boundary bounds(dim);
  bounds.bounding_box(data);

  /* Allocate and push root node */
  tree.push_back (NodeTree(-1, -1, 0, -1, bounds));
  tree[0].beg = 0;
  tree[0].num = data.size();
  tree[0].level = 0;

  /* Intiialize the array of indices to the D-dimensional input data */
  index.resize(data.size());
  std::iota(index.begin(), index.end(), 0);

  /* Build the ball-tree */
  while (arr_beg < arr_end) {
    arr_count = arr_end;
    for (int k = arr_beg; k < arr_end; ++k) {
      tree[k].nodeid = k;

      /* Check if "max" points per leaf condition is satisfied */
      if (tree[k].num > pts_max) {
        tree[k].child = arr_count;

        /* Splits node into two */
        int pivot = mid_split_node (k);
        if (pivot > 0) {
            /* Increment the array count by 2 (since we're splitting the node into 2) */
            arr_count = arr_count + num_child();

            /* Allocate and push the left branch/leaf */
            tree.push_back (NodeTree(k, -1, tree[k].depth+1, 0, tree[k].bounds));
            child_id = tree[k].child + 0;
            tree[child_id].num = pivot - tree[k].beg;
            tree[child_id].beg = tree[k].beg;
            tree[child_id].bounds.update_bounding_box(tree[k].split_dim, tree[child_id].begin(), tree[child_id].end(), index.data(), data);
            tree[child_id].level = tree[k].level + 1;

            /* Allocate and push the right branch/leaf */
            tree.push_back (NodeTree(k, -1, tree[k].depth+1, 1, tree[k].bounds));
            child_id = tree[k].child + 1;
            tree[child_id].num = (tree[k].beg + tree[k].num) - pivot;
            tree[child_id].beg = pivot;
            tree[child_id].bounds.update_bounding_box(tree[k].split_dim, tree[child_id].begin(), tree[child_id].end(), index.data(), data);
            tree[child_id].level = tree[k].level + 1;
        }
        else {
          break;
        }
      }
    }
    arr_beg = arr_end;
    arr_end = arr_count;
  }
  /* Populate the permuted data array */
  for (unsigned i = 0; i < n; ++i)
    data_perm[i] = data[index[i]];

  return 0;
}


template <typename Boundary>
int
Tree<Boundary>::split_node (int k)
{
  vector<NodeTree>& tree = this->node_data;

  /* Pick a dimension to partition the tree */
  tree[k].split_dim = get_split_dim(k);
  /* Find and store the pivot used to split the hyperspace in two */
  int pivot = (tree[k].beg + (tree[k].beg + tree[k].num)) / 2;

  /* Use nth_element to bucket the index array based on the median */
  std::nth_element(index.begin() + tree[k].beg,
                   index.begin() + pivot,
                   index.begin() + tree[k].beg + tree[k].num,
                   CompIdx(tree[k].split_dim, data));

  tree[k].split_value = data[index[pivot]][tree[k].split_dim];

  return pivot;
}


template <typename Boundary>
int
Tree<Boundary>::split_node_column_major (int k)
{
  vector<NodeTree>& tree = this->node_data;

  /* Pick a dimension to partition the tree */
  tree[k].split_dim = get_split_dim_column_major(k);
  /* Find and store the pivot used to split the hyperspace in two */
  int pivot = (tree[k].beg + (tree[k].beg + tree[k].num)) / 2;

  /* Use nth_element to bucket the index array based on the median */
  std::nth_element(index.begin() + tree[k].beg,
                   index.begin() + pivot,
                   index.begin() + tree[k].beg + tree[k].num,
                   CompIdx_column_major(tree[k].split_dim, data));

  tree[k].split_value = data[tree[k].split_dim][index[pivot]];

  return pivot;
}


/**
 * Returns the median index to split a node into two. It also re-arranges
 * the index array, so all elements to the left are less than the median
 * and those to the right are greater than the median.
 */
template <typename Boundary>
int
Tree<Boundary>::mid_split_node (int k)
{
    vector<NodeTree>& tree = this->node_data;

    int pivot = 0;
    /* Pick a dimension to partition the tree */
    tree[k].split_dim = get_split_dim(k);
    if ( (tree[k].split_dim  < 0) || (tree[k].split_dim  > data[0].size()-1)) {
        return -1;
    }

      real_t mid_box = (tree[k].bounds.lo[tree[k].split_dim] + tree[k].bounds.hi[tree[k].split_dim])/2;
       for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++)
         if (data[index[i]][tree[k].split_dim] <= mid_box)
             pivot++;

      if((pivot == tree[k].num ) && (pivot > getenv__leaf_size()))
      // if( (pivot > getenv__leaf_size()))
        pivot /=2;

      pivot += tree[k].beg;
       std::nth_element(index.begin() + tree[k].beg,
                         index.begin() + pivot,
                         index.begin() + tree[k].beg + tree[k].num,
                         CompIdx(tree[k].split_dim, data));


      tree[k].split_value = mid_box;

      return pivot;

}

template <typename Boundary>
int
Tree<Boundary>::mid_split_node_column_major (int k)
{
  vector<NodeTree>& tree = this->node_data;

  int pivot = 0;

  /* Pick a dimension to partition the tree */
  tree[k].split_dim = get_split_dim_column_major(k);
  if ( (tree[k].split_dim  < 0) || (tree[k].split_dim  > data.size()-1))
      return -1;

  real_t mid_box = (tree[k].bounds.lo[tree[k].split_dim] + tree[k].bounds.hi[tree[k].split_dim])/2;
   for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++)
     if (data[tree[k].split_dim][index[i]] <= mid_box)
         pivot++;

  /* iterate the values of index for tree construction */
  int left = tree[k].beg;
  int right = tree[k].beg + tree[k].num;
  while ((left <= right) && (data[tree[k].split_dim][index[left]] < mid_box))
        left++;
  while ((left <= right) && (right > 0) && !(data[tree[k].split_dim][index[right]] < mid_box))
        right--;
  while (left <= right) {
    /* perform swap */
    int temp = index[left];
    index[left] = index[right];
    index[right]  = temp;

    while ((left <= right) && (data[tree[k].split_dim][index[left]] < mid_box))
          left++;
    while ((left <= right) && (right > 0) && !(data[tree[k].split_dim][index[right]] < mid_box))
          right--;
  }

  tree[k].split_value = mid_box;
  return left;

}


/**
 * Returns the mid point index to split a node into two. It also re-arranges
 * the index array, so all elements to the left are less than the median
 * and those to the right are greater than the median.
 */
template <typename Boundary>
int
Tree<Boundary>::mid_split_node_tight (int k)
{
  vector<NodeTree>& tree = this->node_data;
  if (get_split_dim(k) < 0) {
    return -1;
  }
  int pivot = 0;

  /* Pick a dimension to partition the tree */
  tree[k].split_dim = get_split_dim(k);

  /* Updating the border of bounding box for tighter bounds */
  real_t min = tree[k].bounds.hi[tree[k].split_dim];
  real_t max = tree[k].bounds.lo[tree[k].split_dim];
  int min_find = 0;
  int max_find = 0;

  for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++) {
      if (data[index[i]][tree[k].split_dim] <= min){
        min = data[index[i]][tree[k].split_dim];
        min_find = 1;
      }
      if (data[index[i]][tree[k].split_dim] >= max) {
        max = data[index[i]][tree[k].split_dim];
        max_find = 1;
      }
 }
 if (min_find)
    tree[k].bounds.lo[tree[k].split_dim] = min;
 if (max_find)
    tree[k].bounds.hi[tree[k].split_dim] = max;

  real_t maxWidth = 0;
  if (min_find && max_find){
    maxWidth = fabs(max-min);
  } else{
    maxWidth = tree[k].bounds.width(tree[k].split_dim);
  }

  tree[k].split_dim = get_split_dim(k);
  maxWidth = tree[k].bounds.width(tree[k].split_dim);
  pivot = 0;
  real_t mid_box = (tree[k].bounds.lo[tree[k].split_dim] + tree[k].bounds.hi[tree[k].split_dim])/2;
   for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++)
     if (data[index[i]][tree[k].split_dim] < mid_box)
         pivot++;


  while (((pivot == 0 )  or (pivot == tree[k].num))) {

      if (get_split_dim(k) < 0) {
        break;
      }
      tree[k].split_dim = get_split_dim(k);

      real_t min = tree[k].bounds.hi[tree[k].split_dim];
      real_t max = tree[k].bounds.lo[tree[k].split_dim];
      int min_find = 0;
      int max_find = 0;

      for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++) {
          if (data[index[i]][tree[k].split_dim] <= min){
            min = data[index[i]][tree[k].split_dim];
            min_find = 1;
          }
          if (data[index[i]][tree[k].split_dim] >= max) {
            max = data[index[i]][tree[k].split_dim];
            max_find = 1;
          }
     }
     if (min_find)
        tree[k].bounds.lo[tree[k].split_dim] = min;
     if (max_find)
        tree[k].bounds.hi[tree[k].split_dim] = max;

    real_t maxWidth = 0;
    if (min_find && max_find){
      maxWidth = fabs(max-min);
    } else{
      maxWidth = tree[k].bounds.width(tree[k].split_dim);
    }
    pivot = 0;
    real_t mid_box = (tree[k].bounds.lo[tree[k].split_dim] + tree[k].bounds.hi[tree[k].split_dim])/2;
     for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++)
       if (data[index[i]][tree[k].split_dim] < mid_box)
           pivot++;

  }

  /* Use nth_element to bucket the index array based on the median */
  std::nth_element(index.begin() + tree[k].beg,
                   index.begin() + tree[k].beg + pivot,
                   index.begin() + tree[k].beg + tree[k].num,
                   CompIdx(tree[k].split_dim, data));


  tree[k].split_value = data[index[pivot+tree[k].beg]][tree[k].split_dim];

  return (pivot+tree[k].beg);
}


/**
 * Returns the mean to split a node into two. It also re-arranges
 * the index array, so all elements to the left are less than the median
 * and those to the right are greater than the median.
 */
template <typename Boundary>
int
Tree<Boundary>::mean_split_node (int k)
{
  vector<NodeTree>& tree = this->node_data;
  if (get_split_dim(k) < 0) {
    return -1;
  }
  int pivot = 0;

  /* Pick a dimension to partition the tree */
  tree[k].split_dim = get_split_dim(k);

  real_t min = tree[k].bounds.hi[tree[k].split_dim];
  real_t max = tree[k].bounds.lo[tree[k].split_dim];
  int min_find = 0;
  int max_find = 0;

  for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++) {
      if (data[index[i]][tree[k].split_dim] <= min){
        min = data[index[i]][tree[k].split_dim];
        min_find = 1;
      }
      if (data[index[i]][tree[k].split_dim] >= max) {
        max = data[index[i]][tree[k].split_dim];
        max_find = 1;
      }
 }
 if (min_find)
    tree[k].bounds.lo[tree[k].split_dim] = min;
 if (max_find)
    tree[k].bounds.hi[tree[k].split_dim] = max;

  real_t maxWidth = 0;
  if (min_find && max_find){
    maxWidth = fabs(max-min);
  } else{
    maxWidth = tree[k].bounds.width(tree[k].split_dim);
  }

  tree[k].split_dim = get_split_dim(k);
  maxWidth = tree[k].bounds.width(tree[k].split_dim);
  pivot = 0;

  real_t mid_box = 0;
  int count = 0;
  for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++) {
    mid_box += data[index[i]][tree[k].split_dim];
    count++;
  }
  mid_box /=count;
  for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++)
    if (data[index[i]][tree[k].split_dim] < mid_box)
        pivot++;


  while (((pivot == 0 )  or (pivot == tree[k].num))) {
      if (get_split_dim(k) < 0)
        break;
      tree[k].split_dim = get_split_dim(k);

      real_t min = tree[k].bounds.hi[tree[k].split_dim];
      real_t max = tree[k].bounds.lo[tree[k].split_dim];
      int min_find = 0;
      int max_find = 0;

      for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++) {
          if (data[index[i]][tree[k].split_dim] <= min){
            min = data[index[i]][tree[k].split_dim];
            min_find = 1;
          }
          if (data[index[i]][tree[k].split_dim] >= max) {
            max = data[index[i]][tree[k].split_dim];
            max_find = 1;
          }
     }
     if (min_find)
        tree[k].bounds.lo[tree[k].split_dim] = min;
     if (max_find)
        tree[k].bounds.hi[tree[k].split_dim] = max;

      real_t maxWidth = 0;
      if (min_find && max_find){
        maxWidth = fabs(max-min);
      } else{
        maxWidth = tree[k].bounds.width(tree[k].split_dim);
      }
      pivot = 0;
      real_t mid_box = 0;
      int count = 0;
      for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++) {
        mid_box += data[index[i]][tree[k].split_dim];
        count++;
      }
      mid_box /=count;

     for (int i = tree[k].beg ; i < (tree[k].beg + tree[k].num) ; i++)
       if (data[index[i]][tree[k].split_dim] < mid_box)
           pivot++;

  }
  /* Use nth_element to bucket the index array based on the median */
  std::nth_element(index.begin() + tree[k].beg,
                   index.begin() + tree[k].beg + pivot,
                   index.begin() + tree[k].beg + tree[k].num,
                   CompIdx(tree[k].split_dim, data));

  tree[k].split_value = mid_box;

  return (pivot+tree[k].beg);
}

/**
 * Returns the median index to split a node into two. It also re-arranges
 * the index array, so all elements to the left are less than the median
 * and those to the right are greater than the median.
 */
template <typename Boundary>
int
Tree<Boundary>::median_split_node (int k)
{
  vector<NodeTree>& tree = this->node_data;

 /* Pick a dimension to partition the tree */
 tree[k].split_dim = get_split_dim(k);

 /* Find and store the pivot used to split the hyperspace in two */
 int pivot = (tree[k].beg + (tree[k].beg + tree[k].num)) / 2;


 /* Use nth_element to bucket the index array based on the median */
 std::nth_element(index.begin() + tree[k].beg,
                  index.begin() + pivot,
                  index.begin() + tree[k].beg + tree[k].num,
                  CompIdx(tree[k].split_dim, data));

 tree[k].split_value = data[index[pivot]][tree[k].split_dim];

 return pivot;

}

/**
 * Returns the dimension in which the dataset has the widest spread
 * aka widest dimension.
 */
template <typename Boundary>
int
Tree<Boundary>::get_split_dim(int k)
{
  int dim = data[0].size();
  vector<NodeTree>& tree = this->node_data;

  real_t max_width = 0;
  int split_dim = 0;
  for (int d = 0; d < dim; d++) {
    real_t width = tree[k].bounds.width(d);
    if (width >= max_width) {
      max_width = width;
      split_dim = d;
    }
  }
  return split_dim;
}

template <typename Boundary>
int
Tree<Boundary>::get_split_dim_column_major(int k)
{
  int dim = data.size();
  vector<NodeTree>& tree = this->node_data;

  real_t max_width = 0;
  int split_dim = 0;
  for (int d = 0; d < dim; d++) {
    real_t width = tree[k].bounds.width(d);
    if (width >= max_width) {
      max_width = width;
      split_dim = d;
    }
  }
  return split_dim;
}

#include "IRNode.h"
#include "Function.h"

namespace Nbody {

/* All variables that are part of a function definition must be either
pure args or parameters */
struct CheckVars : public IRGraphVisitor {
  std::vector<std::string> pure_args;
  const std::string name;
  ReductionDomain rdom;

  CheckVars(const std::string& n) : name(n) {}

  using IRVisitor::visit;

  void visit(const Variable* var) {
    /* Is it a parameter? */
    if (var->param.defined())
      return;

    /* Is it a pure argument? */
    for (size_t i = 0; i < pure_args.size(); ++i) {
      if (var->name == pure_args[i])
        return;
    }

    /* Is it part of a reduction domain? */
    if (var->rdom.defined()) {
      if (!rdom.defined()) {
        rdom = var->rdom;
        return;
      }
      else if (var->rdom.same_as(rdom))
        return;
      else
        assert(false && "Multiple reduction domains in single function definition");
    }

    std::cerr << "Undefined variable in function definition: " << var->name << std::endl;
    assert(false);
  }
};

void Function::define(const std::vector<std::string>& args, Expr value) {
  assert(!name().empty() && "Function does not have a name"); 
  assert(value.defined() && "Undefined expression on the right-hand side");

  /* Check to ensure that all variables are either arguments or
  attached to some parameter */
  CheckVars check(name());
  check.pure_args = args;
  value.accept(&check);

  assert (!check.rdom.defined() && "Reduction domain in pure function definition");

  if(!contents.defined()) {
    contents = new FunctionContents;
    contents.ptr->name = generate_name('f');
  }

  assert(!contents.ptr->value.defined() && "Function is already defined");
  contents.ptr->value = value;
  contents.ptr->args = args;

  for (size_t i = 0; i < args.size(); ++i) {
    Schedule::Dim d = {args[i], 1, For::Serial};
    contents.ptr->schedule.dim.push_back(d);
  }
}

void Function::define_reduction(const std::vector<Expr>& args, Expr value) {
  assert(!name().empty() && "Function does not have a name");
  assert(has_pure_definition() && "Function needs a definition first");
  assert(value.defined() && "Undefined expression on the right-hand side");
  assert(!is_reduction() && "Function already has a reduction definition");

  /* Check to ensure dimensions match */
  assert(args.size() == dims() && "Dimensions of reduction must match pure definition");

  /* Check to ensure value types match */
  assert(value.type() == contents.ptr->value.type() && "Types of reduction must match pure definition");

  /* Get the pure arguments */
  std::vector<std::string> pure_args;
  for (size_t i = 0; i < args.size(); ++i) {
    assert(args[i].defined() && "Undefined expression in LHS of reduction");
    if (const Variable* v = args[i].ir<Variable>()) {
      if (!v->param.defined() && !v->rdom.defined()) {
        assert(v->name == contents.ptr->args[i] && "Names of reduction argument must match pure argument");
        pure_args.push_back(v->name);
      }
    }
  }

  /* Check to ensure that all variables are either arguments or
  attached to some parameter */
  CheckVars check(name());
  check.pure_args = pure_args;
  value.accept(&check);
  for (size_t i = 0; i < args.size(); ++i)
    args[i].accept(&check);

  assert (check.rdom.defined() && "No reduction domain in reduction definition");

  contents.ptr->rargs = args;
  contents.ptr->rvalue = value;
  contents.ptr->rdom = check.rdom;

  /* First, add the reduction domain */
  for (size_t i = 0; i < check.rdom.domain().size(); ++i) {
    Schedule::Dim d = {check.rdom.domain()[i].name, 1, For::Serial};
    contents.ptr->rschedule.dim.push_back(d);
  }
  
  /* Then, add the pure arguments in order */
  for (size_t i = 0; i < pure_args.size(); ++i) {
    Schedule::Dim d = {pure_args[i], 1, For::Serial};
    contents.ptr->rschedule.dim.push_back(d);
  }

}
}


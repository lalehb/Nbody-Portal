#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Binary_tree.h"
#include "Hrect_bounds.h"
#include "boruvka_emst.h"
#include "Clock.hpp"

using namespace std;

static inline
  void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <filename> <level>\n", use);
}

bool validate(size_t num_pts, size_t dim, Points_t& data, Mat& results) {
  bool error = false;
  /* Set tolerance value for validation */
  const real_t tolerance = 1e-2;

  std::vector<EdgePair> edges;
  edges.reserve(num_pts-1);
  Mat naive_results(num_pts-1, 3);

  Neighbors neighbors(num_pts);
  for (size_t i = 0; i < num_pts; ++i)
    neighbors[i].squared_distance = DBL_MAX;
  UnionFind connections(num_pts);

  /* Compute the naive minimum spanning tree */
  while (edges.size() < (num_pts - 1)) {
    for (size_t i = 0; i < num_pts; ++i) {
      /* Find the query component index */
      size_t query_component_index = connections.find(i);
      for (size_t j = 0; j < num_pts; ++j) {
        /* Find the reference component index */
        size_t reference_component_index = connections.find(j);
        /* Check if the points are in different components and if so, compute distance between them */
        if (query_component_index != reference_component_index) {
          real_t dist = 0.0;
          for (size_t d = 0; d < dim; d++)
            dist += (data[j][d] - data[i][d]) * (data[j][d] - data[i][d]);
          dist = sqrt(dist);
          /* Update current nearest neighbor */
          if (dist < neighbors[query_component_index].squared_distance) {
            assert (i != j);
            neighbors[query_component_index] = ComponentData(i, j, dist);
          }
        }
      }
    }
    /* Add all edges found to the list of neighbors */
    for (size_t i = 0; i < num_pts; ++i) {
      size_t component = connections.find(i);
      size_t inedge = neighbors[component].in;
      size_t outedge = neighbors[component].out;
      if (connections.find(inedge) != connections.find(outedge)) {
        if (inedge < outedge)
          edges.push_back(EdgePair(inedge, outedge, neighbors[component].squared_distance));
        else
          edges.push_back(EdgePair(outedge, inedge, neighbors[component].squared_distance));
        connections.merge(inedge, outedge);
      }
    }
    /* Reset the neighbors after each iteration */
    for (size_t i = 0; i < num_pts; ++i)
      neighbors[i].squared_distance = DBL_MAX;
  }
  /* Sort the edges */
  std::sort (edges.begin(), edges.end(), CompEdges);
  /* Output the results */
  for (size_t i = 0; i < edges.size(); ++i) {
    naive_results[i][0] = edges[i].first();
    naive_results[i][1] = edges[i].second();
    naive_results[i][2] = edges[i].distance();
  }

  /* Compare naive emst with the fast tree algorithm */
  for (size_t i = 0; i < naive_results.size(); ++i) {
    if (naive_results[i][0] != results[i][0] ||
        naive_results[i][1] != results[i][1] ||
        fabs(naive_results[i][2] - results[i][2]) >= tolerance) {
      cerr << "Correct in edge: " << naive_results[i][0] << endl;
      cerr << "Correct out edge: " << naive_results[i][1] << endl;
      cerr << "Correct distance: " << naive_results[i][2] << endl;
      cerr << "Computed in edge: " << results[i][0] << endl;
      cerr << "Computed out edge: " << results[i][1] << endl;
      cerr << "Computed distance: " << results[i][2] << endl;
      error = true;
      exit(0);
    }
  }
  return error;
}


int main (int argc, char** argv)
{
  char* filename;
  int num_pts;
  int dim;
  bool error = false;
  Clock timer;
  int level = 10;

  if ((argc != 4) && (argc != 5)) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  filename = argv[3];
  if (argc == 5)
    level = atoi(argv[4]);

  /* Initialize RNG with seed */
  srand48(26);

  /* Allocate memory for original dataset */
  Points_t data(num_pts, dim);

  from_file(data, data, filename, filename);

  /* Allocate memory for the permuted points */
  Points_t data_perm(num_pts, dim);

  /* Create and build tree */
  Tree<Hrect> tree(data, data_perm);

  fprintf (stderr, "Building tree...\n");
  timer.start();
  tree.build_kdtree ();
  double time_tree = timer.seconds();
  fprintf (stderr, "Tree built.\n");

#ifdef _DEBUG
  for (int i = 0; i < num_pts; i++) {
    cout << data[i][0] << " ";
    for (int j = 1; j < dim; j++) {
      cout << ",";
      cout << data[i][j];
    }
    cout << endl;
  }

  cout << "In the main program...\n";
  for (int i = 0; i < num_pts; i++) {
    for (int j = 0; j < dim; j++)
      cout << data_perm[i][j] << " ";
    cout << endl;
  }

  for (int i = 0; i < num_pts; i++) {
      cout << tree.index[i] << " ";
  }
  cout << endl;

  cout << "\n Tree structure...\n";
  vector<Tree<Hrect>::NodeTree>& ntree = tree.nodes;
  for (size_t i = 0; i < ntree.size(); i++) {
    fprintf (stderr, "node: %d  beg: %d  num: %d child: %d\n", i, ntree[i].beg, ntree[i].num, ntree[i].child);
    if (ntree[i].child == -1) {
      for (int j = ntree[i].beg; j < ntree[i].beg+ntree[i].num; j++) {
        fprintf (stderr, "%d --> ", j);
        for (int k = 0; k < dim; k++)
          fprintf (stderr, "%lf \t", tree.data[j][k]);
        fprintf (stderr, "\n");
      }
    }
  }
#endif

  /* Compute the minimum spanning tree */
  MST<Tree<Hrect>> emst(num_pts, level, dim, tree, 1);

  Mat results(num_pts-1, 3);
  emst.compute_mst(results);

#ifdef _DEBUG
  /* Print the edges and distances */
  for (size_t i = 0; i < results.size(); i++) {
    cout << results[i][0] << "->" << results[i][1] << "=" << results[i][2] << endl;
  }
#endif

  /* Validate the results */
  error = validate(num_pts, dim, data, results);

  /* Print timing results */
  cerr << "Tree construction time: " << time_tree << " seconds\n";
  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}

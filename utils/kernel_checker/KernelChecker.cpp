#include <iostream>
#include <string>
#include <ginac/ginac.h>
#include <ginac/flags.h>

#include "KernelChecker.hpp"

using namespace std;
using namespace GiNaC;


// Macro defs for user defined GiNaC functions
// Format is DECLARE_FUNCTION_<N>P(<func name>) where N = # params
// float_32 transform
DECLARE_FUNCTION_1P(float32);
REGISTER_FUNCTION(float32, eval_func(ret_contents).evalf_func(ret_contents));

// float_64 transform
DECLARE_FUNCTION_1P(float64);
REGISTER_FUNCTION(float64, eval_func(ret_contents).evalf_func(ret_contents));
	
// sqrt_f32 transform
DECLARE_FUNCTION_1P(sqrt_f32);
REGISTER_FUNCTION(sqrt_f32, eval_func(sqrt_).evalf_func(sqrt_));

// sqrt_f64 transform
DECLARE_FUNCTION_1P(sqrt_f64);
REGISTER_FUNCTION(sqrt_f64, eval_func(sqrt_).evalf_func(sqrt_));

// pow_f32 transform
DECLARE_FUNCTION_2P(pow_f32);
REGISTER_FUNCTION(pow_f32, eval_func(pow_).evalf_func(pow_));

// pow_f64 transform
DECLARE_FUNCTION_2P(pow_f64);
REGISTER_FUNCTION(pow_f64, eval_func(pow_).evalf_func(pow_));

// rqrt transform
DECLARE_FUNCTION_1P(rsqrt);
REGISTER_FUNCTION(rsqrt, eval_func(rsqrt_).evalf_func(rsqrt_));

	// Function Test Section

	// check if input ex is within threashold
	// input diff should be a numerical value and should be checked before calling this function
	bool KernelChecker::checkInThreshold(ex diff) {
		if (diff >= 0) {
			if (diff < threshold)
				return true;
		}
		else if (-1 * diff < threshold) {
			return true;
		}
		return false;
	}

	bool KernelChecker::analyze_deriv() {
		ex deriv = func.diff(x, 1);
		//cout << deriv << endl;
		if (is_a<numeric>(deriv)) {
			if (deriv.info(info_flags::positive)) {
				printf("[Analyze_Deriv] Function is always increasing!\n");
				return true;
			}
			else if (deriv.info(info_flags::negative)) {
				printf("[Analyze_Deriv] Function is always decreasing!\n");
				return true;
			}
		}
		printf("[Analyze_Deriv] Function derivative isn't constant, conducting more tests...\n");
		return false;
	}

	// checks to see if limit of a given function aproaches a constant value or infinity
	// currently only works with 2 variables
	bool KernelChecker::analyze_limit() {
		ex prev;
		ex diff;
		bool constantLim = false;
		bool posOrNeg = true;
		bool derivChange = false;
		int i = start;

		ex inf_0;

		ex temp = 0;

		// unwrapped 1st 2 loop iterations
		prev = evalf(func.subs(x == i));
		//		cout << inf_0 << endl;

		if (is_a<numeric>(prev)) {
			i+=step;
			inf_0 = evalf(func.subs(x == i));


			diff = inf_0 - prev;
			if (diff > 0)
				posOrNeg = true;
			else if (diff < 0)
				posOrNeg = false;
			if (checkInThreshold(diff))
				constantLim = true;
			else
				constantLim = false;
			prev = inf_0;
		}
		else {
			cout << "[ERROR][Anaylze_Limit] Error with input. Input evaluated as " << prev << endl;
			return false;
		}
		// end unwrapped loop
		// main loop
		for (; i < end; i+=step) {
			// evaluate at a big number
			inf_0 = evalf(func.subs(x == i));

	//				cout << inf_0 << endl;

			if (is_a<numeric>(inf_0)) {
				diff = inf_0 - prev;

	//			cout << diff << endl;

				if (diff > 0) {
					if (!posOrNeg){
	//					cout << "Deriv sign change found at " << i <<endl;
						derivChange = true;
					}
					posOrNeg = true;
				}
				else if (diff < 0) {
					if (posOrNeg){
	//					cout << "Deriv sign change found at " << i <<endl;
						derivChange = true;
					}
					posOrNeg = false;
				}
				if (checkInThreshold(diff)) {
					constantLim = true;
				}
				else {
	//				cout << "non_constant\n";
					constantLim = false;
				}
				prev = inf_0;
			}
			else {
				cout << "[ERROR][Anaylze_Limit] Error with limit to infinity." << endl;
				return false;
			}
		}
		// end main loop

		cout << "[Anaylze_Limit] last value calculated: " << inf_0 << endl;

		bool retVal = constantLim || (!derivChange);
		if (retVal)
			cout << "[Analyze_Limit] Function has constant limit or derivative thats always pos/neg." << endl;
		else
			cout << "[Analyze_Limit] Function doesn't have constant limit or pos / neg derivative. Conducting more tests..." << endl;
		return retVal;
	}

	// analyzes the square integral of a function. 
	// Returns true if approaches a constant value
	// Returns false if not.
	bool KernelChecker::analyze_integral() {
		ex prev, curr;
		ex diff;
		bool constantIntegral = false;
		func = pow(func,2);
		ex integral;

		for (int i = start; i < end; i += step) {
			integral = adaptivesimpson(x, 1, i, func, 5);

			if (is_a<numeric>(integral)) {

	//			cout << curr << endl;

				if (i == 1) {
					prev = integral;
				}
				diff = prev - integral;

	//			cout << diff << endl;

				if (checkInThreshold(diff))
					constantIntegral = true;
				else {
					constantIntegral = false;
				}
				prev = integral;
			}
			else {
				cout << "[ERROR][Analyze_Integral] Error with integral to infinity" << endl;
				return false;
			}
		}

		if(constantIntegral)
			cout << "[Analyze_Integral] Function has constant integral." << endl;
		else
			cout << "[Analyze_Integral] Function doesn't have constant integral. No tests left..." << endl;

		return constantIntegral;

	}

	// main funciton of the KernelChecker. Takes the internal GiNaC expression and does the analyze functions
	bool KernelChecker::test() {

		cout << "-----------------\n";
		cout << func << endl;

		analyze_deriv();

		if(analyze_limit()){
			cout << "[TEST] PASSES\n\n";
			return true;
		}

		if(analyze_integral()){
			cout << "[TEST] PASSES\n\n";
			return true;
		}

		//cout << "integral: " << integral << endl;

		//integral = adaptivesimpson( x , 1 , bigNum * 2 , func, 5);

		cout << "[TEST] FAILS \n\n";
		return false;
	}
	
	// takes the Func used during creation to add symbols to symtable table
	void KernelChecker::setupTable(){
		// set up the GiNaC symbols
		Nbody::Function function = kernel.function();
		for(int i = 0; i < function.args().size(); i++){
			table[function.args().at(i)] = x;
		}
	}
	
	// take the func param and run kernel checks on it
	bool KernelChecker::check(){
		
		parser reader(table);
		
		// will be turning internals of func into a string
		ostringstream stream;
		
		// grab the internal NBody::Expr of func
		stream << kernel.function().value() << endl;
		
		ex e = reader(stream.str());
		return test();
	}

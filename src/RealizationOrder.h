#ifndef INC_REALIZATIONORDER_H
#define INC_REALIZATIONORDER_H

/* Lowering pass that determines the order in which realizations are injected. */

namespace Nbody {

void realization_order_dfs(std::string current,
      const std::map<std::string, std::set<std::string>> &graph,
      std::set<std::string> &visited,
      std::set<std::string> &result_set,
      std::vector<std::string> &order) {
  visited.insert(current);

  std::map<std::string, std::set<std::string>>::const_iterator iter = graph.find(current);
  assert(iter != graph.end());

  for (const std::string &fn : iter->second) {
    if (visited.find(fn) == visited.end()) 
      realization_order_dfs(fn, graph, visited, result_set, order);
    else if (fn != current) { 
      // Self-loops are allowed in update stages
      assert(result_set.find(fn) != result_set.end() && "Stuck in a loop computing a realization order.");
    }
  }

  result_set.insert(current);
  order.push_back(current);
}

/* Given a bunch of functions that call each other, determine an
  order in which to do the scheduling. */
std::vector<std::string> realization_order(std::string output, const std::map<std::string, Function> &env) {
  // Create a DAG
  std::map<std::string, std::set<std::string>> graph;
  for (const std::pair<std::string, Function> &caller : env) {
    std::set<std::string> &s = graph[caller.first];
      for (const std::pair<std::string, Function> &callee : find_direct_calls(caller.second)) 
        s.insert(callee.first);
    }

    std::vector<std::string> order;
    std::set<std::string> result_set;
    std::set<std::string> visited;

    realization_order_dfs(output, graph, visited, result_set, order);

    return order;
}

}

#endif

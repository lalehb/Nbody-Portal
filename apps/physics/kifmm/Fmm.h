#ifndef __FMM_H__
#define __FMM_H__

#include <boost/range/iterator_range.hpp>
#include "Trans.h"

typedef boost::iterator_range<Points::const_iterator> points_iter;
typedef Vec::iterator vec_iter;
typedef Vec::pointer vec_ptr;

using namespace std;

template <typename KernelType, typename KernelMatrixType>
class FMM {
public:
  Tree& stree;
  Tree& ttree;
  Trans<KernelMatrixType>& trans;
  KernelType& knl;

  /* Re-arranged (permuted) set of src and trg pts */
  Points s_perm;
  Points t_perm;

  /* Source density */
  Vec& charge;
  Vec charge_perm;

  /* Target potential */
  Vec& result;
  Vec result_perm;

  /* Source upward equivalent density */
  Vec src_upw_equ_den_;

  /* Source upward check value */
  Vec src_upw_chk_val_;

  /* Target downward equivalent density */
  Vec trg_dwn_equ_den_;

  /* Target downward check value */
  Vec trg_dwn_chk_val_;

  /* Effective density */
  Vec eff_den_;

  /* Effective value */
  Vec eff_val_;

  FMM(Tree& st, Tree& tt, Trans<KernelMatrixType>& tr, KernelType& k, Vec& den, Vec& pot) : stree(st), ttree(tt), trans(tr), knl(k), charge(den), result(pot) { }


  /* Setup */
  void setup();

  /* Evaluate */
  int run();

  /* Re-arrange (permute) the source points in the same order as tree-traversal */
  void permute_src();

  /* Re-arrange (permute) the target points in the same order as tree-traversal */
  void permute_trg();

  /* Upward tree traversal (S2M + M2M) */
  int up_calc();

  /* Direct evaluation (P2P) */
  int ulist_calc();

  /* Far-field evaluation (M2L) */
  int vlist_calc();

  int wlist_calc();

  int xlist_calc();

  /* Downward tree traversal (L2L + L2T) */
  int down_calc();

  /* S2M */
  int S2M(int i);

  /* M2M */
  int M2M(int i);

  /* Upward equivalent density calculation */
  void upward_density_calc (int i);

  /* Plain density to effective density (FFT of source densities) */
  int compute_fft_src ();

  /* Effective value to plain value (IFFT of target value */
  int compute_ifft_trg();

  /* L2L */
  int L2L_DE2DC(int i);
  int L2L_DC2DE(int i);

  /* L2T */
  int L2T(int i);

  /* Auxilary helper functions */
  int copy_trg_val ();
  points_iter source(int i) {
    auto sbeg = s_perm.begin() + stree.node_data[i].beg;
    auto send = sbeg + stree.node_data[i].num;
    return boost::make_iterator_range(sbeg, send);
  }

  points_iter target(int i) {
    auto tbeg = t_perm.begin() + ttree.node_data[i].beg;
    auto tend = tbeg + ttree.node_data[i].num;
    return boost::make_iterator_range(tbeg, tend);
  }

  vec_iter density(int i) {
    return charge_perm.begin() + stree.node_data[i].beg*knl.src_dof();
  }

  vec_iter potential(int i) {
    return result_perm.begin() + ttree.node_data[i].beg*knl.trg_dof();
  }

  vec_iter src_upw_chk_val_it(int i) {
    return src_upw_chk_val_.begin() + i*trans.pln_size(UC);
  }

  vec_iter src_upw_equ_den_it(int i) {
    return src_upw_equ_den_.begin() + i*trans.pln_size(UE);
  }

  vec_iter eff_den_it(int i) {
    return eff_den_.begin() + i*trans.eff_data_size(UE);
  }

  vec_iter trg_dwn_chk_val_it(int i) {
    return trg_dwn_chk_val_.begin() + i*trans.pln_size(DC);
  }

  vec_iter trg_dwn_equ_den_it(int i) {
    return trg_dwn_equ_den_.begin() + i*trans.pln_size(DE);
  }

  vec_iter eff_val_it(int i) {
    return eff_val_.begin() + i*trans.eff_data_size(DC);
  }

  vec_ptr src_upw_chk_val_data(int i) {
    return src_upw_chk_val_.data() + i*trans.pln_size(UC);
  }

  vec_ptr src_upw_equ_den_data(int i) {
    return src_upw_equ_den_.data() + i*trans.pln_size(UE);
  }

  vec_ptr eff_den_data(int i) {
    return eff_den_.data() + i*trans.eff_data_size(UE);
  }

  vec_ptr trg_dwn_chk_val_data(int i) {
    return trg_dwn_chk_val_.data() + i*trans.pln_size(DC);
  }

  vec_ptr trg_dwn_equ_den_data(int i) {
    return trg_dwn_equ_den_.data() + i*trans.pln_size(DE);
  }

  vec_ptr eff_val_data(int i) {
    return eff_val_.data() + i*trans.eff_data_size(DC);
  }

  /* Compute root mean square error */
  int validate (int num_pts, real_t &rerr);
};


#include "Setup.cpp"
#include "Evaluate.cpp"
#include "Verify.cpp"
#endif

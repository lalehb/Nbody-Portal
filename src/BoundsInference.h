#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#ifndef INC_BOUNDSINFERENCE_H
#define INC_BOUNDSINFERENCE_H

#include "IRNode.h"

namespace Nbody {

Stmt infer_bounds(Stmt s, const std::vector<std::string> &order, const std::map<std::string, Function> &env) {
  Function f = env.find(order[0])->second;

  /* Inject let statements defining the bounds of this function */
  for(size_t i = 0; i < f.args().size(); ++i) {
    DEBUG_MSG("Injecting bounds for " << f.name() << ", " << f.args()[i] << "\n");
    std::ostringstream begin_name, end_name;
    begin_name << f.name() << ".begin." << i;
    end_name << f.name() << ".end." << i;
    Expr begin = Variable::make(Int(32), begin_name.str());
    Expr end = Variable::make(Int(32), end_name.str());
    s = LetStmt::make(f.name() + ".s0." + f.args()[i] + ".begin", begin, s);
    s = LetStmt::make(f.name() + ".s0." + f.args()[i] + ".end", end, s);
  }
  if (f.reduction_domain().defined()) {
    for(size_t i = 0; i < f.args().size(); ++i) {
      std::ostringstream begin_name, end_name;
      begin_name << f.name() << ".begin." << i;
      end_name << f.name() << ".end." << i;
      Expr begin = Variable::make(Int(32), begin_name.str());
      Expr end = Variable::make(Int(32), end_name.str());
      s = LetStmt::make(f.name() + ".s1." + f.args()[i] + ".begin", begin, s);
      s = LetStmt::make(f.name() + ".s1." + f.args()[i] + ".end", end, s);
    }
    const std::vector<ReductionVariable>& dom = f.reduction_domain().domain();
    for (size_t i = 0; i < dom.size(); i++) {
      s = LetStmt::make(f.name() + ".s1." + dom[i].name + ".begin", dom[i].begin, s); 
      s = LetStmt::make(f.name() + ".s1." + dom[i].name + ".end", dom[i].end, s); 
    }
  }
  return s;
}

}

#endif

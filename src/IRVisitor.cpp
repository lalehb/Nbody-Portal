#include "IRVisitor.h"
#include "IRNode.h"

namespace Nbody {

IRVisitor::~IRVisitor() { }

void IRVisitor::visit (const Integer*) { }

void IRVisitor::visit (const Flt*) { }

void IRVisitor::visit (const Dbl*) { }

void IRVisitor::visit (const String*) { }

void IRVisitor::visit (const Cast* op) {
  op->value.accept(this);
}

void IRVisitor::visit (const rsqrt* op) {
  op->value.accept(this);
}

void IRVisitor::visit (const Variable*) { }

void IRVisitor::visit (const Add* op) {
  op->a.accept(this);
  op->b.accept(this);
}
void IRVisitor::visit (const Sub* op) {
  op->a.accept(this);
  op->b.accept(this);
}
void IRVisitor::visit (const Mul* op) {
  op->a.accept(this);
  op->b.accept(this);
}
void IRVisitor::visit (const Div* op) {
  op->a.accept(this);
  op->b.accept(this);
}

void IRVisitor::visit (const Mod* op) {
  op->a.accept(this);
  op->b.accept(this);
}

void IRVisitor::visit (const Min* op) {
  op->a.accept(this);
  op->b.accept(this);
}

void IRVisitor::visit (const Max* op) {
  op->a.accept(this);
  op->b.accept(this);
}

void IRVisitor::visit (const ComparisonOp* op) {
  op->a.accept(this);
  op->b.accept(this);
}

void IRVisitor::visit (const Call* op) {
  for (size_t i = 0; i < op->args.size(); i++)
    op->args[i].accept(this);
}

void IRVisitor::visit (const Vector* op) {
  op->base.accept(this);
  op->stride.accept(this);
}

void IRVisitor::visit (const VectorSet* op) {
  op->value.accept(this);
}

void IRVisitor::visit (const Assign* op) {
  op->value.accept(this);
}
void IRVisitor::visit (const Let* op) {
  op->value.accept(this);
  op->body.accept(this);
}
void IRVisitor::visit (const LetStmt* op) {
  op->value.accept(this);
  op->body.accept(this);
}

void IRVisitor::visit (const Select* op) {
  op->cond.accept(this);
  op->true_value.accept(this);
  op->false_value.accept(this);
}

void IRVisitor::visit (const Load* op) {
  op->index.accept(this);
  op->stride.accept(this);
}

void IRVisitor::visit (const Store* op) {
  op->value.accept(this);
  op->index.accept(this);
  op->stride.accept(this);
}

void IRVisitor::visit (const MultiStore* op) {
  op->value.accept(this);
  for (size_t i = 0; i < op->args.size(); i++)
    op->args[i].accept(this);
}

void IRVisitor::visit (const For* op) {
  op->begin.accept(this);
  op->end.accept(this);
  op->stride.accept(this);
  op->body.accept(this);
}

void IRVisitor::visit (const While* op) {
  op->condition.accept(this);
  op->body.accept(this);
}

void IRVisitor::visit (const OrderedArray_Insert* op) {

  op->value.accept(this);
  op->index.accept(this);
}


void IRVisitor::visit(const Realize* op) {
    for (size_t i = 0; i < op->bounds.size(); ++i) {
        op->bounds[i].min.accept(this);
        op->bounds[i].max.accept(this);
    }
    op->body.accept(this);
}

void IRVisitor::visit (const Evaluate* op) {
  op->value.accept(this);
}

void IRVisitor::visit (const Block* op) {
  for (size_t i = 0; i < op->stmts.size(); i ++)
    op->stmts[i].accept(this);
}

void IRVisitor::visit (const BlockStmt* op) {
  for (size_t i = 0; i < op->stmts.size(); i ++)
    op->stmts[i].accept(this);
}

void IRVisitor::visit(const SSO* op){}

// void IRVisitor::visit(const Transpose* op){}
void IRVisitor::visit(const Determinant* op){}
// void IRVisitor::visit(const Inverse* op){}

void IRGraphVisitor::include (const Expr& e) {
  if (visited.count(e.ptr))
    return;
  else {
    visited.insert(e.ptr);
    e.accept(this);
    return;
  }
}

void IRGraphVisitor::include (const Stmt& s) {
  if (visited.count(s.ptr))
    return;
  else {
    visited.insert(s.ptr);
    s.accept(this);
    return;
  }
}

void IRGraphVisitor::visit (const Integer*) { }

void IRGraphVisitor::visit (const Flt*) { }

void IRGraphVisitor::visit (const Dbl*) { }

void IRGraphVisitor::visit (const String*) { }

void IRGraphVisitor::visit (const Cast* op) {
  include(op->value);
}

void IRGraphVisitor::visit (const rsqrt* op) {
  include(op->value);
}

void IRGraphVisitor::visit (const Variable*) { }

void IRGraphVisitor::visit (const Add* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const Sub* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const Mul* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const Div* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const Mod* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const Min* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const Max* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const ComparisonOp* op) {
  include(op->a);
  include(op->b);
}

void IRGraphVisitor::visit (const Call* op) {
  for (size_t i = 0; i < op->args.size(); i++)
    include(op->args[i]);
}

void IRGraphVisitor::visit (const Vector* op) {
  include(op->base);
  include(op->stride);
}

void IRGraphVisitor::visit (const VectorSet* op) {
  include(op->value);
}

void IRGraphVisitor::visit (const Assign* op) {
  include(op->value);
}
void IRGraphVisitor::visit (const Let* op) {
  include(op->value);
  include(op->body);
}
void IRGraphVisitor::visit (const LetStmt* op) {
  include(op->value);
  include(op->body);
}

void IRGraphVisitor::visit (const Select* op) {
  include(op->cond);
  include(op->true_value);
  include(op->false_value);
}

void IRGraphVisitor::visit (const Load* op) {
  include(op->index);
  include(op->stride);
}

void IRGraphVisitor::visit (const Store* op) {
  include(op->value);
  include(op->index);
  include(op->stride);
}

void IRGraphVisitor::visit (const MultiStore* op) {
  include(op->value);
  for (size_t i = 0; i < op->args.size(); i++)
    include(op->args[i]);
}

void IRGraphVisitor::visit (const For* op) {
  include(op->begin);
  include(op->end);
  include(op->stride);
  include(op->body);
}

void IRGraphVisitor::visit (const While* op) {
  include(op->condition);
  include(op->body);
}
void IRGraphVisitor::visit (const OrderedArray_Insert* op) {
  include(op->value);
  include(op->index);
}


void IRGraphVisitor::visit(const Realize* op) {
    for (size_t i = 0; i < op->bounds.size(); ++i) {
        include(op->bounds[i].min);
        include(op->bounds[i].max);
    }
    include(op->body);
}

void IRGraphVisitor::visit (const Evaluate* op) {
  include(op->value);
}

void IRGraphVisitor::visit (const Block* op) {
  for (size_t i = 0; i < op->stmts.size(); i ++)
    include(op->stmts[i]);
}

void IRGraphVisitor::visit (const BlockStmt* op) {
  for (size_t i = 0; i < op->stmts.size(); i ++)
    include(op->stmts[i]);
}

void IRGraphVisitor::visit (const SSO* op){}
void IRGraphVisitor::visit (const Determinant* op){}

}

#ifndef _UNION_FIND_H_
#define _UNION_FIND_H_

/**
 * Union-Find/Disjoint set/Connected components data structure. The
 * structure tracks the components of a graph.  Each point in the graph is
 * initially in its own component.  Calling Union(x, y) unites the components
 * indexed by x and y.  Find(x) returns the index of the component containing
 * point x.
 */
class UnionFind
{
 private:
  size_t* parent;
  unsigned int* rank;

 public:
  /* Construct an union find data structure with n isolated sets */
  UnionFind(const size_t n) 
  {
    parent = new size_t[n];
    rank = new unsigned int[n];
    for (size_t i = 0; i < n; ++i)
    {
      parent[i] = i;
      rank[i] = 0;
    }
  }

  /* Destroy the object */
  ~UnionFind() { 
    delete[] parent;
    delete[] rank;
  }

  /**
   * Returns the component containing an element.
   *
   * @param x the component to be found
   * @return The index of the component containing x
   */
  size_t find(const size_t x) {
    if (parent[x] == x)
      return x;
    return find(parent[x]);
  }

  /**
   * Union(Merge) the components containing x and y.
   *
   * @param x one component
   * @param y the other component
   */
  void merge(const size_t x, const size_t y) {
    const size_t root_x = find(x);
    const size_t root_y = find(y);

    if (root_x == root_y)
      return;
    if (rank[root_x] == rank[root_y])
    {
      parent[root_y] = parent[root_x];
      rank[root_x] += 1;
    }
    else if (rank[root_x] < rank[root_y])
      parent[root_x] = root_y;
    else
      parent[root_y] = root_x;
  }
  /**
   * Are objects x and y in the same component? 
   *
   * @param x one component 
   * @param y the other component
   */
  bool connected(const size_t x, const size_t y) {
    return find(x) == find(y);
  }
}; 

#endif 

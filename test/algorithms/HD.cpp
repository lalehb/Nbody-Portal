#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> \n", use);
}



int main (int argc, char** argv){

    if (argc != 3) {
        usage__ (argv[0]);
        return -1;
    }

    /* input the target dataset */
    std::string filePathT = argv[1];
    Storage fileObjT(filePathT);

    /* input the source dataset */
    std::string filePathS = argv[2];
    Storage fileObjS(filePathS);

    Clock timer;

    /* defining the kernel funcion */
    Expr a = Variable::make("outterSet");
    Expr b = Variable::make("innerSet");
    Expr EuclidDist = sqrt(((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1])));

    /* defining the N-body expression using portal IRs*/
    NBodyExpression expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::MAX) , "outterSet", fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::MIN) , "innerSet" , fileObjS , EuclidDist);

    const char* result_file = "test/HD.stmt";
    expr.compile_to_lowered_form(result_file);

    /* N-body caculation with portal IRs using the tree traversal */
    timer.start();
    expr.executeTraverse();
    cout << "Total time: " << timer.seconds() << endl;

    Storage output = expr.getOutput();
    bool valid = expr.Validate(output);


    if (valid) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }

	  return 0;
}

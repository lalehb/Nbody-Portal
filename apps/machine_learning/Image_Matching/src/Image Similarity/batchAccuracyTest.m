% accuracyTest()
% By Daniel Jooryabi
% Batch test entire sets of images with accuracyTest() 


function batchAccuracyTest()

% feel free to add more items to the test set

SET = {'dollar_bill', ... % reference image
    'airplanes', ...
    'beaver', ...
    'camera', ...
    'butterfly', ...
    'cellphone', ...
    'chair', ...
    'crab', ...
    'crayfish', ...
    'accordion', ...
    'flamingo', ...
    'anchor', ...
    'barrel', ...
    'cougar_face', ...
    'crab', ...
    'crocodile', ...
    'crayfish', ...
    'dalmatian', ... 
    'cup', ... 
    'dolphin', ...
    'emu', ...
    'Faces_easy', ...
    'Faces', ...
    'rooster'
    };


for p = 1: size(SET, 2)
    fprintf('%s\n', strcat(SET{1}, ' test'));
    
   
    
    d = 0;
    
    cd('C:\Users\Daniel\Documents\hpclab\apps\machine_learning\Image_Matching\res\caltech_images\');
    
    for i = 1 : 40
        
        parfor j = 1: 40
            if (i == j)
                continue;
            end
            
            d = d + daniel_dist(strcat(SET{1},'\', 'a(', num2str(i), ').jpg'), strcat(SET{1},'\','a(', num2str(j), ').jpg'));
        end
        
        d = d / 39;
        totalAvg(i) = d;
        fprintf('%.2f%% done...\n', i/40*100);
        
    end
    totalAvg = mean(totalAvg);
    
    
    printResult(SET{1}, SET{1}, totalAvg);
    
   
    
    % begin code
    n = size(SET, 2);
    clear totalAvg;
    
    
    for i = 2: n
        
        cd('C:\Users\Daniel\Documents\hpclab\apps\machine_learning\Image_Matching\res\caltech_images\');
        clear totalAvg;
        parfor j = 1 : 40
            d = 0;
            for k = 1: 40
                d = d + daniel_dist(strcat(SET{1},'/','a(', num2str(j), ').jpg'), strcat(SET{i},'/','a(', num2str(k), ').jpg'));
            end
            d = d / 40;
            totalAvg(j) = d;
        end
        
        setAverage(i) = mean(totalAvg);
        
        printResult(SET{1}, SET{i}, setAverage(i));
        
        
        
    end
    
  
    
    fprintf('\n\n\n');
    % rotate array
    SET = circshift(SET,1,2); 
    
    
end

end

function printResult(im1,im2,dist)

fprintf(strcat(im1, '<---->', im2, '===', num2str(dist), '\n'));
end
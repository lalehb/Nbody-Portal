#ifndef _RANGE_SEARCH_RULES_H_
#define _RANGE_SEARCH_RULES_H_

#include <cfloat>
#include <algorithm>
#include <vector>
#include <set>
// #include "Binary_tree.h"
#include "Metric.h"
#include "range.h"

typedef Metric<Point, Point> MetricType;
typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;

/**
 * \brief kNN-distance structure. References a feature vector by its index and its squared distance to another implicit vector.
 *
 * Implements its own comparison function with the parenthesis operator for STL-based algorithm use.
 *
 * \param T  Type used to encode the distance between two feature vectors. Should be the same than the data from the vectors.
 */
class kNN_distance {
  public:
    int index; 	/* Index of the feature vector in the data set */
    real_t squared_distance; 	/* Squared distance of the referenced element to an implicit point */

    /* Default and convenience constructors */
    kNN_distance() {}
    kNN_distance(int i, real_t sq_dist) : index(i), squared_distance(sq_dist) {}

    /* Distance comparison operator for kNN_distances. Allows kNN_distance objects to be used as STL comparison functors */
    bool operator< (const kNN_distance &v) const {
      return squared_distance < v.squared_distance;
    }
};

typedef vector<kNN_distance> kNeighbors;
typedef vector<kNeighbors> resultNeighbors;

typedef set<long> kNeighborsCheck;
typedef vector<kNeighborsCheck> resultNeighborsCheck;


/* Helper structure for tree traversal and incremental hyperrectangle-hypersphere intersection data */
template <typename Tree, typename S>
class RangeSearchRules {
  public:
    typedef typename Tree::NodeTree Box;
    int dim;

    /* The range we are searching */
    Range& range;

    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;

    /* Points to permutated source array */
    Points_t& src_data;
    /* Reference input target array */
    Points_t& trg_data;

    MetricType* metric;

    /* Current k neighbor indices */
    resultNeighbors& neighbors;

    /* Check for duplicates */
    // resultNeighborsCheck neighborsCheck;

    RangeSearchRules(int d, Range& r, Tree& st, Tree& tt, Points_t& s, Points_t& t, MetricType* m, resultNeighbors& n) : dim(d), range(r), src_tree(st), trg_tree(tt), src_data(s), trg_data(t), metric(m), neighbors(n) {}

    /* Base case calculation */
    void base_case (Box& s, int t);
    void base_case (Box& s, Box& t);
    void base_case (vector<Box*> t);

    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);
    bool prune_subtree (Box& s, Box& t);
    bool prune_subtree (vector<Box*> t);

    /* Calculate which branch to visit first */
    vector<int> visit (Box& s, int t);
    vector<int> visit (Box& s, Box& t);
    vector<int> visit (vector<Box*> t, int div);

    /* Update distance */
    void update_state (Box& s, Box& t);
    void update_state (vector<Box*> t);

    /* Update the current value of the distance to the nearest point in the hyperrectangle */
    real_t distance_update (Box& s, int t);
    real_t local_distance_update (Box& s, int t);
    real_t distance_update (Box& s, Box& t);
    real_t distance_update (vector<Box*> t);

    /* centorid case computation for the subtree is will be prunes, in this case since it's prune it does nothing*/
    void centroid_case (Box& s, int t) {};
    void centroid_case (Box& s, Box&  t) {};
    void centroid_case (vector<Box*> t) {};

    void set_temp(int temp) {};
    void adjustPartitionCounter(int , int , int , int){};
    double get_temp(){};
};

// Template implementation
template<typename Tree, typename S>
void
RangeSearchRules<Tree,S>::base_case (Box& s, int t) {
  real_t dist;

  for (size_t j = s.begin(); j < s.end(); ++j) {
    dist = metric->compute_distance (trg_data[t], src_data[j]);
    if (range.contains(dist)) {
      neighbors[t].push_back(kNN_distance(src_tree.index[j], dist));
    }
  }
}


template<typename Tree, typename S>
void
RangeSearchRules<Tree,S>::base_case (Box& s, Box& t) {
  real_t dist;
  for (size_t i = t.begin(); i < t.end(); ++i) {
    /* Check if this target point can be improved further */
    if (local_distance_update(s, i) == DBL_MAX)
      continue;

    for (size_t j = s.begin(); j < s.end(); ++j) {
      dist = metric->compute_distance (trg_data[i], src_data[j]);
      if (range.contains(dist) ) {
        kNN_distance kd(src_tree.index[j], dist);
        neighbors[i].push_back(kd);
      }
    }
  }
}

  /* k is set to 1 for KNN with two dataset and has been used
  * for the multi-tree, in the case of an algorithm with more
  * than two dataset the kernel function and k should have 
  * change accordingly
  */

template<typename Tree, typename S>
void
RangeSearchRules<Tree,S>::base_case (vector<Box*> s) {
  real_t dist;
  int k = 1;
  for (size_t i = s[k]->begin(); i < s[k]->end(); ++i) {
    /* Check if this target point can be improved further */
    if (local_distance_update(*s[k-1], i) == DBL_MAX)
      continue;

    for (size_t j = s[k-1]->begin(); j < s[k-1]->end(); ++j) {
      dist = metric->compute_distance (trg_data[i], src_data[j]);
      if (range.contains(dist)) {
        neighbors[i].push_back(kNN_distance(src_tree.index[j], dist));
      }
    }
  }
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S>
inline
bool
RangeSearchRules<Tree,S>::prune_subtree (Box& s, int t) {
  int i = s.index();
  Range sum = s.range_distance(trg_data[t]);
  /* If the range does not overlap, prune */
  if (!sum.overlaps(range))
    return true;
  /* All points are within range, add all of them */
  if (range.contains(sum))
    return true;
  return false;
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S>
inline
bool
RangeSearchRules<Tree,S>::prune_subtree (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();

  Range sum = t.range_distance(s);
  /* If the range does not overlap, prune */
  real_t temp = sum.overlaps(range) ? 0 : DBL_MAX;

  /* All points are within range, prune */
  if (range.contains(sum))
    temp = DBL_MAX;    
  if (temp == DBL_MAX)
    return true;
  return false;
}

template<typename Tree, typename S>
inline
bool
RangeSearchRules<Tree,S>::prune_subtree (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  Range sum = s[k]->range_distance(*s[k-1]);
  real_t temp = sum.overlaps(range) ? 0 : DBL_MAX;
  
  if (range.contains(sum))
    temp = DBL_MAX;
  if (temp == DBL_MAX)
    return true;
  return false;
}
/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S>
inline
vector<int>
RangeSearchRules<Tree,S>::visit (Box& s, int t) {
  vector<Box>& stree = src_tree.node_data;
  real_t left_score = distance_update(stree[s.child], t);
  real_t right_score = distance_update(stree[s.child+1], t);
  /* Check if left branch should be traversed first */
   if (left_score < right_score)
      return vector<int> {0,1};
    else
      return vector<int> {1, 0};
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S>
vector<int>
RangeSearchRules<Tree,S>::visit (Box& s, Box& t) {
  vector<int> visit_order;
  vector<Box>& stree = src_tree.node_data;
  for (int i = 0; i < s.num_child(); i++) {
    /* This condition would eiliminate some of the child nodes and that would influence the # BaseCases*/
    if (distance_update(stree[s.child+i], t) != DBL_MAX)
      visit_order.push_back(i);
  }
  return visit_order;
}

template<typename Tree, typename S>
inline
vector<int>
RangeSearchRules<Tree,S>::visit (vector<Box*> s, int div) {
  vector<int> visit_order;
  vector<Box>& stree = src_tree.node_data;
  /* Decide which box to compare with */
  int next = (div == s.size() - 1) ? div - 1: div + 1;
  for (int i = 0; i < s[div]->num_child(); i++) {
    /* This condition would eiliminate some of the child nodes and that would influence the # BaseCases*/
    if (distance_update(stree[s[div]->child+i], *s[next]) != DBL_MAX)
      visit_order.push_back(i);
  }
  return visit_order;
}
/**
 * Update distance information.
 */
template<typename Tree, typename S>
inline
void
RangeSearchRules<Tree,S>::update_state (Box& s, Box& t) {
  vector<Box>& ttree = trg_tree.node_data;
  for (int i = 0; i < t.num_child(); i++)
    distance_update(s, ttree[t.child+i]);
}
template<typename Tree, typename S>
inline
void
RangeSearchRules<Tree,S>::update_state (vector<Box*> s) {
  vector<Box>& ttree = trg_tree.node_data;
  distance_update(*s[0], ttree[s[1]->child]);
  distance_update(*s[0], ttree[s[1]->child+1]);
}
/**
 * Intersection data is computed.
 */
template<typename Tree, typename S>
real_t
RangeSearchRules<Tree,S>::distance_update (Box& s, int t) {
  int i = s.index();
  Range sum = s.range_distance(trg_data[t]);
  real_t temp = 0;
  // If the range does not overlap, prune
  if (!sum.overlaps(range)) {
    return DBL_MAX;
  }
 

  // All points are within range, add all of them
  if (range.contains(sum)) {
    real_t dist;
    for (size_t j = s.begin(); j < s.end(); ++j) {
      dist = metric->compute_distance (trg_data[t], src_data[j]);
      neighbors[t].push_back(kNN_distance(src_tree.index[j], dist));
    }
    temp = DBL_MAX;
  }

  return temp;
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S>
real_t
RangeSearchRules<Tree,S>::local_distance_update (Box& s, int t) {
  Range sum = s.range_distance(trg_data[t]);
  /* If the range does not overlap, prune */
  if (!sum.overlaps(range))
    return DBL_MAX;

  /* All points are within range, add all of them */
  if (range.contains(sum)) {
    real_t dist;
    for (size_t j = s.begin(); j < s.end(); ++j) {
      dist = metric->compute_distance (trg_data[t], src_data[j]);
      kNN_distance kd(src_tree.index[j], dist);
      neighbors[t].push_back(kd);
    }
    return DBL_MAX;
  }
  /* Recursion order doesn't matter in range search */
  return 0;
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S>
real_t
RangeSearchRules<Tree,S>::distance_update (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();

  Range sum = t.range_distance(s);
  /* If the range does not overlap, prune */
  real_t temp = sum.overlaps(range) ? 0 : DBL_MAX;

  /* All points are within range, add all of them */
  if (range.contains(sum)) {    
    real_t dist;
    for (size_t ii = t.begin(); ii < t.end(); ++ii) {
      for (size_t jj = s.begin(); jj < s.end(); ++jj) {
        dist = metric->compute_distance (trg_data[ii], src_data[jj]);
        neighbors[ii].push_back(kNN_distance(src_tree.index[jj], dist));
        // kNN_distance kd(src_tree.index[jj], dist);
        // if (neighborsCheck[ii].count(jj) == 0) {
        //   neighborsCheck[ii].insert(jj);     
        //   neighbors[ii].push_back(kd);
        // }
      }
    }
    temp = DBL_MAX;
  }
  return temp;
}

template<typename Tree, typename S>
real_t
RangeSearchRules<Tree,S>::distance_update (vector<Box*> s) {
  int i = s[0]->index();
  int j = s[1]->index();

  Range sum = s[1]->range_distance(*s[0]);
   /* If the range does not overlap, prune */
  real_t temp = sum.overlaps(range) ? 0 : DBL_MAX;

  /* All points are within range, add all of them */
  if (range.contains(sum)) {
    real_t dist;
    for (size_t ii = s[1]->begin(); ii < s[1]->end(); ++ii)
      for (size_t jj = s[0]->begin(); jj < s[0]->end(); ++jj) {
        dist = metric->compute_distance (trg_data[ii], src_data[jj]);
        neighbors[ii].push_back(kNN_distance(src_tree.index[jj], dist));
      }
    temp = DBL_MAX;
  }
  return temp;
}
#endif

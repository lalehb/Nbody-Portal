#ifndef INC_TYPE_H
#define INC_TYPE_H

#include <stdint.h>
#include <iostream>

#if !defined (IDEAL_ALIGNMENT)
/** Default alignment, in bytes. */
# define IDEAL_ALIGNMENT 16
#endif

namespace Nbody {

struct Expr;

/* Defines standard types */
struct Type {
  enum {Int, UInt, Float, Handle} t;

  /* Size of the element */
  int bits;

  int bytes () const {return bits/8;}

  /* No of words per vector register. */
  int vector_len () {
    return IDEAL_ALIGNMENT / bytes();
  }

  /* Number of elements */
  int nelem;

  /* Helper functions */
  bool is_bool()    const {return t == UInt && bits == 1;}
  bool is_vector()  const {return nelem > 1;}
  bool is_scalar()  const {return nelem == 1;}
  bool is_float()   const {return t == Float;}
  bool is_int()     const {return t == Int;}
  bool is_uint()    const {return t == UInt;}
  bool is_handle()  const {return t == Handle;}

  /* Compare if two types are the same */
  bool operator==(const Type &other) const {
    return t == other.t && bits == other.bits && nelem == other.nelem;
  }

  /* Compare if two types are not the same */
  bool operator!=(const Type &other) const {
    return t != other.t || bits != other.bits || nelem != other.nelem;
  }


  /* Create a vector of this type with n elements */
  Type make_vector(int n) const {
    Type type = {t, bits, n};
    return type;
  }

  /* Return the type of a single element of this vector */
  Type element_type() const {
    Type type = {t, bits, 1};
    return type;
  }

  void print() const {
    if (is_bool())
          std::cout << "bool\n";
    if (is_vector())
          std::cout << "vector\n";
    if (is_scalar())
          std::cout << "scalar\n";
    if (is_float())
          std::cout << "float\n";
    if (is_int())
          std::cout << "int\n";
    if (is_uint())
          std::cout << "uint\n";
    if (is_handle())
          std::cout << "handle\n";
    
  }
};




/* Create a signed integer type */
inline Type Int (int bits, int nelem = 1) {
  Type t;
  t.t = Type::Int;
  t.bits = bits;
  t.nelem = nelem;
  return t;
}

/* Create an unsigned integer type */
inline Type UInt (int bits, int nelem = 1) {
  Type t;
  t.t = Type::UInt;
  t.bits = bits;
  t.nelem = nelem;
  return t;
}

/* Create a floating point type */
inline Type Float (int bits, int nelem = 1) {
  Type t;
  t.t = Type::Float;
  t.bits = bits;
  t.nelem = nelem;
  return t;
}

/* Create a boolean type */
inline Type Bool(int nelem = 1) {
  return UInt(1, nelem);
}

/* Create an opaque pointer type */
inline Type Handle (int nelem = 1) {
  Type t;
  t.t = Type::Handle;
  t.bits = 64;
  t.nelem = nelem;
  return t;
}

/* Return the type of the expression */
template<typename T>
struct type_of_helper;

template<typename T>
struct type_of_helper<T*> {
  operator Type() {
    return Handle();
  }
};

template<>
struct type_of_helper<float> {
  operator Type() {return Float(32);}
};

template<>
struct type_of_helper<double> {
  operator Type() {return Float(64);}
};

template<>
struct type_of_helper<int> {
  operator Type() {return Int(32);}
};

template<>
struct type_of_helper<bool> {
  operator Type() {return Bool();}
};

/* Prototype for type_of */
template<typename T>
Type type_of() {
  return Type(type_of_helper<T>());
}


}
#endif

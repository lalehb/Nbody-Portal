#ifndef _GAUSSIAN_KERNEL_H_
#define _GAUSSIAN_KERNEL_H_

#include "Metric.h"
#include "Kernel.h"

//typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;
//typedef Metric<Point, Point> MetricType;

/**
 * The standard well-known Gaussian kernel computed as
 * \f[
 * K(x, y) = exp(-\frac{|| x - y ||^2}{2 \mu^2})
 * \f]
 *
 * where \f$\mu\f$ is the kernel width.
 */
class GaussianKernel: public Kernel
{
  /* Kernel bandwidth */
  real_t bandwidth;

  /* Pre-computed helper value: \f$ \tau = -\frac{1}{2 \mu^2} \f$ */
  real_t tau;

  MetricType* metric;

  public:
  /* Construct gaussian kernel */
  GaussianKernel (real_t width) : bandwidth(width), tau(-0.5 * pow(bandwidth, -2.0)), metric(new SquaredEuclideanMetricType()) { }

  /* Evaluates the kernel */
  real_t compute (const Point& a, const Point&b) const {
    return exp(tau * metric->compute_distance(a, b));
  }

  /* Evaluates the kernel given the squared distance between two points */
  real_t compute (const real_t dist) const {
    return exp(tau * dist);
  }

  /* Evaluates the log of the kernel */
  real_t log_compute (const Point&a, const Point&b) const {
    return tau * metric->compute_distance(a, b);
  }

  /* Evaluates the log of the kernel given the squared distance between two points */
  real_t log_compute (const real_t dist) const {
    return tau * dist;
  }

  /* Returns the normalization constant of the Gaussian kernel */
  real_t norm (size_t dim) {
    return 1.0 / pow(sqrt(2.0 * M_PI) * bandwidth, dim);
  }

  /* Returns the log of the normalization constant */
  real_t log_norm (size_t dim) {
    return -0.5 * dim * log(2.0 * M_PI * bandwidth * bandwidth);
  }
};

#endif

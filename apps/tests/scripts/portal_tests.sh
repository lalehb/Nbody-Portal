#!/bin/bash

SIZE=1000
DIM=10
INPUT=../input/uniform_random/n${SIZE}_d${DIM}.csv
OUT=results.out

for i in 1 50 100
do
  env LS=$i ./test--kdtree ${SIZE} ${DIM} uniform 2>&1 | tee -a ${OUT}
  env LS=$i ./test--balltree ${SIZE} ${DIM} uniform 2>&1 | tee -a ${OUT}
  for tree in yes no
  do
    env LS=$i S=$tree ./test--kNN_kdtree ${SIZE} ${DIM} ${INPUT} 3 2>&1 | tee -a ${OUT}
    env LS=$i S=$tree ./test--kNN_balltree ${SIZE} ${DIM} ${INPUT} 3 2>&1 | tee -a ${OUT}
    env LS=$i S=$tree ./test--range_search_kdtree ${SIZE} ${DIM} ${INPUT} 2 3 2>&1 | tee -a ${OUT}
    env LS=$i S=$tree ./test--range_search_balltree ${SIZE} ${DIM} ${INPUT} 2 3 2>&1 | tee -a ${OUT}
    env LS=$i S=$tree ./test--emst_kdtree ${SIZE} ${DIM} ${INPUT} 2>&1 | tee -a ${OUT}
    env LS=$i S=$tree ./test--emst_balltree ${SIZE} ${DIM} ${INPUT} 2>&1 | tee -a ${OUT}
  done
done

#include <cfloat>
#include "PortalOperator.h"


PortalOperator::PortalOperator() : k(1){
}

/**
Constructor of operator.
Operators that require the use of a k are not allowed to be created using this.
**/
PortalOperator::PortalOperator(PortalOperator::OP o) : k(1){
	assert("PortalOperator not initialized with int k" && o != PortalOperator::OP::KARGMAX);
	assert("PortalOperator not initialized with int k" && o != PortalOperator::OP::KARGMIN);
	assert("PortalOperator not initialized with int k" && o != PortalOperator::OP::KMIN);
	assert("PortalOperator not initialized with int k" && o != PortalOperator::OP::KMAX);

	this->o = o;
	this->initFilterVar = this->getInitFilterVar();
}

/**
Construction of operator.
Operators that don't require a k are not allowed to be created using this.
**/
PortalOperator::PortalOperator(PortalOperator::OP o , int k){
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::FORALL);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::SUM);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::PRODUCT);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::LOGSUM);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::UNION);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::UNIONARG);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::ARGMIN);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::ARGMAX);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::MIN);
	assert("PortalOperator initialized with unnecessary int k" && o != PortalOperator::OP::MAX);

	this->o = o;
	this->k = k;
	this->initFilterVar = this->getInitFilterVar();
}

/**
Returns the operator
**/
PortalOperator::OP PortalOperator::getOperator() const{
	return this->o;
}

/**
Returns k if it exists, or throws an error
**/
int PortalOperator::getK() const{
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::FORALL);
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::SUM);
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::PRODUCT);
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::LOGSUM);
	//assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::UNION);
	//assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::UNIONARG);
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::ARGMIN);
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::ARGMAX);
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::MIN);
	assert("PortalOperator getK() called on operator without k" && this->o != PortalOperator::OP::MAX);

	return this->k;
}

/**
Returns k, if or if not it exists (1 is default)
**/
int PortalOperator::getUncheckedK() const{
	return this->k;
}

/**
Change the k value

Args
	newK - k to change to
**/
void PortalOperator::changeK(int newK){
	this->k = newK;
}

/**
Gets the output type
2 Types
	LIST - Output of this Operator is a list of values
	VALUE - Output of this Operator is a value
**/
PortalOperator::IOType PortalOperator::getOutputType() const{
	if(this->o == PortalOperator::OP::FORALL || this->o == PortalOperator::OP::UNION || this->o == PortalOperator::OP::UNIONARG || this->o == PortalOperator::OP::KARGMAX || this->o == PortalOperator::OP::KARGMIN || this->o == PortalOperator::OP::KMAX || this->o == PortalOperator::OP::KMIN){
		return PortalOperator::IOType::LIST;
	}
	return PortalOperator::IOType::VALUE;
}

/**
Gets the input of the operator type
2 Types
	ALL - Anything (IE LIST or VALUE)
	VALUE - Only a set of a values can be used as the input to this op
**/
PortalOperator::IOType PortalOperator::getInputType() const{
	if(this->o == PortalOperator::OP::FORALL){
		return PortalOperator::IOType::ALL;
	}
	return PortalOperator::IOType::VALUE;
}

/**
Returns if this operator is a single variable filter.

A single variable filter takes in a set of values as the input and returns 1 single value.

Also known as a single variable reduction filter.
**/
bool PortalOperator::isSingleVariableFilter() const{
	return this->getInputType() == PortalOperator::IOType::VALUE && this->getOutputType() == PortalOperator::IOType::VALUE;
}

/**
Returns if this operator is a multi variable filter.

A multi variable filter takes in a set of values as the input and returns a set of values as the output.

Also known as a multi variable reduction filter.
**/
bool PortalOperator::isMultiVariableFilter() const{
	return this->getInputType() == PortalOperator::IOType::VALUE && this->getOutputType() == PortalOperator::IOType::LIST;
}

/**
Returns if or if not this is is either a single or multi variable reduction. filter.
**/
bool PortalOperator::isReductionFilter() const{
	return this->getInputType() == PortalOperator::IOType::VALUE;
}

/**
Returns if this is an argument operator.

An argument operator is an operator that returns the indices of the points the give the result, rather than the result itself.
**/
bool PortalOperator::isArgOp() const{
	switch(this->o){
		case PortalOperator::OP::ARGMIN:
		case PortalOperator::OP::ARGMAX:
		case PortalOperator::OP::KARGMIN:
		case PortalOperator::OP::KARGMAX: return true;
	}
	return false;
}

/**
Returns the initial filter double for any operator.

This is useful for starting out the filter.

For example, if we started off all values as a 0 and tried to do Max(0 , -1), we would get 0, even thoguh -1 was the first valid value
Similarly, for the product, 0 * n is always 0.
**/
double PortalOperator::getInitFilterVar() const{
	switch(this->o){
		//enum OP{FORALL , SUM , PRODUCT , LOGSUM , UNION , UNIONARG , ARGMIN , ARGMAX , MIN , MAX , KARGMIN , KARGMAX , KMIN , KMAX};
		case PortalOperator::OP::SUM: return 0;
		case PortalOperator::OP::PRODUCT: return 1;
		case PortalOperator::OP::KMIN:
		case PortalOperator::OP::MIN: return std::numeric_limits<double>::max();
		case PortalOperator::OP::KMAX:
		case PortalOperator::OP::MAX: return std::numeric_limits<double>::lowest();
		case PortalOperator::OP::KARGMIN:
		case PortalOperator::OP::ARGMIN: return std::numeric_limits<double>::max();
		case PortalOperator::OP::KARGMAX:
		case PortalOperator::OP::ARGMAX: return std::numeric_limits<double>::lowest();
		case PortalOperator::OP::UNIONARG:
		case PortalOperator::OP::UNION: return -1;
	}
	return 2; //If we see a 2 anywhere in any start, we know that something has gone wrong here
}

/**
Returns if the operator is comparative.

A comparative operator checks the current value's validity based on others.
**/
bool PortalOperator::isComparative() const{
       switch(this->o){
               case PortalOperator::OP::MIN:     return true;
               case PortalOperator::OP::MAX:     return true;
               case PortalOperator::OP::ARGMIN:  return true;
               case PortalOperator::OP::ARGMAX:  return true;
               case PortalOperator::OP::KMIN:    return true;
               case PortalOperator::OP::KMAX:    return true;
               case PortalOperator::OP::KARGMIN: return true;
               case PortalOperator::OP::KARGMAX: return true;
               case PortalOperator::OP::SUM:     return false;
               case PortalOperator::OP::PRODUCT: return false;
               case PortalOperator::OP::UNION:   return false;
               case PortalOperator::OP::LOGSUM:  return false;
               case PortalOperator::OP::FORALL:  return false;

       }
       return false;
}



/**
Returns the template inital values for the operator
**/
double PortalOperator::getTemp() const{
       switch(this->o){
               case PortalOperator::OP::MIN:     return DBL_MIN;
               case PortalOperator::OP::MAX:     return DBL_MAX;
               case PortalOperator::OP::ARGMIN:  return DBL_MIN;
               case PortalOperator::OP::ARGMAX:  return DBL_MAX;
               case PortalOperator::OP::KMIN:    return DBL_MIN;
               case PortalOperator::OP::KMAX:    return DBL_MAX;
               case PortalOperator::OP::KARGMIN: return DBL_MIN;
               case PortalOperator::OP::KARGMAX: return DBL_MAX;
               case PortalOperator::OP::SUM:     return 0.0;
               case PortalOperator::OP::PRODUCT: return 1.0;
               case PortalOperator::OP::UNION:   return 1.0;
               case PortalOperator::OP::LOGSUM:  return 0.0;
               case PortalOperator::OP::FORALL:  return 0.0;

       }
       return false;
}
/**
Returns the inverse of this operator.

Args
	a - first value
	b - second value
**/
bool PortalOperator::reverse(double a, double b){
       switch(this->o){
               case PortalOperator::OP::MIN:     return  (a > b);
               case PortalOperator::OP::MAX:     return  (a < b);
               case PortalOperator::OP::ARGMIN:  return  (a > b);
               case PortalOperator::OP::ARGMAX:  return  (a < b);
               case PortalOperator::OP::KMIN:    return  (a > b);
               case PortalOperator::OP::KMAX:    return  (a < b);
               case PortalOperator::OP::KARGMIN: return  (a > b);
               case PortalOperator::OP::KARGMAX: return  (a < b);
               case PortalOperator::OP::SUM:     return  false;
               case PortalOperator::OP::PRODUCT: return  false;
               case PortalOperator::OP::UNION:   return  false;
               case PortalOperator::OP::LOGSUM:  return  false;
               case PortalOperator::OP::FORALL:  return  false;

       }
       return false;
}


/**
Ostream operator for PortalOperator
**/
std::ostream& operator<<(std::ostream& os, const PortalOperator& po){
	switch(po.getOperator()){
		case PortalOperator::OP::FORALL:
			os << "FORALL PortalOperator";
			break;
		case PortalOperator::OP::SUM:
			os << "SUM PortalOperator";
			break;
		case PortalOperator::OP::PRODUCT:
			os << "PRODUCT PortalOperator";
			break;
		case PortalOperator::OP::LOGSUM:
			os << "LOGSUM PortalOperator";
			break;
		case PortalOperator::OP::UNION:
			os << "UNION PortalOperator";
			break;
		case PortalOperator::OP::UNIONARG:
			os << "UNIONARG PortalOperator";
			break;
		case PortalOperator::OP::ARGMIN:
			os << "ARGMIN PortalOperator";
			break;
		case PortalOperator::OP::ARGMAX:
			os << "ARGMAX PortalOperator";
			break;
		case PortalOperator::OP::MIN:
			os << "MIN PortalOperator";
			break;
		case PortalOperator::OP::MAX:
			os << "MAX PortalOperator";
			break;
		case PortalOperator::OP::KARGMIN:
			os << "KARGMIN PortalOperator";
			break;
		case PortalOperator::OP::KARGMAX:
			os << "KARGMAX PortalOperator";
			break;
		case PortalOperator::OP::KMIN:
			os << "KMIN PortalOperator";
			break;
		case PortalOperator::OP::KMAX:
			os << "KMAX PortalOperator";
			break;
	}
	return os;
}

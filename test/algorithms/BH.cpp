#include <iostream>
#include "Nbody.h"
#include "NBodyExpression.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "JITModule.h"
#include <math.h>
#include "Clock.hpp"

//using namespace std;
using namespace Nbody;
// typedef GaussianKernel KernelType;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <threshold>\n", use);
}


int main (int argc, char** argv){

	if (argc != 4) {
            usage__ (argv[0]);
            return -1;
    }

    std::string filePathT = argv[1];
    Storage fileObjT(filePathT);


    std::string filePathS = argv[2];
    Storage fileObjS(filePathS);

    real_t threshold = atof(argv[3]);
    Clock timer;


    Expr a = Variable::make("outterSet");
    Expr b = Variable::make("innerSet");
    Expr gravitational_constant = Flt::make(6.67e-11);
    Expr regulizer = Flt::make(6.67e-14);


    /* computing Potential instead of 3 dimenstion of forces */
    Expr denum = sqrt(pow(a[0] - b[0] , 2) + pow(a[1] - b[1] , 2) + pow(a[2] - b[2] , 2)) + regulizer ;
    Expr force = gravitational_constant * (a[3] * b[3])/denum;


    NBodyExpression expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSet" , fileObjS , force);


    const char* result_file = "test/BH.stmt";
    expr.compile_to_lowered_form(result_file);

    timer.start();
    expr.executeTraverse();
    cout << "Total time: " << timer.seconds() << endl;

    Storage output = expr.getOutput();
    bool valid = expr.Validate(output);


    if (!(error)) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }


	return 0;
}
